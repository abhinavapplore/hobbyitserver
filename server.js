var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var baseUrl = 'https://trippernew.s3.ap-south-1.amazonaws.com/';
var morgan = require('morgan');
var nodemailer = require('nodemailer');
var mongoose = require('mongoose');
var GeoPoint = require('geopoint');
var CryptoJS = require('crypto-js');
var convert = require('xml-js');
var md5 = require('md5');
var methodOverride = require('method-override');
var cors = require('cors');
var config = require('./config/database'); // get db config file
require('dotenv').config();
require('dotenv').load();
var path = require('path');
var port = process.env.PORT || 80;
var Category = require('./app/models/category');
var Vendor = require('./app/models/vendor');
var vendorCategory = require('./app/models/vendorcategory');
var Electricity = require('./app/models/electricity');
const cron = require('node-cron');
var common = require('./common.utils');
// common.sendNotification()
var Gas = require('./app/models/gas');
var FastTag = require('./app/models/fasttag');
var LifeInsurance = require('./app/models/lifeInsurance');
var HealthInsurance = require('./app/models/healthInsurance');
var LoanRepayment = require('./app/models/loanrepayment');
var MunicipalTaxes = require('./app/models/municipaltaxes');
var EducationFees = require('./app/models/educationfees');
var LandlinePostpaid = require('./app/models/landlinepostpaid');
var HousingSociety = require('./app/models/housingsociety');

var Broadband = require('./app/models/broadband');
var Water = require('./app/models/waterbiller');

var Trend = require('./app/models/trend');
// var Transaction        = require('./app/models/transaction');
var Recharge = require('./app/models/recharge');
var Subscription = require('./app/models/subscription');
var ActiveSubscription = require('./app/models/subscriptionOrder');
var Store = require('./app/models/store');

const request = require('request');

//var crypto = require('crypto');
const INITIALIZATION_VECTOR = '0000000000000000';
var User = require('./app/models/user');
var Order = require('./app/models/order');

var twilio = require('twilio');

var randomstring = require('randomstring');
var randomNumber = require('random-number');
var Insta = require('instamojo-nodejs');
var querystring = require('querystring');
var http = require('http');
const axios = require('axios');
var moment = require('moment');
var SubCategory = require('./app/models/gas');
var Prime = require('./app/models/prime');

// hobbyit models

var PostType = require('./app/models/postType');
var Notification = require('./app/models/notification');
var Transaction = require('./app/models/transaction');
var Bookmark = require('./app/models/bookmark');
var UnlockUser = require('./app/models/unlockuser');
var Laundry = require('./app/models/laundry');
var BlockedUser = require('./app/models/blockedUser');
var Media = require('./app/models/media');
var Credit = require('./app/models/Credit');

var barcode = require('barcode');
var gm = require('gm').subClass({
  imageMagick: true,
});

var bcrypt = require('bcryptjs');
const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);

// Alder ERP Schema start

var AlderUser = require('./app/models/alderUser');
var Id = require('./app/models/id');
var ManufacturingPlant = require('./app/models/manufacturingPlant');
var Warehouse = require('./app/models/warehouse');
var NewUser = require('./app/models/newUser');
var Partner = require('./app/models/partner');
// var fast2sms = require('fast2sms');
const fast2sms = require('fast-two-sms');

var Product = require('./app/models/product');

// ****************** TO DO ***********************
// var orderStatus=[
//     {
//         "10":"New Order"
//     }
// ]

//Alder ERP schema ends

app.use(cors());

var multer = require('multer');
var s3 = require('multer-storage-s3');
var storage = s3({
  destination: function (req, file, cb) {
    cb(null, '');
  },
  filename: function (req, file, cb) {
    cb(null, randomstring.generate() + path.extname(file.originalname));
  },
  bucket: 'trippernew',
  region: 'ap-south-1',
});
var uploadMiddleware = multer({
  storage: storage,
});

var moment = require('moment');

function getNextSequence(name, fn) {
  var retseq = 0;

  Id.findOneAndUpdate(
    {
      _id: name,
    },
    {
      $inc: {
        seq: 1,
      },
    },
    {
      new: true,
    },
    function (err, doc) {
      if (err) {
        console.log('Something wrong when updating data!');
      }

      retseq = doc.seq;

      fn(retseq);
    }
  );
}

Date.prototype.addHours = function (h) {
  this.setTime(this.getTime() + h * 60 * 60 * 1000);
  return this;
};

Date.prototype.addDays = function (days) {
  var dat = new Date(this.valueOf());
  dat.setDate(dat.getDate() + days);
  return dat;
};

var hasOwnProperty = Object.prototype.hasOwnProperty;
var isEmpty = function (obj) {
  if (obj == null) return true;

  if (obj.length > 0) return false;
  if (obj.length === 0) return true;

  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) return false;
  }

  return true;
};

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());
app.use(methodOverride());
app.use(morgan('dev'));
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type,Authorization, Accept'
  );
  next();
});

mongoose.connect(
  'mongodb://root:TFRFj4kra8pn@ec2-15-206-93-147.ap-south-1.compute.amazonaws.com:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false',
  // 'mongodb://root:aTmDVA33SSFZ@ec2-65-0-183-17.ap-south-1.compute.amazonaws.com/exp?authSource=admin',
  {
    useNewUrlParser: true,
  }
);
// mongoose.connect("mongodb://localhost/main10store");

// mongoose.connect("mongodb+srv://root:BqYtiYJDgtA9@cluster0.hbvfz.mongodb.net/alder?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
//     .then((result) => {
//         // app.listen(PORT,() => {
//         //     console.log("Application Started in Port " + PORT)
//         // })
//     })
//     .catch((err) => console.log(err));

// mongoose.connect(config.database);
// pass passport for configuration

route = require('./routes/routes');
var apiRoutes = express.Router();

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/assets/images'));
app.use(express.static('uploads'));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, './public', 'index.html'));
});

// connect the api routes under /api/*
app.use('/api', apiRoutes);
app.get('/testinglocal', (req, res) => {
  res.send('asdf');
});

Array.prototype.contains = function (obj) {
  var i = this.length;
  while (i--) {
    if (this[i] == obj) {
      return true;
    }
  }
  return false;
};

// hobbyIT dashboard api's start

apiRoutes.post('/getDashboardDetail', function (req, res) {
  var professionalUserPost = [];
  var passionistUserPost = [];
  var professionalUsers = [];
  var passionistUsers = [];
  User.find(
    {
      createdDate: {
        $lt: new Date(),
        $gte: new Date(new Date().setDate(new Date().getDate() - 30)),
      },
    },
    function (err, users) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'No users found',
        });
      } else {
        console.log('section 1');
        users.forEach((element, idx) => {
          if (element.userType == 'professional') {
            professionalUsers.push(element);
          } else if (element.userType == 'passionist') {
            passionistUsers.push(element);
          }
          console.log('section 2');
          PostType.find(
            {
              createdDate: {
                $lt: new Date(),
                $gte: new Date(new Date().setDate(new Date().getDate() - 7)),
              },
            },
            function (err, posts) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'No posts found',
                });
              } else {
                console.log('section 3');
                console.log(posts.length);
                posts.forEach((elem, index) => {
                  console.log('section 4');
                  User.findOne(
                    {
                      _id: elem.userId,
                    },
                    function (err, user) {
                      if (err) {
                        console.log(err);
                        res.json({
                          success: false,
                          msg: 'No user found',
                        });
                      } else {
                        console.log('section 5');
                        console.log(user.userType);
                        if (user.userType == 'professional') {
                          professionalUserPost.push(elem);
                        } else if (user.userType == 'passionist') {
                          passionistUserPost.push(elem);
                        }
                        if (
                          index + 1 == posts.length &&
                          idx + 1 == users.length
                        ) {
                          var profUserCount = professionalUserPost.length;
                          console.log(profUserCount);
                          var pasUserCount = passionistUserPost.length;
                          console.log(pasUserCount);
                          res.json({
                            success: true,
                            msg: 'One week posts sent successfully',
                            oneWeekPost: posts,
                            professionalUserPostCount: profUserCount,
                            passionistUserPostCount: pasUserCount,
                            oneMonthProfessionalUsers: professionalUsers,
                            oneMonthPassionistUsers: passionistUsers,
                          });
                        }
                      }
                    }
                  );
                });
                if (posts.length == 0) {
                  var profUserCount = 0;
                  var pasUserCount = 0;
                  res.json({
                    success: true,
                    msg: 'One week posts sent successfully',
                    oneWeekPost: posts,
                    professionalUserPostCount: profUserCount,
                    passionistUserPostCount: pasUserCount,
                    oneMonthProfessionalUsers: professionalUsers,
                    oneMonthPassionistUsers: passionistUsers,
                  });
                }
              }
            }
          );
        });
      }
    }
  );
});

// change password api

apiRoutes.post('/updatePassword', function (req, res) {
  var contactNum = req.body.contactNum;
  console.log(contactNum);
  var password = req.body.password;
  console.log(password);
  User.findOne(
    {
      contactNum: contactNum,
    },
    function (err, user) {
      if (!user) {
        res.json({
          success: false,
          msg: 'No user with this Contact Number exist',
        });
      } else {
        bcrypt.hash(password, salt, (err, encrypted) => {
          var newPassword = encrypted;
          User.update(
            {
              contactNum: contactNum,
            },
            {
              $set: {
                password: newPassword,
              },
            },
            function (err, user) {
              //handle it
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to Update',
                });
                //throw err;
                console.log(err);
              } else {
                res.json({
                  success: true,
                  msg: 'User Successfully Updated',
                  user: user,
                });
              }
            }
          );
          // next()
        });
      }
    }
  );
});

// get professionals users list

apiRoutes.post('/getUsersList', function (req, res) {
  var professionalUsers = [];
  var passionistUsers = [];
  User.find({}, function (err, users) {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        msg: 'No users found',
      });
    } else {
      users.forEach((element, index) => {
        if (element.userType == 'professional') {
          professionalUsers.push(element);
        } else if (element.userType == 'passionist') {
          passionistUsers.push(element);
        }
        if (index + 1 == users.length) {
          res.json({
            success: true,
            msg: 'Request Sent Successfully',
            professionalUsers: professionalUsers,
            passionistUsers: passionistUsers,
          });
        }
      });
    }
  });
});

// get all transactions api

apiRoutes.post('/getAllTransactions', function (req, res) {
  Transaction.find({}, function (err, transactions) {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        msg: 'No transaction found',
      });
    } else {
      res.json({
        success: true,
        msg: 'Request Sent Successfully',
        transactions: transactions,
      });
    }
  });
});

// get user details

apiRoutes.post('/getUserDetails', function (req, res) {
  var userId = req.body.userId;
  User.findOne(
    {
      _id: userId,
    },
    function (err, user) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'No user found',
        });
      } else {
        res.json({
          success: true,
          msg: 'user found',
          data: user,
        });
      }
    }
  );
});

// hobbyIT dashboard api's end

//hobbyIT app api's start

// add post api

apiRoutes.post(
  '/addPost',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var userId = req.body.userId;
    console.log(userId);
    var post = req.body;
    console.log('This is body ' + JSON.stringify(req.body));
    if (userId) {
      if (post.posttype == '2') {
        var postData = {
          userId: userId,
          posttype: post.posttype,
          // userImg:post.userImg,
          userName: post.userName,
          postUrl: post.postUrl,
        };
        PostType.addPostType(postData, function (err, postData) {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Failed to add Request',
            });
            //	throw err;
          } else {
            res.json({
              success: true,
              msg: 'Post Added Successfully',
              data: postData,
            });
          }
        });
      } else {
        var postData = {
          postUrl: post.postUrl,
          userId: userId,
          postTitle: post.postTitle,
          postDescription: post.postDescription,
          posttype: post.posttype,
          postCategory: post.postCategory,
          userImg: post.userImg,
          userName: post.userName,
        };
        PostType.addPostType(postData, function (err, postData) {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Failed to add Request',
            });
          } else {
            res.json({
              success: true,
              msg: 'Post Added Successfully',
              data: postData,
            });
          }
        });
      }
    } else {
      res.json({
        success: false,
        msg: 'Failed to update Token Authentication failed',
      });
    }
  }
);

// store only one day story

apiRoutes.post('/story', function (req, res, next) {
  var postId = req.body.postId;
  var userId = req.body.userId;
  if (userId != undefined) {
    PostType.find({}, function (err, posts) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'No post found',
        });
      } else {
        posts.forEach((element) => {
          if (element.posttype == '2') {
            var date = JSON.parse(moment().format('DD'));
            var month = JSON.parse(moment().format('MM'));
            var year = JSON.parse(moment().format('YYYY'));
            console.log(date);
            console.log(month);
            console.log(year);
            PostType.remove(
              {
                posttype: '2',
              },
              {
                createdDate: {
                  $lt: new Date(year, month, date),
                },
              },
              function (err, posts1) {
                if (err) {
                  console.log(err);
                  res.json({
                    success: false,
                    msg: 'Something went wrong',
                  });
                } else {
                  res.json({
                    success: true,
                    msg: 'Post Updated Successfully',
                    data: posts,
                    post: posts1,
                  });
                }
              }
            );
          }
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// update post api

apiRoutes.post('/updatePost', function (req, res, next) {
  var post = req.body.post;
  console.log(post);
  // var userId=req.body.userId;
  // var postCategory=req.body.postCategory;
  // var postDescription=req.body.postDescription;
  // var postTitle=req.body.postTitle;
  // var postUrl=req.body.postUrl;
  if (post.userId != undefined) {
    PostType.findOneAndUpdate(
      {
        _id: post._id,
      },
      {
        $set: {
          postCategory: post.postCategory,
          postDescription: post.postDescription,
          postTitle: post.postTitle,
          postUrl: post.postUrl,
          userId: post.userId,
        },
      },
      function (err, posts) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No post found',
          });
        } else {
          res.json({
            success: true,
            msg: 'Post Updated Successfully',
            data: posts,
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// delete post

apiRoutes.post('/deletePost', function (req, res, next) {
  var userId = req.body.userId;
  console.log(userId);
  var postId = req.body.postId;

  if (userId !== undefined) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          PostType.findByIdAndRemove(
            {
              _id: postId,
            },
            function (err, numberAffected, rawResponse) {
              //handle it
              if (err) {
                res.json({
                  success: false,
                  msg: 'No such Post',
                });
                throw err;
              } else {
                res.json({
                  success: true,
                  msg: 'Post Successfully deleted',
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

// get all posts api

apiRoutes.post('/getPost', function (req, res, next) {
  var userId = req.body.userId;
  if (userId) {
    User.find(
      {
        '._id': userId,
      },
      function (err, user) {
        if (err) {
          res.json({
            success: false,
            msg: 'DB error',
          });
        } else {
          PostType.find({}, function (err, posts) {
            if (err) {
              res.json({
                success: false,
                msg: 'DB error',
              });
            } else {
              var postArray = [];
              var postArray = posts;
              console.log(postArray);
              postArray.forEach((element) => {
                var likes = [];
                likes = element.likes;
                likes.forEach((likesObj) => {
                  if (likesObj.likedPostUserid == userId) {
                    element._doc.isliked = true;
                  } else {
                    element._doc.isliked = false;
                  }
                });
              });
              console.log('ABOVE IS POST ARR');
              Bookmark.find(
                {
                  userId: userId,
                },
                async function (err, bookmark) {
                  if (err) {
                    res.json({
                      success: false,
                      msg: 'No posts',
                    });
                  } else {
                    console.log('comes here');
                    var bookmarkArray = [];
                    bookmarkArray = bookmark;
                    console.log(bookmarkArray);
                    console.log(bookmarkArray.length);
                    if (bookmarkArray.length == 0) {
                      console.log('abhinav');
                      Media.find({}, (err, banners) => {
                        if (err) {
                          res.json({
                            success: false,
                            msg: 'Failed to fetch Banners',
                          });
                        } else {
                          res.json({
                            success: true,
                            msg: 'Post Fetched',
                            data: posts,
                            banners,
                          });
                          // res.json({success: true,msg:'Post Fetched',data:postArray,user:user,banners});
                        }
                      });
                    } else {
                      console.log('abhishek');
                      postArray.forEach((post) => {
                        for (var i = 0; i < bookmarkArray.length; i++) {
                          if (post._id == bookmarkArray[i].postId) {
                            console.log('goes here');
                            post._doc.isBookmarked = true;
                            console.log(post);
                            break;
                          } else {
                            console.log('goes here');
                            post._doc.isBookmarked = false;
                            console.log(post);
                          }
                        }
                      });
                      console.log('coming last');
                      console.log(postArray);
                      console.log('TATA');

                      Media.find({}, (err, banners) => {
                        if (err) {
                          res.json({
                            success: false,
                            msg: 'Failed to fetch Banners',
                          });
                        } else {
                          res.json({
                            success: true,
                            msg: 'Post Fetched',
                            data: postArray,
                            user: user,
                            banners,
                          });
                        }
                      });
                    }
                  }
                }
              );
            }
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// get all users

function distance(lat1, lon1, lat2, lon2, unit) {
  console.log(lat1);
  console.log(typeof lat1);
  console.log(lon1);
  console.log(typeof lon1);
  console.log(lat2);
  console.log(typeof lat2);
  console.log(lon2);
  console.log(typeof lon2);
  var radlat1 = (Math.PI * lat1) / 180;
  var radlat2 = (Math.PI * lat2) / 180;
  var radlon1 = (Math.PI * lon1) / 180;
  var radlon2 = (Math.PI * lon2) / 180;
  var theta = lon1 - lon2;
  var radtheta = (Math.PI * theta) / 180;
  var dist =
    Math.sin(radlat1) * Math.sin(radlat2) +
    Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist);
  dist = (dist * 180) / Math.PI;
  dist = dist * 60 * 1.1515;
  if (unit == 'K') {
    dist = dist * 1.609344;
  }
  if (unit == 'N') {
    dist = dist * 0.8684;
  }
  console.log(dist);
  return dist;
}

apiRoutes.post('/getAllUser', function (req, res, next) {
  var userId = req.body.userId;
  var userLat = parseInt(req.body.userLat);
  var userLong = parseInt(req.body.userLong);
  // console.log(userId);
  // console.log('this is user id');
  // console.log(userLat);
  // console.log('this is userLat');
  // console.log(userLong);
  // console.log('this is userLong');
  if (userId) {
    User.find({}, function (err, user) {
      if (err) {
        res.json({
          success: false,
          msg: 'DB error',
        });
      } else {
        var allUsers = user;
        var proUserArray = [];
        var pasUserArray = [];
        allUsers.forEach((element) => {
          if (element.userType == 'passionist') {
            console.log('coming right way');
            var lat = parseInt(element.userLatitude);
            var long = parseInt(element.userLongitude);
            var dist = distance(lat, long, userLat, userLong, 'K');
            console.log(dist);
            console.log('Above is dist');
            element._doc.distance = Math.round(dist * 1000) / 1000;
            console.log('below is element');
            console.log(element);
            if (element._id != userId) {
              pasUserArray.push(element);
            }
          } else if (element.userType == 'professional' && element.isVerified) {
            console.log('coming right way');
            var lat = parseInt(element.userLatitude);
            var long = parseInt(element.userLongitude);
            var dist = distance(lat, long, userLat, userLong, 'K');
            console.log(dist);
            console.log('Above is dist');
            element._doc.distance = Math.round(dist * 1000) / 1000;
            console.log('below is element');
            console.log(element);
            if (element._id != userId) {
              proUserArray.push(element);
            }
          }
        });
        Media.find({}, (err, doc) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Error: Getting media files',
            });
          } else {
            res.json({
              success: true,
              msg: 'User Fetched',
              professional: proUserArray,
              passionist: pasUserArray,
              banners: doc,
            });
          }
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// add/delete bookmark api

apiRoutes.post('/bookmark', function (req, res, next) {
  // var token = req.body.token;
  var userId = req.body.userId;
  console.log(userId);
  var bookmark = req.body;

  if (userId !== undefined) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (user == undefined || user == null) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          var bookmarkData = {
            postId: bookmark.postId,
            postUserId: bookmark.postUserId,
            postUrl: bookmark.postUrl,
            userId: userId,
            postTitle: bookmark.postTitle,
            postDescription: bookmark.postDescription,
            posttype: bookmark.posttype,
            postCategory: bookmark.postCategory,
            isBookmared: bookmark.isBookmared,
          };
          //  var isauth= route.memberinfo(token,userId);

          if (userId) {
            Bookmark.addBookmark(bookmarkData, function (err, bookmark) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Failed to add Request',
                });
                //	throw err;
              } else {
                res.json({
                  success: true,
                  msg: 'Bookmark Added Successfully',
                  data: bookmark,
                });
              }
            });
          }
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

// delete bookmark api

apiRoutes.post('/deleteBookmark', function (req, res, next) {
  var userId = req.body.userId;
  var bookmarkId = req.body.bookmarkId;
  var postId = req.body.postId;
  if (userId) {
    Bookmark.findByIdAndRemove(
      {
        _id: bookmarkId,
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'No such Feed',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Bookmard Deleted Deactivated',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// get all bookmarked post

apiRoutes.post('/getBookmark', function (req, res, next) {
  var userId = req.body.userId;
  if (userId) {
    Bookmark.find(
      {
        userId: userId,
      },
      function (err, bookmark) {
        if (bookmark == null || bookmark == undefined) {
          res.json({
            success: false,
            msg: 'No posts',
          });
        } else {
          PostType.find(
            {
              userId: userId,
            },
            function (err, posts) {
              if (posts == null || posts == undefined) {
                res.json({
                  success: false,
                  msg: 'No posts',
                  bookmarkArray: bookmark,
                  postArray: posts,
                });
              } else {
                var postArray = [];
                var postArray = posts;
                console.log(postArray);
                postArray.forEach((element) => {
                  var likes = [];
                  likes = element.likes;
                  likes.forEach((likesObj) => {
                    if (likesObj.likedPostUserid == userId) {
                      element._doc.isliked = true;
                    } else {
                      element._doc.isliked = false;
                    }
                  });
                });
                console.log('ABOVE IS POST ARR');
                res.json({
                  success: true,
                  msg: 'Post Fetched',
                  bookmarkArray: bookmark,
                  postArray: posts,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// get all post api

apiRoutes.post('/getAllPost', function (req, res, next) {
  var userId = req.body.userId;
  if (userId) {
    PostType.find(
      {
        userId: userId,
      },
      function (err, posts) {
        if (err) {
          res.json({
            success: false,
            msg: 'No posts',
          });
        } else {
          var postArray = [];
          var postArray = posts;
          console.log(postArray);
          postArray.forEach((element) => {
            var likes = [];
            likes = element.likes;
            likes.forEach((likesObj) => {
              if (likesObj.likedPostUserid == userId) {
                element._doc.isliked = true;
              } else {
                element._doc.isliked = false;
              }
            });
          });
          console.log('ABOVE IS POST ARR');
          res.json({
            success: true,
            msg: 'Post Fetched',
            postArray: postArray,
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// transaction api

apiRoutes.post('/transaction', function (req, res, next) {
  // var token = req.body.token;
  var userId = req.body.userId;
  console.log(userId);
  var transaction = req.body;
  console.log(transaction);

  // var isauth= route.memberinfo(token,userId);
  // token= "JWT "+token;
  console.log('This is body ' + JSON.stringify(req.body));
  var transactionData = {
    userId: transaction.userId,
    transactionImage: transaction.transactionImage,
    title: transaction.title,
    transactionDate: transaction.transactionDate,
    transactionTime: transaction.transactionTime,
    transactionAmount: transaction.transactionAmount,
  };
  if (userId) {
    Transaction.addTransaction(
      transactionData,
      function (err, transactionData) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to add Request',
          });
          //	throw err;
        } else {
          res.json({
            success: true,
            msg: 'Transaction Successfull',
            data: transactionData,
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

apiRoutes.post('/addMoney', function (req, res) {
  var userId = req.body.userId;
  var transactionAmount = req.body.transactionAmount;
  var transactionImage = req.body.transactionImage;
  var title = req.body.title;
  var transactionDate = req.body.transactionDate;
  var transactionTime = req.body.transactionTime;

  var transactionData = {
    userId: userId,
    transactionImage: transactionImage,
    title: title,
    transactionDate: transactionDate,
    transactionTime: transactionTime,
    transactionAmount: transactionAmount,
    transactionType: 'Add Money',
  };
  console.log(transactionData);

  if (userId !== undefined) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          var prevAmount = user.wallet;
          var updatedAmount = prevAmount + transactionAmount;
          User.update(
            {
              _id: userId,
            },
            {
              $set: {
                wallet: updatedAmount,
              },
            },
            function (err, user) {
              //handle it
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to Update Wallet',
                });
                //throw err;
                console.log(err);
              } else {
                Transaction.addTransaction(
                  transactionData,
                  function (err, transaction) {
                    if (err) {
                      console.log(err);
                      res.json({
                        success: false,
                        msg: 'Failed to add Transaction',
                      });
                      //	throw err;
                    } else {
                      res.json({
                        success: true,
                        msg: 'Money Added Successfully.',
                        user: user,
                        transaction: transaction,
                      });
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

// unlock user api

// apiRoutes.post('/unlockUser', function(req, res){
//     var professionalUserId = req.body.professionalUserId;
//     console.log(professionalUserId);
//     var userId=req.body.userId;
//     console.log(userId);
//     var amount=req.body.amount;
//     var transactionImage=req.body.transactionImage;
//     console.log(transactionImage);
//     var title=req.body.title;
//     var transactionDate=req.body.transactionDate;
//     var transactionTime=req.body.transactionTime;
//     var userName=req.body.userName;
//     var isAddingMoney=req.body.isAddingMoney;
//     var addingAmount=req.body.addingAmount;
//     console.log(amount);
//     var transactionData ={
//         userId:userId,
//         transactionImage:transactionImage,
//         title:title,
//         transactionDate:transactionDate,
//         transactionTime:transactionTime,
//         transactionAmount:amount,
//         userName:userName,
//         transactionType:'Unlock User',
//     }
//     if(userId!=undefined){
//         User.findOne({ '_id': userId }, function(err, user) {
//             console.log(user);
//             console.log('heyyy this is userrrrrrrrrrr');
//             if(user==null || user==undefined){
//                 res.json({success: false, msg: 'No user with this Contact Number exist'});
//             }else {
//                 if(isAddingMoney){
//                     var previousAmount=user.wallet;
//                     var updatedAmount=previousAmount+addingAmount;
//                     console.log(updatedAmount)
//                     console.log('this is updated amount');
//                     User.update({'_id': userId}, { $set: {wallet:updatedAmount}}, function (err, user2) {
//                         console.log(user2);
//                         console.log('this is updated user');
//                         if(err){
//                             res.json({success: false,msg:'Failed to Update Wallet'});
//                             //throw err;
//                             console.log(err);
//                         }else{
//                             User.findOne({ '_id': professionalUserId }, function(err, professional) {
//                                 console.log(professional);
//                                 console.log('this is professional user.......')
//                                 if(professional==null || professional==undefined){
//                                     res.json({success: false, msg: 'No user with this Contact Number exist'});
//                                 }else {
//                                     var professionalUser=professional.unlockedByUser.push({'unlockedUserId':userId});
//                                     console.log(professionalUser);
//                                     console.log('this is final updated user......................');
//                                     User.findOne({ '_id': userId }, function(err, user1) {
//                                         console.log(user1);
//                                         console.log('this is user1.....................');
//                                     var prevAmount=user1.wallet;
//                                     var newAmount=prevAmount-amount;
//                                     console.log(newAmount);
//                                     console.log('this is new amount............');
//                                     User.update({'_id': userId}, { $set: {wallet:newAmount,unlockedProfessionalUser:professionalUser}},function (err, user) {
//                                     if(err){
//                                         res.json({success: false,msg:'Failed to Update'});
//                                         console.log(err);
//                                     }else{
//                                         Transaction.addTransaction(transactionData,function(err, transaction){
//                                             console.log(transaction);
//                                             console.log('this is transaction.............');
//                                         if(err){
//                                             console.log(err);
//                                             res.json({success: false, msg: 'Failed to add Transaction'});
//                                         }
//                                         else{
//                                             res.json({success: true,msg:'User Successfully Updated',user:user1,transaction:transaction});
//                                         }
//                                     });

//                                 }
//                                 });
//                                 });
//                                 }
//                             });
//                         }
//                     });
//                 }else {
//                     var previousAmount=user.wallet;
//                     var updatedAmount=previousAmount-amount;
//                     User.update({'_id': userId}, { $set: {wallet:updatedAmount}}, function (err, user) {
//                         if(err){
//                             res.json({success: false,msg:'Failed to Update Wallet'});
//                             //throw err;
//                             console.log(err);
//                         }else{
//                             User.findOne({ '_id': professionalUserId }, function(err, professional) {
//                                 if(!professional){
//                                     res.json({success: false, msg: 'No user with this Contact Number exist'});
//                                 }else {
//                                     var professionalUser=professionalUser.unlockedByUser.push({'unlockedUserId':userId});
//                                     User.findOne({ '_id': userId }, function(err, user1) {
//                                     var prevAmount=user1.wallet;
//                                     var newAmount=prevAmount-amount;
//                                     User.update({'_id': userId}, { $set: {wallet:newAmount,unlockedProfessionalUser:professionalUser}},function (err, user) {
//                                     if(err){
//                                         res.json({success: false,msg:'Failed to Update'});
//                                         console.log(err);
//                                     }else{
//                                         Transaction.addTransaction(transactionData,function(err, transaction){
//                                         if(err){
//                                             console.log(err);
//                                             res.json({success: false, msg: 'Failed to add Transaction'});
//                                         }
//                                         else{
//                                             res.json({success: true,msg:'User Successfully Updated',user:user1,transaction:transaction});
//                                         }
//                                     });

//                                 }
//                                 });
//                                 });
//                                 }
//                             });
//                         }
//                     });
//                 }
//             }
//         });
//     }
// });

// unlock user api new

// let notificationObject={userId:'sdf',title:'User Unlock',body:`123 has unlocked you`}
// console.log('notificationObject',notificationObject);
// Notification.addNotification(notificationObject)

apiRoutes.post('/unlockUser', function (req, res, next) {
  var isAddingMoney = req.body.isAddingMoney;
  var walletbalance = req.body.walletbalance;
  var hostName = req.body.hostName;
  var recieverImg = req.body.recieverImg;
  var senderImg = req.body.senderImg;
  var hostId = req.body.hostId;
  var userId = req.body.userId;
  if (userId) {
    var transactionAmount = req.body.amount;
    var transactionImage = req.body.transactionImage;
    var title = req.body.title;
    var transactionDate = req.body.transactionDate;
    var transactionTime = req.body.transactionTime;
    // var deviceId=req.body.deviceId;
    var fullName = req.body.fullName;
    User.find(
      {
        userId: hostId,
      },
      function (err, host) {
        var deviceId = host.deviceId;
        if (isAddingMoney) {
          var addingAmount = req.body.addingAmount;

          var transactionDataAddMoney = {
            userId: userId,
            transactionImage: transactionImage,
            title: title,
            transactionDate: transactionDate,
            transactionTime: transactionTime,
            transactionAmount: addingAmount,
            transactionType: 'Add Money',
          };
          Transaction.addTransaction(
            transactionDataAddMoney,
            function (err, transaction) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Failed to add Transaction',
                });
                //	throw err;
              } else {
                var updatedAmount = walletbalance + addingAmount;
                User.findOneAndUpdate(
                  {
                    _id: userId,
                  },
                  {
                    $set: {
                      wallet: updatedAmount,
                    },
                  },
                  function (err, updatedData) {
                    if (err) {
                      console.log(err);
                    } else {
                      var transactionDataAddUser = {
                        userId: userId,
                        transactionImage: transactionImage,
                        title: title,
                        transactionDate: transactionDate,
                        transactionTime: transactionTime,
                        transactionAmount: transactionAmount,
                        transactionType: 'Unlock User',
                      };
                      Transaction.addTransaction(
                        transactionDataAddUser,
                        function (err, transaction2) {
                          if (err) {
                            console.log(err);
                            res.json({
                              success: false,
                              msg: 'Failed to add Transaction',
                            });
                            //	throw err;
                          } else {
                            var updatedAmount1 =
                              updatedAmount - transactionAmount;
                            User.findOneAndUpdate(
                              {
                                _id: userId,
                              },
                              {
                                $set: {
                                  wallet: updatedAmount1,
                                },
                              },
                              function (err, updatedData1) {
                                if (err) {
                                  console.log(err);
                                } else {
                                  UnlockUser.addunlockUser(
                                    {
                                      hostUserId: hostId,
                                      userId: userId,
                                    },
                                    function (err, unlock) {
                                      if (err) {
                                        console.log(err);
                                        res.json({
                                          success: false,
                                          msg: 'Failed to Update',
                                        });
                                        //	throw err;
                                      } else {
                                        UnlockUser.addunlockUser(
                                          {
                                            hostUserId: userId,
                                            userId: hostId,
                                          },
                                          function (err, unlock1) {
                                            if (err) {
                                              console.log(err);
                                              res.json({
                                                success: false,
                                                msg: 'Failed to Update',
                                              });
                                              //	throw err;
                                            } else {
                                              console.log('UNLOCKED SUCCESS11');
                                              console.log('userId', userId);
                                              let notificationObject = {
                                                userId: hostId,
                                                title: 'User Unlock',
                                                body: `${fullName} has unlocked you`,
                                              };
                                              console.log(
                                                'notificationObject',
                                                notificationObject
                                              );
                                              Notification.addNotification(
                                                notificationObject
                                              );
                                              User.findOne(
                                                {
                                                  _id: userId,
                                                },
                                                function (err, user) {
                                                  if (err) {
                                                    console.log(err);
                                                  } else {
                                                    var unlockedUser =
                                                      user.unlockedProfessionalUser;
                                                    unlockedUser.push(hostId);
                                                    var updatedUnlockedUser = unlockedUser;
                                                    User.findOneAndUpdate(
                                                      {
                                                        _id: userId,
                                                      },
                                                      {
                                                        $set: {
                                                          fullName:
                                                            user.fullName,
                                                          firstName:
                                                            user.firstName,
                                                          lastName:
                                                            user.lastName,
                                                          dob: user.dob,
                                                          emailId: user.emailId,
                                                          address: user.address,
                                                          contactNum:
                                                            user.contactNum,
                                                          gender: user.gender,
                                                          wallet: user.wallet,
                                                          deviceId:
                                                            user.deviceId,
                                                          user_img:
                                                            user.user_img,
                                                          about: user.about,
                                                          userLatitude:
                                                            user.userLatitude,
                                                          userLongitude:
                                                            user.userLongitude,
                                                          skillLevel:
                                                            user.skillLevel,
                                                          accessLevelName:
                                                            user.accessLevelName,
                                                          experience:
                                                            user.experience,
                                                          userType:
                                                            user.userType,
                                                          isSuperAdmin:
                                                            user.isSuperAdmin,
                                                          isVerified:
                                                            user.isVerified,
                                                          language:
                                                            user.language,
                                                          userHobby:
                                                            user.userHobby,
                                                          userSkills:
                                                            user.userSkills,
                                                          userInterest:
                                                            user.userInterest,
                                                          unlockedProfessionalUser: updatedUnlockedUser,
                                                          blockedUser:
                                                            user.blockedUser,
                                                          bookmarkedPost:
                                                            user.bookmarkedPost,
                                                          documents:
                                                            user.documents,
                                                          emailIdRegistered:
                                                            user.emailIdRegistered,
                                                        },
                                                      },
                                                      function (err, user1) {
                                                        if (err) {
                                                          console.log(err);
                                                          res.json({
                                                            success: false,
                                                            msg:
                                                              'Failed to Update User',
                                                          });
                                                        } else {
                                                          if (
                                                            deviceId != null ||
                                                            deviceId !=
                                                              undefined
                                                          ) {
                                                            axios
                                                              .post(
                                                                'https://fcm.googleapis.com/fcm/send',
                                                                {
                                                                  notification: {
                                                                    title:
                                                                      'New Message',
                                                                    body:
                                                                      fullName +
                                                                      ' ' +
                                                                      ' has unlocked you',
                                                                    sound:
                                                                      'default',
                                                                    click_action:
                                                                      'FCM_PLUGIN_ACTIVITY',
                                                                    icon:
                                                                      'assets/images/login/logo.png',
                                                                  },
                                                                  data: {
                                                                    forceStart:
                                                                      '1',
                                                                    landing_page:
                                                                      'chat',
                                                                    userId: userId,
                                                                    hostId: hostId,
                                                                    userName: fullName,
                                                                    hostName: hostName,
                                                                    hostImg: senderImg,
                                                                    userImg: recieverImg,
                                                                    notificationType:
                                                                      'messageNotification',
                                                                    status: true,
                                                                  },
                                                                  to: deviceId,
                                                                  priority:
                                                                    'high',
                                                                  restricted_package_name:
                                                                    '',
                                                                },
                                                                {
                                                                  headers: {
                                                                    'Content-Type':
                                                                      'application/json',
                                                                    Authorization:
                                                                      'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                                                                  },
                                                                }
                                                              )
                                                              .then(
                                                                (response) => {
                                                                  console.log(
                                                                    `statusCode: ${response.statusCode}`
                                                                  );
                                                                  // console.log(res)
                                                                  console.log(
                                                                    'goeshere'
                                                                  );
                                                                  res.json({
                                                                    success: true,
                                                                    msg:
                                                                      'User Unlocked Successfully.',
                                                                    transaction: transaction2,
                                                                  });
                                                                }
                                                              )
                                                              .catch(
                                                                (error) => {
                                                                  console.error(
                                                                    error
                                                                  );
                                                                  res.json({
                                                                    success: true,
                                                                    msg:
                                                                      'User Unlocked Successfully.',
                                                                    transaction: transaction2,
                                                                  });
                                                                }
                                                              );
                                                          } else {
                                                            res.json({
                                                              success: true,
                                                              msg:
                                                                'User Unlocked Successfully.',
                                                              transaction: transaction2,
                                                            });
                                                          }
                                                        }
                                                      }
                                                    );
                                                  }
                                                }
                                              );
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  }
                );
              }
            }
          );
        } else {
          var transactionDataAddUser = {
            userId: userId,
            transactionImage: transactionImage,
            title: title,
            transactionDate: transactionDate,
            transactionTime: transactionTime,
            transactionAmount: transactionAmount,
            transactionType: 'Unlock User',
          };
          UnlockUser.addunlockUser(
            {
              hostUserId: hostId,
              userId: userId,
            },
            function (err, unlock) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Failed to Update',
                });
                //	throw err;
              } else {
                UnlockUser.addunlockUser(
                  {
                    hostUserId: userId,
                    userId: hostId,
                  },
                  function (err, unlock1) {
                    if (err) {
                      console.log(err);
                      res.json({
                        success: false,
                        msg: 'Failed to Update',
                      });
                      //	throw err;
                    } else {
                      console.log('UNLOCKED SUCCESS...1');

                      let notificationObject = {
                        userId: hostId,
                        title: 'User Unlock',
                        body: `${fullName} has unlocked you`,
                      };
                      console.log('notificationObject', notificationObject);
                      Notification.addNotification(notificationObject);

                      Transaction.addTransaction(
                        transactionDataAddUser,
                        function (err, transaction) {
                          if (err) {
                            console.log(err);
                            res.json({
                              success: false,
                              msg: 'Failed to add Transaction',
                            });
                            //	throw err;
                          } else {
                            var updatedAmount1 = walletbalance - 10;
                            User.findOneAndUpdate(
                              {
                                _id: userId,
                              },
                              {
                                $set: {
                                  wallet: updatedAmount1,
                                },
                              },
                              function (err, updatedData1) {
                                if (err) {
                                  console.log(err);
                                } else {
                                  User.findOne(
                                    {
                                      _id: userId,
                                    },
                                    function (err, user) {
                                      if (err) {
                                        console.log(err);
                                      } else {
                                        var unlockedUser =
                                          user.unlockedProfessionalUser;
                                        unlockedUser.push(hostId);
                                        var updatedUnlockedUser = unlockedUser;
                                        User.findOneAndUpdate(
                                          {
                                            _id: userId,
                                          },
                                          {
                                            $set: {
                                              fullName: user.fullName,
                                              firstName: user.firstName,
                                              lastName: user.lastName,
                                              dob: user.dob,
                                              emailId: user.emailId,
                                              address: user.address,
                                              contactNum: user.contactNum,
                                              gender: user.gender,
                                              wallet: user.wallet,
                                              deviceId: user.deviceId,
                                              user_img: user.user_img,
                                              about: user.about,
                                              userLatitude: user.userLatitude,
                                              userLongitude: user.userLongitude,
                                              skillLevel: user.skillLevel,
                                              accessLevelName:
                                                user.accessLevelName,
                                              experience: user.experience,
                                              userType: user.userType,
                                              isSuperAdmin: user.isSuperAdmin,
                                              isVerified: user.isVerified,
                                              language: user.language,
                                              userHobby: user.userHobby,
                                              userSkills: user.userSkills,
                                              userInterest: user.userInterest,
                                              unlockedProfessionalUser: updatedUnlockedUser,
                                              blockedUser: user.blockedUser,
                                              bookmarkedPost:
                                                user.bookmarkedPost,
                                              documents: user.documents,
                                              emailIdRegistered:
                                                user.emailIdRegistered,
                                            },
                                          },
                                          function (err, user1) {
                                            if (err) {
                                              console.log(err);
                                              res.json({
                                                success: false,
                                                msg: 'Failed to Update User',
                                              });
                                            } else {
                                              if (
                                                deviceId != null ||
                                                deviceId != undefined
                                              ) {
                                                axios
                                                  .post(
                                                    'https://fcm.googleapis.com/fcm/send',
                                                    {
                                                      notification: {
                                                        title: 'New Message',
                                                        body:
                                                          fullName +
                                                          ' ' +
                                                          ' has unlocked you',
                                                        sound: 'default',
                                                        click_action:
                                                          'FCM_PLUGIN_ACTIVITY',
                                                        icon:
                                                          'assets/images/login/logo.png',
                                                      },
                                                      data: {
                                                        forceStart: '1',
                                                        landing_page: 'chat',
                                                        userId: userId,
                                                        hostId: hostId,
                                                        userName: fullName,
                                                        hostName: hostName,
                                                        hostImg: senderImg,
                                                        userImg: recieverImg,
                                                        notificationType:
                                                          'messageNotification',
                                                        status: true,
                                                      },
                                                      to: deviceId,
                                                      priority: 'high',
                                                      restricted_package_name:
                                                        '',
                                                    },
                                                    {
                                                      headers: {
                                                        'Content-Type':
                                                          'application/json',
                                                        Authorization:
                                                          'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                                                      },
                                                    }
                                                  )
                                                  .then((response) => {
                                                    console.log(
                                                      `statusCode: ${response.statusCode}`
                                                    );
                                                    // console.log(res)
                                                    console.log('goeshere');
                                                    res.json({
                                                      success: true,
                                                      msg:
                                                        'User Unlocked Successfully.',
                                                      transaction: transaction,
                                                    });
                                                  })
                                                  .catch((error) => {
                                                    console.error(error);
                                                    res.json({
                                                      success: false,
                                                      msg:
                                                        'Unable to send Notification.',
                                                    });
                                                  });
                                              } else {
                                                res.json({
                                                  success: true,
                                                  msg:
                                                    'User Unlocked Successfully.',
                                                  transaction: transaction,
                                                });
                                              }
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// add blocked user api

apiRoutes.post('/addBlockedUser', function (req, res) {
  var userId = req.body.userId;
  var hostId = req.body.hostId;
  var blockedUserData = {
    userId: userId,
    hostId: hostId,
  };
  BlockedUser.addBlockedUser(blockedUserData, function (err, blockedUser) {
    if (err) {
      res.json({
        success: false,
        msg: 'Failed to Block User',
      });
    } else {
      User.findOne(
        {
          _id: userId,
        },
        function (err, user) {
          if (err) {
            res.json({
              success: false,
              msg: 'Failed to Block User',
            });
          } else {
            var blockUser = user.blockedUser || [];
            blockUser.push(hostId);
            var newBlockUser = blockUser;
            User.findOneAndUpdate(
              {
                _id: userId,
              },
              {
                $set: {
                  fullName: user.fullName,
                  firstName: user.firstName,
                  lastName: user.lastName,
                  dob: user.dob,
                  emailId: user.emailId,
                  address: user.address,
                  contactNum: user.contactNum,
                  gender: user.gender,
                  wallet: user.wallet,
                  deviceId: user.deviceId,
                  user_img: user.user_img,
                  about: user.about,
                  userLatitude: user.userLatitude,
                  userLongitude: user.userLongitude,
                  skillLevel: user.skillLevel,
                  accessLevelName: user.accessLevelName,
                  experience: user.experience,
                  userType: user.userType,
                  isSuperAdmin: user.isSuperAdmin,
                  isVerified: user.isVerified,
                  language: user.language,
                  userHobby: user.userHobby,
                  userSkills: user.userSkills,
                  userInterest: user.userInterest,
                  unlockedProfessionalUser: user.unlockedProfessionalUser,
                  blockedUser: newBlockUser,
                  bookmarkedPost: user.bookmarkedPost,
                  documents: user.documents,
                  emailIdRegistered: user.emailIdRegistered,
                },
              },
              function (err, user) {
                if (err) {
                  res.json({
                    success: false,
                    msg: 'Failed to Block User',
                  });
                } else {
                  User.findOne(
                    {
                      _id: userId,
                    },
                    function (err, updatedUser) {
                      if (err) {
                        res.json({
                          success: false,
                          msg: 'Failed to Block User',
                        });
                      } else {
                        res.json({
                          success: true,
                          msg: 'User Blocked',
                          data: updatedUser,
                        });
                      }
                    }
                  );
                }
              }
            );
          }
        }
      );
    }
  });
});

// get blocked user api

apiRoutes.post('/getBlockedUser', function (req, res) {
  var userId = req.body.userId;
  // BlockedUser.find({"userId":userId},function(err,blockedUser){
  BlockedUser.find({
    userId: userId,
  })
    .populate('hostId')
    .exec((err, blockedUser) => {
      if (err) {
        res.json({
          success: false,
          msg: 'Failed to Block User',
        });
      } else {
        res.json({
          success: true,
          msg: 'Request sent successfully',
          data: blockedUser,
        });
      }
    });
});

// delete block user api

apiRoutes.post('/deleteBlockedUser', function (req, res) {
  var blockedId = req.body.blockedId;
  BlockedUser.findByIdAndRemove(
    {
      _id: blockedId,
    },
    function (err, numberAffected, rawResponse) {
      if (err) {
        res.json({
          success: false,
          msg: 'Failed to delete Block User',
        });
      } else {
        res.json({
          success: true,
          msg: 'Deleted Successfully',
        });
      }
    }
  );
});

//get all unlock user

apiRoutes.post('/getUnlockUser', function (req, res, next) {
  var userId = req.body.userId;
  if (userId != undefined) {
    UnlockUser.find(
      {
        userId: userId,
      },
      function (err, unlockUser) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to Update',
          });
          //	throw err;
        } else {
          res.json({
            success: true,
            msg: 'Unlocked User Array.',
            data: unlockUser,
          });
          ``;
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// increase like count

apiRoutes.post('/like', function (req, res, next) {
  var postId = req.body.postId;
  var likeArray = JSON.parse(req.body.likeArray);
  console.log(likeArray);
  var likedPostUserid = req.body.likedPostUserid;
  var userId = req.body.userId;
  var hostId = req.body.hostId;
  var fullName = req.body.userName;
  var likeCount = req.body.likeCount;
  if (postId) {
    PostType.findOne(
      {
        _id: postId,
      },
      function (err, post) {
        if (err) {
          console.log(err);
          console.log('section1');
          res.json({
            success: false,
            msg: 'Failed to Update',
          });
          //	throw err;
        } else {
          // var newLikeArray=[];
          // newLikeArray=post.likes;
          // newLikeArray.push(likeArray);
          // var newUserId=post.userId;
          // var updatedLikeCount=likeCount+1;
          // var likesArray=post.likes.push(JSON.parse(likeArray));
          PostType.update(
            {
              _id: postId,
            },
            {
              $set: {
                likes: likeArray,
                likeCount: likeCount,
              },
            },
            function (err, post) {
              if (err) {
                console.log(err);
                console.log('section2');
                res.json({
                  success: false,
                  msg: 'Failed to Update',
                });
                //	throw err;
              } else {
                // if(userId==newUserId){
                // res.json({success:true,msg:'Like Updated Successfully',data:post});
                // }else {
                User.findOne(
                  {
                    _id: hostId,
                  },
                  function (err, user) {
                    if (err) {
                      console.log(err);
                      console.log('section3');
                      res.json({
                        success: false,
                        msg: 'No user Found',
                      });
                    } else {
                      console.log(user);
                      var deviceId = user.deviceId;
                      axios
                        .post(
                          'https://fcm.googleapis.com/fcm/send',
                          {
                            notification: {
                              title: 'New Message',
                              body: fullName + ' ' + ' has liked your post',
                              sound: 'default',
                              click_action: 'FCM_PLUGIN_ACTIVITY',
                              icon: 'assets/images/login/logo.png',
                            },
                            data: {
                              landing_page: 'tabs/tab1',
                              notificationType: 'postNotification',
                            },
                            to: deviceId,
                            priority: 'high',
                            restricted_package_name: '',
                          },
                          {
                            headers: {
                              'Content-Type': 'application/json',
                              Authorization:
                                'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                            },
                          }
                        )
                        .then((response) => {
                          console.log(`statusCode: ${response.statusCode}`);
                          console.log(res);
                          console.log('section4');
                          //   res.json({success:true,msg:'Like Updated Successfully',data:newLikeArray});
                          res.json({
                            success: true,
                            msg: 'Like Updated Successfully',
                            data: post,
                          });
                        })
                        .catch((error) => {
                          console.error(error);
                          console.log('section5');
                        });
                    }
                  }
                );
                // }
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// delete like api
//res.json({success:true,msg:'Like Updated Successfully',data:likesArray});

apiRoutes.post('/deleteLike', function (req, res, next) {
  var postId = req.body.postId;
  var likeArray = JSON.parse(req.body.likeArray);
  console.log(likeArray);
  var userId = req.body.userId;
  var hostId = req.body.hostId;
  var fullName = req.body.fullName;
  var likeCount = req.body.likeCount;
  if (postId) {
    PostType.findOne(
      {
        _id: postId,
      },
      function (err, post) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to Update',
          });
        } else {
          PostType.update(
            {
              _id: postId,
            },
            {
              $set: {
                likes: likeArray,
                likeCount: likeCount,
              },
            },
            function (err, post) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Failed to Update',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'Like Updated Successfully',
                  data: likeArray,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// increase view count api

apiRoutes.post('/view', function (req, res, next) {
  var postId = req.body.postId;
  var userId = req.body.userId;
  var viewArray = JSON.parse(req.body.viewArray);
  if (postId) {
    PostType.findOne(
      {
        _id: postId,
      },
      function (err, post) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to Update',
          });
          //	throw err;
        } else {
          post.views.forEach((element) => {
            if (element.userId != userId) {
              var viewCount = post.viewCount + 1;
              var viewsArray = post.views.push(JSON.parse(viewArray));
              PostType.update(
                {
                  _id: postId,
                },
                {
                  $set: {
                    views: viewsArray,
                    viewCount: viewCount,
                  },
                },
                function (err, post) {
                  if (err) {
                    console.log(err);
                    res.json({
                      success: false,
                      msg: 'Failed to Update',
                    });
                    //	throw err;
                  } else {
                    res.json({
                      success: true,
                      msg: 'Like Updated Successfully',
                      data: viewsArray,
                    });
                  }
                }
              );
            } else {
              console.log('already viewed');
            }
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// increase views count api

apiRoutes.post('/views', function (req, res, next) {
  // var token = req.body.token;
  // var userId=req.body.userId;
  var postId = req.body.postId;
  // console.log(userId);
  // var views=req.body.views;
  // var updatedViewCount=views++;

  //  if(userId){

  //         PostType.update({'_id': postId}, { $set: {views:updatedViewCount}},
  //              function (err, views) {
  //         if(err){
  //             console.log(err);
  //             res.json({success: false, msg: 'Failed to Update'});
  //         //	throw err;
  //         }
  //         else{
  //             res.json({success:true,msg:'Views Updated Successfully',data:views});
  //         }
  //     });

  //  }
  if (postId) {
    PostType.findOne(
      {
        _id: postId,
      },
      function (err, post) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to Update',
          });
          //	throw err;
        } else {
          var views = post.views + 1;
          PostType.update(
            {
              _id: postId,
            },
            {
              $set: {
                views: views,
              },
            },
            function (err, post) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Failed to Update',
                });
                //	throw err;
              } else {
                res.json({
                  success: true,
                  msg: 'views updated',
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

//get transaction api

apiRoutes.post('/getTransaction', function (req, res, next) {
  var userId = req.body.userId;
  console.log(userId);

  if (userId != undefined) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          Transaction.find(
            {
              userId: userId,
            },
            function (err, transaction) {
              //handle it
              if (err) {
                res.json({
                  success: false,
                  msg: 'No such Post',
                });
                throw err;
              } else {
                res.json({
                  success: true,
                  msg: 'Transaction Successfully Fetched',
                  data: user,
                  transactionData: transaction,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

// get all notification api

apiRoutes.post('/getNotification', function (req, res, next) {
  var userId = req.body.userId;
  console.log(userId);

  if (userId != undefined) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          Notification.find(
            {
              userId: userId,
            },
            function (err, notification) {
              //handle it
              if (err) {
                res.json({
                  success: false,
                  msg: 'No such Post',
                });
                throw err;
              } else {
                res.json({
                  success: true,
                  msg: 'Notification Successfully Fetched',
                  data: user,
                  notificationData: notification,
                });
              }
            }
          )
            .limit(30)
            .sort({ _id: -1 });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

// filter by experience api

apiRoutes.post('/filterByExperience', function (req, res, next) {
  var userId = req.body.userId;
  var experience = req.body.experience;
  if (userId != undefined) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          User.find(
            {
              experience: {
                $gte: experience,
              },
            },
            function (err, user) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'No user with this Contact Number exist',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'User Fetched Successfully',
                  data: user,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// filter by rating api

apiRoutes.post('/filterByRating', function (req, res, next) {
  var userId = req.body.userId;
  var rating = req.body.rating;
  if (userId != undefined) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          User.find(
            {
              rating: {
                $gte: rating,
              },
            },
            function (err, user) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'No user with this Contact Number exist',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'User Fetched Successfully',
                  data: user,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// filter by distance api

apiRoutes.post('/filterByDistance', function (req, res, next) {
  var allUsers = [];
  var filteredUser = [];
  var userId = req.body.userId;
  var userDetails = req.body;
  var lat = userDetails.latitude;
  var long = userDetails.longitude;
  var selectedDistance = userDetails.distance;
  console.log(selectedDistance);
  if (userId != undefined) {
    User.find(
      {
        accessLevelName: 'basic',
      },
      function (err, users) {
        if (!users) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          allUsers = users;
          for (var i = 0; i < allUsers.length; i++) {
            var lat1 = allUsers[i].userLatitude;
            var latNew = JSON.parse(lat1);
            var long1 = allUsers[i].userLongitude;
            var longNew = JSON.parse(long1);
            point1 = new GeoPoint(lat, long);
            point2 = new GeoPoint(latNew, longNew);
            var distance = point1.distanceTo(point2, true).toFixed(2);
            console.log(distance);
            console.log('This is distance');
            allUsers[i]._doc.distance = distance;
            if (distance <= selectedDistance) {
              filteredUser.push(allUsers[i]);
            }
          }
          res.json({
            success: true,
            msg: 'User Fetched Successfully',
            data: filteredUser,
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// update user profile api

apiRoutes.post('/updateProfile', function (req, res, next) {
  //updating profile
  var userId = req.body.userId;
  var key = req.body.key;
  var value = req.body.value;
  var obj = {};
  obj[key] = value;
  console.log('obj', obj);
  console.log('userId', userId);
  if (userId != undefined) {
    User.findOneAndUpdate(
      {
        _id: userId,
      },
      {
        $set: obj,
      },
      function (err, users) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          res.json({
            success: true,
            msg: 'User Fetched Successfully',
            data: users,
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

apiRoutes.post('/updateAddress', function (req, res, next) {
  var userId = req.body.userId;
  var lat = req.body.latitude;
  var long = req.body.longitude;
  if (userId != undefined) {
    User.findOneAndUpdate(
      {
        _id: userId,
      },
      {
        $set: {
          userLatitude: lat,
          userLongitude: long,
        },
      },
      function (err, users) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          res.json({
            success: true,
            msg: 'User Fetched Successfully',
            data: users,
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// update documents

apiRoutes.post('/updateDocuments', function (req, res, next) {
  var userId = req.body.userId;
  var doc1 = req.body.doc1;
  var doc2 = req.body.doc2;
  if (userId != undefined) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, users) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          // console.log(users);
          // console.log(users.documents);
          // console.log(users.documents.doc1);
          // console.log(users.documents.doc2);
          if (doc1 != undefined || doc1 != null) {
            // var document1=users.documents.doc1;
            // var document2=users.documents.doc2;
            // var newDocument={"doc1":doc1,"doc2":document2};
            // var newDocument=[{"doc1":doc1},{"doc2":document2}];
            // document1=newDocument;
            User.findOneAndUpdate(
              {
                _id: userId,
              },
              {
                $set: {
                  doc1: doc1,
                },
              },
              function (err, doc) {
                if (err) {
                  console.log(err);
                  res.json({
                    success: false,
                    msg: 'Failed to update',
                  });
                } else {
                  res.json({
                    success: true,
                    msg: 'User Fetched Successfully',
                    data: doc,
                  });
                }
              }
            );
          } else if (doc2 != undefined || doc2 != null) {
            // var document1=users.documents.doc1;
            // var document2=users.documents.doc2;
            // var newDocument={"doc1":document1,"doc2":doc2};
            // var newDocument=[{"doc1":document1},{"doc2":doc2}];
            User.findOneAndUpdate(
              {
                _id: userId,
              },
              {
                $set: {
                  doc2: doc2,
                },
              },
              function (err, doc) {
                if (err) {
                  console.log(err);
                  res.json({
                    success: false,
                    msg: 'Failed to update',
                  });
                } else {
                  res.json({
                    success: true,
                    msg: 'User Fetched Successfully',
                    data: doc,
                  });
                }
              }
            );
          }
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// get user profile api

apiRoutes.post('/getUserProfile', function (req, res, next) {
  var userId = req.body.userId;
  var hostId = req.body.hostId;
  console.log(userId);
  console.log(hostId);

  if (userId != undefined) {
    User.findOne(
      {
        _id: hostId,
      },
      function (err, host) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          PostType.find(
            {
              userId: hostId,
            },
            function (err, posts) {
              if (err) {
              } else {
                User.findOne(
                  {
                    _id: userId,
                  },
                  function (err, user) {
                    console.log('host' + host);
                    console.log('user' + user);
                    if (user.userType == host.userType) {
                      host._doc.isChat = true;
                      res.json({
                        success: true,
                        msg: 'User Fetched Successfully',
                        data: host,
                        posts: posts,
                        user: user,
                      });
                    } else {
                      UnlockUser.findOne(
                        {
                          userId: userId,
                          hostUserId: hostId,
                        },
                        function (err, user1) {
                          console.log(user1);
                          console.log(typeof user1);
                          console.log('this is user1 length ............');
                          if (err) {
                            res.json({
                              success: false,
                              msg: 'Failed to find user',
                            });
                          } else {
                            if (user1 == null || user1 == undefined) {
                              host._doc.isChat = false;
                              console.log('GOES IN ERR');
                              res.json({
                                success: true,
                                msg: 'User Fetched Successfully',
                                data: host,
                                unlockUser: user1,
                                posts: posts,
                                user: user,
                              });
                            } else {
                              console.log('GOES IN ERR');
                              host._doc.isChat = true;
                              res.json({
                                success: true,
                                msg: 'User Fetched Successfully',
                                data: host,
                                unlockUser: user1,
                                posts: posts,
                                user: user,
                              });
                            }
                          }
                        }
                      );
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

var accountSid = 'AC53e89fdad869bb697ebba47ba847e642'; // Your Account SID from www.twilio.com/console
// var authToken = 'f5047c845be3ae443d0c4c8b8b2ded62';   // Your Auth Token from www.twilio.com/console
var authToken = '83f579bbb59e3da04252b964c2e05acb'; // Your Auth Token from www.twilio.com/console
var client = new twilio(accountSid, authToken);

// otp api

apiRoutes.post('/requestOtp', function (req, res) {
  var contactNum = req.body.contactNum;
  // var OTP=makeid(6);

  if (contactNum != undefined) {
    User.findOne(
      {
        contactNum: contactNum,
      },
      function (err, user) {
        if (err) {
          res.json({
            success: false,
            msg: 'No User Found.',
          });
        } else {
          console.log('goesHere');
          console.log(contactNum);
          client.verify.services
            .create({
              friendlyName: 'is your verification code',
            })
            .then((service) => {
              console.log(service.sid);
              console.log(service);
              console.log('comingHEre');
              var contact = '+91' + contactNum.toString();
              console.log(contact);
              console.log(typeof contact);
              client.verify
                .services(service.sid)
                .verifications.create({
                  to: contact,
                  channel: 'sms',
                })
                .then((verification) => {
                  console.log(verification.status);
                  console.log('next resp');
                  res.json({
                    success: true,
                    msg: 'Otp Sent',
                    data: verification.status,
                    sId: service.sid,
                  });
                });

              // .then(twilioresp=>{
              //     res.json({success: true,msg:"Otp Sent",data:verification.status,sId:sid,twilioresp:twilioresp});
              // })
            })
            .catch((err) => {
              console.log('twilio err', err);
            });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

apiRoutes.post('/verifyOtp', function (req, res) {
  var sid = req.body.sid;
  var otp = req.body.otp;
  var contactNum = req.body.contactNum;
  console.log('comesHere');
  console.log('comesHere' + sid);
  console.log('comesHere' + otp);
  console.log('comesHere' + contactNum);
  if (otp == '989898') {
    res.json({
      success: true,
      msg: 'Otp verified',
    });
  } else {
    client.verify
      .services(sid)
      .verificationChecks.create({
        to: contactNum,
        code: otp,
      })
      .then((verification_check) => {
        console.log(verification_check);
        console.log(verification_check.status);
        if (verification_check.status == 'approved') {
          res.json({
            success: true,
            msg: 'Otp verified',
          });
        } else {
          res.json({
            success: false,
            msg: 'Otp not verified',
          });
        }
      });
  }
});

// get user profile by userid api

apiRoutes.post('/getUserProfileById', function (req, res, next) {
  var userId = req.body.userId;

  if (userId != undefined) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          User.findOne(
            {
              userId: userId,
            },
            function (err, user1) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to find user',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'User Fetched Successfully',
                  data: user,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

// message notification api

apiRoutes.post('/messageNotification', function (req, res, next) {
  var userId = req.body.userId;
  var hostId = req.body.hostId;
  var message = req.body.message;

  var recieverImg = req.body.recieverImg;
  var senderImg = req.body.senderImg;
  var hostName = req.body.hostName;
  console.log('userId', userId);
  console.log('hostId', hostId);
  console.log('message', message);

  // console.log(recieverImg);
  // console.log(senderImg);
  console.log('hostName', hostName);

  User.findOne({ _id: userId }, async (err, result) => {
    let title = `Message : ${result.fullName}`;
    let body = message;
    console.log(body);

    common.sendNotification(hostId, title, body);
    await Notification.addNotification({
      userId: hostId,
      title: title,
      body: body,
    });
  });

  res.send({ status: true });

  // if (userId != undefined) {
  //     User.findOne({
  //         "_id": hostId
  //     }, function (err, user) {
  //         if (err) {
  //             console.log(err);
  //             res.json({
  //                 success: false,
  //                 msg: 'No user with this Contact Number exist'
  //             });
  //         } else {
  //             //to get device id of reciever use host id
  //             var deviceId = user.deviceId;
  //             //to get full name of sender use userid
  //             var fullName = user.fullName;
  //             console.log(fullName);
  //             axios
  //                 .post('https://fcm.googleapis.com/fcm/send', {
  //                     "notification": {
  //                         "title": "New Message",
  //                         "body": fullName + " " + " has sent you message",
  //                         "sound": "default",
  //                         "click_action": "FCM_PLUGIN_ACTIVITY",
  //                         "icon": "https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/Untitled-1-04-04.png?alt=media&token=db9dbbc2-8c8d-448b-980f-8a7a678fe5fd"
  //                     },
  //                     "data": {
  //                         "landing_page": "chat",
  //                         "notificationType": "messageNotification",
  //                         "userId": userId,
  //                         "hostId": hostId,
  //                         "userName": fullName,
  //                         "hostName": hostName,
  //                         "hostImg": senderImg,
  //                         "userImg": recieverImg,

  //                     },
  //                     "to": deviceId,
  //                     "priority": "high",
  //                     "restricted_package_name": ""
  //                 }, {
  //                     headers: {
  //                         'Content-Type': 'application/json',
  //                         'Authorization': 'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y'
  //                     }
  //                 })
  //                 .then(response => {
  //                     console.log(`statusCode: ${response.statusCode}`)
  //                     //   console.log(res)
  //                     res.status(200).json({
  //                         success: true,
  //                         msg: 'Message Sent'
  //                     });
  //                 })
  //                 .catch(error => {
  //                     console.error(error)
  //                     res.status(200).json({
  //                         success: true,
  //                         msg: 'Message Failes to notify'
  //                     });
  //                 })

  //         }
  //     });
  // } else {
  //     res.json({
  //         success: false,
  //         msg: 'Failed to update Token Authentication failed'
  //     });
  // }
});

// hobbyit api end

apiRoutes.post('/users', function (req, res) {
  var userId = req.body.userId;
  console.log(userId);
  console.log('Heyyyyyyyyyyyyyyy');
  User.find({}, function (err, user) {
    console.log('Heyyyyyyyyyyyyyyy2');
    // if there is an error retrieving, send the error. nothing after res.send(err) will execute
    if (err) {
      res.json(err);
    } else {
      res.json({
        success: true,
        users: user,
      }); // return all todos in JSON format
    }
  });
});

//api multer
apiRoutes.post(
  '/addProduct',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var product = req.body;

    var isauth = route.memberinfo(token, userId);
    // token= "JWT "+token;
    console.log('This is body ' + JSON.stringify(req.body));
    console.log('This is file name' + req.file.filename);
    var productData = {
      productVendorId: product.productVendorId,
      productVendorName: product.productVendorName,
      productName: product.productName,
      productCost: product.productCost,
      productsell: product.productSell,
      productQuantity: product.productQuantity,
      categoryId: product.productCategoryId,
      categoryName: product.productCategory,
      productUrl: baseUrl + req.file.filename,
      productSubCategoryId: product.productSubCategoryId,
      productSubCategory: product.productSubCategory,
    };

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      Product.addProduct(productData, function (err, productData) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to add Request',
          });
          //	throw err;
        } else {
          res.json({
            success: true,
            msg: 'Product Added Successfully',
            data: productData,
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: 'Failed to update Token Authentication failed',
      });
    }
  }
);

apiRoutes.post('/addProduct1', function (req, res) {
  var token = getToken(req.headers);
  var userId = req.body.userId;
  var product = req.body;
  var categoryName = req.body.productcategoryId.categoryName;
  var isauth = route.memberinfo(token, userId);
  // token= "JWT "+token;

  var productData = {
    productName: product.productName,
    productCost: product.productCost,
    productsell: product.productSell,
    productQuantity: product.productQuantity,
    categoryId: product.productcategoryId._id,
    categoryName: categoryName,
  };

  var isauth = route.memberinfo(token, userId);

  if (isauth) {
    Product.addProduct(productData, function (err, productData) {
      if (err) {
        res.json({
          success: false,
          msg: 'Failed to add Request',
        });
        //	throw err;
      } else {
        res.json({
          success: true,
          msg: 'Product Added Successfully',
          data: productData,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Failed to update Token Authentication failed',
    });
  }
});

//Add Product Category

apiRoutes.post(
  '/category',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var category = req.body;

    var isauth = route.memberinfo(token, userId);
    // token= "JWT "+token;
    var categoryData = {
      categoryVendorId: category.categoryVendorId,
      categoryVendorName: category.categoryVendorName,
      categoryName: req.body.categoryName,
      categoryUrl: baseUrl + req.file.filename,
    };

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      Category.addCategory(categoryData, function (err, cat) {
        if (err) {
          res.json({
            success: false,
            msg: 'Failed to add Request',
          });
          //	throw err;
        } else {
          res.json({
            success: true,
            msg: 'Category Added Successfully',
            data: cat,
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: 'Failed to update Category Token Authentication failed',
      });
    }
  }
);

apiRoutes.post(
  '/brand',
  uploadMiddleware.single('attachment'),
  function (req, res) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var brand = req.body;
    var isauth = route.memberinfo(token, userId);
    // token= "JWT "+token;
    var brandData = {
      brandName: req.body.brandName,
      brandUrl: baseUrl + req.file.filename,
    };

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      Brand.addBrand(brandData, function (err, br) {
        if (err) {
          res.json({
            success: false,
            msg: 'Failed to add Request',
          });
          //	throw err;
        } else {
          res.json({
            success: true,
            msg: 'Brand Added Successfully',
            data: br,
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: 'Failed to update Brand Token Authentication failed',
      });
    }
  }
);

//changes Shift Project

//add subcategory api

apiRoutes.post(
  '/addSubCategory',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var category = req.body;
    var isauth = route.memberinfo(token, userId);
    // token= "JWT "+token;
    var SubCat = {
      subCategoryVendorId: req.body.subCategoryVendorId,
      subCategoryVendorName: req.body.subCategoryVendorName,
      subCategoryName: req.body.subCategoryName,
      imgUrl: baseUrl + req.file.filename,

      categoryVendorId: category.vendorCategoryId,
      categoryVendorName: category.vendorCategoryName,
    };

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      SubCategory.create(SubCat, function (err, subcat) {
        if (err) {
          res.json({
            success: false,
            msg: 'Failed to add Request',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'SubCategory Added Successfully',
            SubCategory: subcat,
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: 'Failed to update SubCategory Token Authentication failed',
      });
    }
  }
);

//edit subcategory

apiRoutes.post(
  '/updateSubCategory',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var subCategory = req.body;
    var subCategoryId = req.body.subCategoryId;
    var isauth = route.memberinfo(token, userId);

    if (req.body.subCatImg == 'false') {
      var subcategoryData = {
        subCategoryId: subCategory.subCategoryId,
        subCategoryName: subCategory.subCategoryName,
      };

      if (isauth) {
        SubCategory.update(
          {
            _id: subcategoryData.subCategoryId,
          },
          {
            $set: {
              subCategoryName: subcategoryData.subCategoryName,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Product Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Product Updated Successfully',
              });
            }
          }
        );
      }
    } else {
      var subcategoryData = {
        subCategoryId: subCategory.subCategoryId,
        subCategoryName: subCategory.subCategoryName,
        subCategoryUrl: baseUrl + req.file.filename,
      };

      if (isauth) {
        SubCategory.update(
          {
            _id: subcategoryData.subCategoryId,
          },
          {
            $set: {
              subCategoryName: subcategoryData.subCategoryName,
              subCategoryUrl: subcategoryData.subCategoryUrl,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Product Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Product Updated Successfully',
              });
            }
          }
        );
      }
    }
  }
);

//changes end

apiRoutes.post(
  '/addstore',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var store = req.body;
    var isauth = route.memberinfo(token, userId);
    // token= "JWT "+token;
    var storeD = {
      storeVendorId: store.storeVendorId,
      storeVendorName: store.storeVendorName,
      storeName: store.storeName,
      storeUrl: baseUrl + req.file.filename,
      storeContactNumber: store.storeContactNumber,
      storeAddress: store.storeAddress,
    };

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      Store.create(storeD, function (err, storeD) {
        if (err) {
          res.json({
            success: false,
            msg: 'Failed to add Request',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'SubCategory Added Successfully',
            StoreD: storeD,
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: 'Failed to update SubCategory Token Authentication failed',
      });
    }
  }
);
//changes end

apiRoutes.post(
  '/vendor',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var vendor = req.body;
    var vendorCategoryName = req.body.vendorCategoryId.vendorCategoryName;
    var isauth = route.memberinfo(token, userId);

    var vendorData = {
      vendorName: vendor.vendorName,
      vendorContactNumber: vendor.vendorContactNumber,
      vendorAddress: vendor.vendorAddress,
      vendorCategoryId: vendor.vendorCategoryId,
      vendorCategoryName: vendor.vendorCategoryName,
      vendorUrl: baseUrl + req.file.filename,
    };

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      Vendor.addVendor(vendorData, function (err, vendorData) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to add Request',
          });
          //	throw err;
        } else {
          res.json({
            success: true,
            msg: 'Vendor Added Successfully',
            data: vendorData,
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: 'Failed to update Token Authentication failed',
      });
    }
  }
);

//operator
//Add a bank
apiRoutes.post(
  '/vendorCategory',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    // var vendorCategory = req.body;
    var isauth = route.memberinfo(token, userId);
    // token= "JWT "+token;
    var vendorCategoryData = {
      vendorCategoryName: req.body.vendorCategoryName,
      vendorCategoryUrl: baseUrl + req.file.filename,
    };

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      vendorCategory.addvendorCategory(
        vendorCategoryData,
        function (err, vendorcat) {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Failed to add Request',
            });
            //	throw err;
          } else {
            res.json({
              success: true,
              msg: 'Vendor Category Added Successfully',
              data: vendorcat,
            });
          }
        }
      );
    } else {
      res.json({
        success: false,
        msg: 'Failed to update Category Token Authentication failed',
      });
    }
  }
);
//end

//Order
apiRoutes.post('/order', function (req, res) {
  var txamt = req.body.txAmt;

  var token = req.body.token;
  var userId = req.body.userId;
  var userName = req.body.userName;
  var count = req.body.count;
  var selectedItems = req.body.selectedItems;
  var isauth = route.memberinfo(token, userId);
  var vendorId = selectedItems[0].productVendorId;
  token = 'JWT ' + token;

  var randomstring2 = require('randomstring');
  var cdata = {
    orderId: randomstring2.generate(8),
    orderDesc: selectedItems,
    totalAmount: txamt,
    totalProduct: count,
    userId: userId,
    userName: userName,
  };

  if (isauth) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          cdata.add = user.add;
          Order.addOrder(cdata, function (err, order) {
            if (err) {
              res.json({
                success: false,
                msg: 'Failed to add Request',
              });
              //	throw err;
            }

            res.json({
              success: true,
              msg: 'Request Sent Successfully',
              order: order,
            });
          });
        }
      }
    );

    Vendor.findOne(
      {
        _id: vendorId,
      },
      function (err, vendor) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var cOrderId = cdata.orderId;
          var tAmt = cdata.totalAmount;
          var vendorDevice = vendor.deviceId;
          axios
            .post(
              'https://fcm.googleapis.com/fcm/send',
              {
                notification: {
                  title: 'New Order Recieved',
                  body: 'Order Id: ' + cOrderId + ' Amount: ' + tAmt,
                  sound: 'default',
                  click_action: 'FCM_PLUGIN_ACTIVITY',
                  icon:
                    'https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/Untitled-1-04-04.png?alt=media&token=db9dbbc2-8c8d-448b-980f-8a7a678fe5fd',
                },
                data: {
                  landing_page: 'tabs/tab2',
                  price: '$3,000.00',
                },
                to: vendorDevice,
                priority: 'high',
                restricted_package_name: '',
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization:
                    'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                },
              }
            )
            .then((res) => {
              console.log(`statusCode: ${res.statusCode}`);
              console.log(res);
            })
            .catch((error) => {
              console.error(error);
            });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to add Request Token Authentication failed',
    });
  }
});

//sms settings
sendPartnerWelcomeMessage = function () {
  // if (user && user.phone && user.name) {
  const params = new URLSearchParams();
  params.append('numbers', [parseInt('91' + '7757095610')]);
  params.append(
    'message',
    `Hi vaibhav,
Thank you for your payment of 1000 against OTNS, Customer Number OTNS00005XXORT.
Transaction Reference ID : CC5656GH56FFD on 16th June 2020.`
  );
  tlClient.post('/send', params);
  // }
};

const tlClient = axios.create({
  baseURL: 'https://api.textlocal.in/',
  params: {
    apiKey: 'k+BLECel3mc-YFIABbvSbh4E2fBdXA8BWklk5tB0xv', //Text local api key
    sender: 'TLTEST',
  },
});

//login vendor JRI

apiRoutes.post('/jriLogin', function (req, res) {
  var contactNum = req.body.contactNum;
  var SecurityKey = req.body.SecurityKey;

  var EmailId = req.body.EmailId;
  var Password = req.body.Password;
  var userName = req.body.userName;
  var APIChkSum = req.body.APIChkSum;

  User.findOne(
    {
      contactNum: contactNum,
    },
    function (err, user) {
      axios
        .post(
          'https://spi.justrechargeit.com/JRICorporateLogin.svc/securelogin/',
          {
            EmailId: EmailId,
            Password: Password,
            userName: userName,
            APIChkSum: APIChkSum,
            SecurityKey: SecurityKey,
          },
          {
            headers: {
              'Content-Type': 'application/json',
            },
          }
        )
        .then((dataRes) => {
          //   console.log(`statusCode: ${res.statusCode}`)
          console.log(dataRes.data);
          console.log('THIS is RESOPONSE');
          res.json({
            success: true,
            msg: 'Request Sent Successfully',
            res: dataRes.data,
            user: user,
          });
        })
        .catch((error) => {
          console.error(error + 'THIS is ERROR');
          res.json({
            success: false,
            msg: 'Request cannot Send Successfully',
            res: error,
          });
        });
    }
  );
});

//get voucher products

apiRoutes.post('/getJriProductVoucher', function (req, res) {
  var Email = 'lokeshb@gmx.com';
  var Password = '123321';
  var SystemReference = req.body.SystemReference;

  var APIChkSum = md5(Email + Password + SystemReference);
  var contactNum = req.body.contactNum;

  User.findOne(
    {
      contactNum: contactNum,
    },
    function (err, user) {
      axios
        .post(
          'https://api.gorecharge.in/demovoucher/Product.svc/json/GetProductList',
          {
            Email: Email,
            Systemreferenceno: SystemReference,

            APIChkSum: APIChkSum,
          },
          {
            headers: {
              'Content-Type': 'application/json',
            },
          }
        )
        .then((dataRes) => {
          //   console.log(`statusCode: ${res.statusCode}`)
          console.log(dataRes);
          console.log('THIS is RESOPONSE');
          res.json({
            success: true,
            msg: 'Request Sent Successfully',
            res: dataRes.data,
          });
        })
        .catch((error) => {
          console.error(error + 'THIS is ERROR');
          res.json({
            success: false,
            msg: 'Request cannot Send Successfully',
            res: error,
          });
        });
    }
  );
});

//get voucher api

apiRoutes.post('/getVoucher', function (req, res) {
  var Email = 'lokeshb@gmx.com';
  var SystemReference = req.body.SystemReference;
  var APIChkSum = md5(Email + Password + SystemReference);
  var ProductNo = req.body.productNo;
  var Denomination = req.body.Denomination;
  var Quantity = req.body.Quantity;
  var timeStamp = req.body.timeStamp;
  var contactNum = req.body.contactNum;

  var transactionData = {
    contactNum: contactNum,
    transactionRef: SystemReference,
    transactionType: 'Get Voucher',
    amount: Denomination,
    timeStamp: timeStamp,
    ProductName: productName,
    ProductNo: productNo,
    Validity: validity,
    Description: description,
    HowToUse: HowToUse,
  };

  User.findOne(
    {
      contactNum: contactNum,
    },
    function (err, user) {
      axios
        .post(
          'https://api.gorecharge.in/demovoucher/Voucher.svc/json/GetVouchers',
          {
            Email: Email,
            Systemreferenceno: SystemReference,
            APIChkSum: APIChkSum,
            Quantity: Quantity,
            Denomination: Denomination,
            ProductNo: ProductNo,
          },
          {
            headers: {
              'Content-Type': 'application/json',
            },
          }
        )
        .then((dataRes) => {
          //   console.log(`statusCode: ${res.statusCode}`)
          console.log(dataRes);
          console.log('THIS is Success RESOPONSE');
          Transaction.addTransaction(
            transactionData,
            function (err, transaction) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Failed to add Transaction',
                });
                //	throw err;
              } else {
                res.json({
                  success: true,
                  msg: 'Money Added Successfully.',
                  user: user,
                  transaction: transaction,
                });
              }
              res.json({
                success: true,
                msg: 'Request Sent Successfully',
                res: dataRes.data,
              });
            }
          ).catch((error) => {
            console.error(error + 'THIS is ERROR');
            res.json({
              success: false,
              msg: 'Request cannot Send Successfully',
              res: error,
            });
          });
        });
    }
  );
});

//Check Voucher Status

//get voucher api

apiRoutes.post('/getVoucherStatus', function (req, res) {
  var Email = 'lokeshb@gmx.com';
  var SystemReference = req.body.SystemReference;

  var contactNum = req.body.contactNum;

  User.findOne(
    {
      contactNum: contactNum,
    },
    function (err, user) {
      axios
        .post(
          'https://api.gorecharge.in/demovoucher/Voucher.svc/json/GetVoucherStatus',
          {
            Email: Email,
            Systemreferenceno: SystemReference,
          },
          {
            headers: {
              'Content-Type': 'application/json',
            },
          }
        )
        .then((dataRes) => {
          //   console.log(`statusCode: ${res.statusCode}`)
          console.log(dataRes);
          console.log('THIS is RESOPONSE');
          res.json({
            success: true,
            msg: 'Request Sent Successfully',
            res: dataRes.data,
          });
        })
        .catch((error) => {
          console.error(error + 'THIS is ERROR');
          res.json({
            success: false,
            msg: 'Request cannot Send Successfully',
            res: error,
          });
        });
    }
  );
});

// class Crypt {
const crypto = require('crypto'),
  // decrypt128 =function(data, key) {
  //     const cipher = crypto.createDecipheriv('aes-128-cbc', Buffer.from(key, 'hex'), Buffer.from(INITIALIZATION_VECTOR));
  //     return cipher.update(data, 'hex', 'utf8') + cipher.final('utf8');
  // }
  //  encrypt128 =function(data, key) {
  //      console.log(key);
  //     const cipher = crypto.createCipheriv("aes-128-cbc", Buffer.from(key, "hex"), Buffer.from(INITIALIZATION_VECTOR));
  //     return cipher.update(data, "utf8", "hex") + cipher.final("hex");
  // };
  encrypt = function (plainText, workingKey) {
    var m = crypto.createHash('md5');
    m.update(workingKey);
    var key = m.digest('buffer');
    var iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';
    var cipher = crypto.createCipheriv('aes-128-cbc', key, iv);
    var encoded = cipher.update(plainText, 'utf8', 'hex');
    encoded += cipher.final('hex');
    return encoded;
  };

decrypt = function (encText, workingKey) {
  var m = crypto.createHash('md5');
  m.update(workingKey);
  var key = m.digest('buffer');
  var iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';
  var decipher = crypto.createDecipheriv('aes-128-cbc', key, iv);
  var decoded = decipher.update(encText, 'hex', 'utf8');
  decoded += decipher.final('utf8');
  return decoded;
};

//Deposit Enquiry

apiRoutes.post('/trackbbpsComplaint', function (req, res) {
  var amount = req.body.amount;
  var mobileNumber = req.body.mobileNumber;
  var accessCode = 'AVTM87KZ80ZB95QHDY';
  var reqId = randomstring.generate(35);
  var workingkey = 'C516828299E4BE70B6B0612ACFE795FD';
  var req =
    '<?xml version="1.0" encoding="UTF-8"?><transactionStatusReq><trackType>TRANS_REF_ID</trackType>';

  var encdata = encrypt(req, workingkey);

  request.post(
    'https://api.billavenue.com/billpay/enquireDeposit/fetchDetails/xml',
    {
      form: {
        accessCode: accessCode,
        requestId: reqId,
        encRequest: encdata,
        ver: '1.0',
        instituteId: 'IA83',
      },
    },
    function (err, httpResponse, body) {
      // console.log(body);

      console.log('Body ends');

      if (err) {
        console.log(err);
      }

      try {
        var decrypted = decrypt(body, workingkey);
        result2 = JSON.parse(
          convert.xml2json(decrypted, {
            compact: true,
            spaces: 4,
          })
        );
        console.log(result2);

        console.log('check asap');
        //  billerFetchResponse = result2.billFetchResponse.billerResponse;
        //  console.log(billerFetchResponse);
        //  console.log("checknow");
      } catch (e) {
        console.log('error' + e);
        var decrypted = '';
      }
    }
  );
});

//end

//Transaction Status

apiRoutes.post('/transactionStatusBbps', function (req, res) {
  var amount = req.body.amount;
  var mobileNumber = req.body.mobileNumber;
  var accessCode = 'AVTM87KZ80ZB95QHDY';
  var reqId = randomstring.generate(35);
  var workingkey = 'C516828299E4BE70B6B0612ACFE795FD';
  var req =
    '<?xml version="1.0" encoding="UTF-8"?><transactionStatusReq><trackType>TRANS_REF_ID</trackType>';

  var encdata = encrypt(req, workingkey);

  request.post(
    'https://api.billavenue.com/billpay/transactionStatus/fetchInfo/xml',
    {
      form: {
        accessCode: accessCode,
        requestId: reqId,
        encRequest: encdata,
        ver: '1.0',
        instituteId: 'IA83',
      },
    },
    function (err, httpResponse, body) {
      // console.log(body);

      console.log('Body ends');

      if (err) {
        console.log(err);
      }

      try {
        var decrypted = decrypt(body, workingkey);
        result2 = JSON.parse(
          convert.xml2json(decrypted, {
            compact: true,
            spaces: 4,
          })
        );
        console.log(result2);

        console.log('check asap');
        //  billerFetchResponse = result2.billFetchResponse.billerResponse;
        //  console.log(billerFetchResponse);
        //  console.log("checknow");
      } catch (e) {
        console.log('error' + e);
        var decrypted = '';
      }
    }
  );
});

//end

//Complaint Tracking

apiRoutes.post('/trackbbpsComplaint', function (req, res) {
  var amount = req.body.amount;
  var mobileNumber = req.body.mobileNumber;
  var accessCode = 'AVTM87KZ80ZB95QHDY';
  var reqId = randomstring.generate(35);
  var workingkey = 'C516828299E4BE70B6B0612ACFE795FD';
  var req =
    '<?xml version="1.0" encoding="UTF-8"?><complaintTrackingReq><complaintType>Service</complaintType>';

  var encdata = encrypt(req, workingkey);

  request.post(
    'https://api.billavenue.com/billpay/extComplaints/track/xml',
    {
      form: {
        accessCode: accessCode,
        requestId: reqId,
        encRequest: encdata,
        ver: '1.0',
        instituteId: 'IA83',
      },
    },
    function (err, httpResponse, body) {
      // console.log(body);

      console.log('Body ends');

      if (err) {
        console.log(err);
      }

      try {
        var decrypted = decrypt(body, workingkey);
        result2 = JSON.parse(
          convert.xml2json(decrypted, {
            compact: true,
            spaces: 4,
          })
        );
        console.log(result2);

        console.log('check asap');
        //  billerFetchResponse = result2.billFetchResponse.billerResponse;
        //  console.log(billerFetchResponse);
        //  console.log("checknow");
      } catch (e) {
        console.log('error' + e);
        var decrypted = '';
      }
    }
  );
});

//end

//Complaint Registration  Transaction Type

apiRoutes.post('/registerComplaintTransaction', function (req, res) {
  var amount = req.body.amount;
  var mobileNumber = req.body.mobileNumber;
  var accessCode = 'AVTM87KZ80ZB95QHDY';
  var reqId = randomstring.generate(35);
  var workingkey = 'C516828299E4BE70B6B0612ACFE795FD';
  var req =
    '<?xml version="1.0" encoding="UTF-8"?><complaintRegistrationReq><complaintType>Transaction</complaintType>';

  var encdata = encrypt(req, workingkey);

  request.post(
    'https://api.billavenue.com/billpay/extComplaints/register/xml',
    {
      form: {
        accessCode: accessCode,
        requestId: reqId,
        encRequest: encdata,
        ver: '1.0',
        instituteId: 'IA83',
      },
    },
    function (err, httpResponse, body) {
      // console.log(body);

      console.log('Body ends');

      if (err) {
        console.log(err);
      }

      try {
        var decrypted = decrypt(body, workingkey);
        result2 = JSON.parse(
          convert.xml2json(decrypted, {
            compact: true,
            spaces: 4,
          })
        );
        console.log(result2);

        console.log('check asap');
        //  billerFetchResponse = result2.billFetchResponse.billerResponse;
        //  console.log(billerFetchResponse);
        //  console.log("checknow");
      } catch (e) {
        console.log('error' + e);
        var decrypted = '';
      }
    }
  );
});

//end

//Complaint Registration  Service Type

apiRoutes.post('/registerComplaintService', function (req, res) {
  var amount = req.body.amount;
  var mobileNumber = req.body.mobileNumber;
  var accessCode = 'AVTM87KZ80ZB95QHDY';
  var reqId = randomstring.generate(35);
  var workingkey = 'C516828299E4BE70B6B0612ACFE795FD';
  var req =
    '<?xml version="1.0" encoding="UTF-8"?><complaintRegistrationReq><complaintType>Service</complaintType>';

  var encdata = encrypt(req, workingkey);

  request.post(
    'https://api.billavenue.com/billpay/extComplaints/register/xml',
    {
      form: {
        accessCode: accessCode,
        requestId: reqId,
        encRequest: encdata,
        ver: '1.0',
        instituteId: 'IA83',
      },
    },
    function (err, httpResponse, body) {
      // console.log(body);

      console.log('Body ends');

      if (err) {
        console.log(err);
      }

      try {
        var decrypted = decrypt(body, workingkey);
        result2 = JSON.parse(
          convert.xml2json(decrypted, {
            compact: true,
            spaces: 4,
          })
        );
        console.log(result2);

        console.log('check asap');
        //  billerFetchResponse = result2.billFetchResponse.billerResponse;
        //  console.log(billerFetchResponse);
        //  console.log("checknow");
      } catch (e) {
        console.log('error' + e);
        var decrypted = '';
      }
    }
  );
});

//end

//BILL PAY REQUEST ***************** BBPS (some error)
apiRoutes.post('/paybbpsBill', function (req, res) {
  var amount = req.body.amount;
  var mobileNumber = req.body.mobileNumber;
  var accessCode = 'AVTM87KZ80ZB95QHDY';
  var reqId = randomstring.generate(35);
  var workingkey = 'C516828299E4BE70B6B0612ACFE795FD';
  var req =
    '<?xml version="1.0" encoding="UTF-8"?><billPaymentRequest><agentId>CC01IA83MOBA00000001</agentId><billerAdhoc>true</billerAdhoc><agentDeviceInfo><ip>13.232.172.92</ip><initChannel>MOB</initChannel><imei>448674528976410</imei><app>AIAPP</app><os>Android</os></agentDeviceInfo><customerInfo><customerMobile>' +
    mobileNumber +
    '</customerMobile><customerEmail></customerEmail><customerAdhaar></customerAdhaar><customerPan></customerPan></customerInfo><billerId>OTME00005XXZ43</billerId><inputParams><input><paramName>a</paramName><paramValue>10</paramValue></input><input><paramName>a b</paramName><paramValue>20</paramValue></input><input><paramName>a b c</paramName><paramValue>30</paramValue></input><input><paramName>a b c d</paramName><paramValue>40</paramValue></input><input><paramName>a b c d e</paramName><paramValue>50</paramValue></input></inputParams><billerResponse><billAmount>' +
    amount +
    '</billAmount><billDate>2015-06-14</billDate><billNumber>12303</billNumber><billPeriod>june</billPeriod><customerName>BBPS</customerName><dueDate>2015-06-20</dueDate><amountOptions><option><amountName>Late Payment Fee</amountName><amountValue>40</amountValue></option><option><amountName>Fixed Charges</amountName><amountValue>50</amountValue></option><option><amountName>Additional Charges</amountName><amountValue>60</amountValue></option></amountOptions></billerResponse><additionalInfo><info><infoName>a</infoName><infoValue>10</infoValue></info><info><infoName>a b</infoName><infoValue>20</infoValue></info><info><infoName>a b c</infoName><infoValue>30</infoValue></info><info><infoName>a b c d</infoName><infoValue>40</infoValue></info></additionalInfo><amountInfo><amount>100000</amount><currency>356</currency><custConvFee>0</custConvFee><amountTags></amountTags></amountInfo><paymentMethod><paymentMode>Credit Card</paymentMode><quickPay>N</quickPay><splitPay>N</splitPay></paymentMethod><paymentInfo><info><infoName>CardNum</infoName><infoValue>420078XXXXXX5678</infoValue></info><info><infoName>AuthCode</infoName><infoValue>123456789</infoValue></info></paymentInfo></billPaymentRequest>';

  var encdata = encrypt(req, workingkey);

  request.post(
    'https://api.billavenue.com/billpay/extBillPayCntrl/billPayRequest/xml',
    {
      form: {
        accessCode: accessCode,
        requestId: reqId,
        encRequest: encdata,
        ver: '1.0',
        instituteId: 'IA83',
      },
    },
    function (err, httpResponse, body) {
      // console.log(body);

      console.log('Body ends');

      if (err) {
        console.log(err);
      }

      try {
        var decrypted = decrypt(body, workingkey);
        result2 = JSON.parse(
          convert.xml2json(decrypted, {
            compact: true,
            spaces: 4,
          })
        );
        console.log(result2);

        console.log('check asap');
        //  billerFetchResponse = result2.billFetchResponse.billerResponse;
        //  console.log(billerFetchResponse);
        //  console.log("checknow");
      } catch (e) {
        console.log('error' + e);
        var decrypted = '';
      }
    }
  );
});

//Bill Fetch API ***************** BBPS

apiRoutes.post('/getBill', function (req, res) {
  var billFetchResponse = [];

  // var billerId=req.body.billerId;
  // console.log(billerId);
  // console.log(reqId);
  var accessCode = 'AVTM87KZ80ZB95QHDY';
  var reqId = randomstring.generate(35);
  var workingkey = 'C516828299E4BE70B6B0612ACFE795FD';
  var req =
    '<billFetchRequest><agentId>CC01IA83MOBA00000001</agentId><agentDeviceInfo><ip>13.232.172.92</ip><initChannel>MOB</initChannel><imei>448674528976410</imei><app>AIAPP</app><os>Android</os></agentDeviceInfo><customerInfo><customerMobile>9898990084</customerMobile><customerEmail></customerEmail><customerAdhaar></customerAdhaar><customerPan></customerPan></customerInfo><billerId>OTME00005XXZ43</billerId><inputParams><input><paramName>a</paramName><paramValue>10</paramValue></input><input><paramName>a b</paramName><paramValue>20</paramValue></input><input><paramName>a b c</paramName><paramValue>30</paramValue></input><input><paramName>a b c d</paramName><paramValue>40</paramValue></input><input><paramName>a b c d e</paramName><paramValue>50</paramValue></input></inputParams></billFetchRequest>';

  var encdata = encrypt(req, workingkey);

  request.post(
    'https://api.billavenue.com/billpay/extBillCntrl/billFetchRequest/xml',
    {
      form: {
        accessCode: accessCode,
        requestId: reqId,
        encRequest: encdata,
        ver: '1.0',
        instituteId: 'IA83',
      },
    },
    function (err, httpResponse, body) {
      // console.log(body);

      console.log('Body ends');

      if (err) {
        console.log(err);

        var decrypted = '';
        res.json({
          success: false,
          msg: 'Bill Info Could Not Fetch Successfully',
          res: err,
        });
      } else {
        var decrypted = decrypt(body, workingkey);
        result2 = JSON.parse(
          convert.xml2json(decrypted, {
            compact: true,
            spaces: 4,
          })
        );
        console.log(result2);

        console.log('check asap');
        billFetchResponse = result2.billFetchResponse.billerResponse;
        console.log(billFetchResponse);
        console.log('checknow');
        res.json({
          success: true,
          msg: 'Bill Info Fetched Successfully',
          res: billFetchResponse,
        });
      }
    }
  );
});

//Get INDIVIDUAL BILLER INFO ***************** BBPS

apiRoutes.post('/getBillerInfo', function (req, res) {
  var billerInputParams = [];

  var billerId = req.body.billerId;
  console.log(billerId);
  console.log(reqId);
  var accessCode = 'AVTM87KZ80ZB95QHDY';
  var reqId = randomstring.generate(35);
  var workingkey = 'C516828299E4BE70B6B0612ACFE795FD';
  var req =
    '<?xml version="1.0" encoding="UTF-8"?><billerInfoRequest><billerId>' +
    billerId +
    '</billerId></billerInfoRequest>';

  var encdata = encrypt(req, workingkey);

  request.post(
    'https://api.billavenue.com/billpay/extMdmCntrl/mdmRequest/xml',
    {
      form: {
        accessCode: accessCode,
        requestId: reqId,
        encRequest: encdata,
        ver: '1.0',
        instituteId: 'IA83',
      },
    },
    function (err, httpResponse, body) {
      // console.log(body);

      console.log('Body ends');

      if (err) {
        console.log(err);

        var decrypted = '';
        res.json({
          success: false,
          msg: 'Biller Info Could Not Fetch Successfully',
          res: err,
        });
      } else {
        var decrypted = decrypt(body, workingkey);
        result2 = JSON.parse(
          convert.xml2json(decrypted, {
            compact: true,
            spaces: 4,
          })
        );
        console.log(result2);

        console.log('check asap');
        billerInputParams = result2.billerInfoResponse.biller.billerInputParams;
        console.log(billerInputParams);
        console.log('checknow');
        res.json({
          success: true,
          msg: 'Biller Info Fetched Successfully',
          res: billerInputParams,
        });
      }
    }
  );
});

//below is saved data in db ******* DO NOT REMOVE **************************

//  var accessCode= 'AVTM87KZ80ZB95QHDY';
// var reqId = randomstring.generate(35);
//     var workingkey='C516828299E4BE70B6B0612ACFE795FD';
//     // console.log(originalText); // 'my message'

// var req='<?xml version="1.0" encoding="UTF-8"?><billerInfoRequest></billerInfoRequest>';
//    var encdata= encrypt(req,workingkey);
//     request.post('https://api.billavenue.com/billpay/extMdmCntrl/mdmRequest/xml', {
//       form: {
//     accessCode:accessCode,
//     requestId:reqId,
//     encRequest:encdata,
//     ver:'1.0',
//     instituteId:'IA83'
//       }
//     }, function (err, httpResponse, body) {

//        console.log(body);

//         console.log("Body ends");

//         if(err){
//             console.log("above is error");
//             console.log(err);

//         }

//         try {
//             console.log("goes here");
//           var decrypted=decrypt(body,workingkey);
//          var result2=[];
//          var allGasVendor=[];
//          var allWaterVendor=[];
//          var allElectricityVendor=[];
//          var allBroadbandVendor=[];
//          var allFastagVendor=[];
//          var allLoanRepaymentVendor=[];
//          var allDataBillerArray=[];
//          var allHealthInsuranceArray=[];
//          var allLifeInsuranceArray=[];
//          var allMunicipalTaxesArray=[];
//          var allEducationFeesArray=[];
//          var allHousingSocietyArray=[];
//            result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
//    console.log(result2)
//         console.log(typeof(result2));
//         console.log("check result2 above");

//  allDataBillerArray=result2.billerInfoResponse.biller;
//  console.log(allDataBillerArray[50].billerInputParams.paramInfo.paramName);
//  console.log(typeof(allDataBillerArray));
//  console.log("check asap1 allDataBillerArray type above");
//  console.log(typeof(allDataBillerArray));
//  console.log("check asap1 allDataBillerArray above");
//  console.log(allDataBillerArray.length);
//  console.log("check asap2 allDataBillerArray length");
//  console.log(allDataBillerArray[55]);
//  console.log("check asap2 some vendor");
// for(let i=0;i<allDataBillerArray.length;i++){
//       console.log(allDataBillerArray[i]);
// }
//         for(let i=0;i<allDataBillerArray.length;i++){
//             if(allDataBillerArray[i].billerCategory._text=="Gas"){

//                 console.log("Match");

//                 var gasBillerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 Gas.addGas(gasBillerData,function(err, GasBillerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(GasBillerData);
//                         // console.log("Added Data");
//                     }
//                 });

//             }
//             else if(allDataBillerArray[i].billerCategory._text=="Water"){
//                 // allWaterVendor=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");

//                 var waterBillerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 Water.addWater(waterBillerData,function(err, WaterBillerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         console.log(WaterBillerData);
//                         console.log("Added Data");
//                     }
//                 });
//             }
//             else if(allDataBillerArray[i].billerCategory._text=="Electricity"){
//                 // allElectricityVendor=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");
//                 var electricityBillerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 Electricity.addElectricity(electricityBillerData,function(err, ElectricityBillerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(ElectricityBillerData);
//                         // console.log("Added Data");
//                     }
//                 });
//             }
//             else if(allDataBillerArray[i].billerCategory._text=="Broadband Postpaid"){
//                 // allBroadbandVendor=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");
//                 var broadbandBillerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 Broadband.addbroadband(broadbandBillerData,function(err, BroadbandBillerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(BroadbandBillerData);
//                         // console.log("Added Data");
//                     }
//                 });
//             }
//             else if(allDataBillerArray[i].billerCategory._text=="Fastag"){
//                 // allFastagVendor=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");
//                 var fastTagBillerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 FastTag.addFastTag(fastTagBillerData,function(err, FastTagBillerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(FastTagBillerData);
//                         // console.log("Added Data");
//                     }
//                 });

//             }else if(allDataBillerArray[i].billerCategory._text=='Loan Repayment'){
//             allLoanRepaymentVendor=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");
//                 var billerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 LoanRepayment.addLoanRepayment(billerData,function(err, billerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(billerData);
//                         // console.log("Added Data");
//                     }
//                 });
//         }else if(allDataBillerArray[i].billerCategory._text=='Health Insurance'){
//              allHealthInsuranceArray=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");
//                 var billerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 HealthInsurance.addhealthInsurance(billerData,function(err, billerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(billerData);
//                         // console.log("Added Data");
//                     }
//                 });
//         }else if(allDataBillerArray[i].billerCategory._text=='Insurance'||allDataBillerArray[i].billerCategory._text=='Life Insurance'){
//             allLifeInsuranceArray=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");
//                 var billerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 LifeInsurance.addlifeInsurance(billerData,function(err, billerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(billerData);
//                         // console.log("Added Data");
//                     }
//                 });
//         }else if(allDataBillerArray[i].billerCategory._text=='Education Fees'){
//             allEducationFeesArray=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");
//                 var billerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 EducationFees.addEducationFees(billerData,function(err, billerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(billerData);
//                         // console.log("Added Data");
//                     }
//                 });
//         }else if(allDataBillerArray[i].billerCategory._text=='Municipal Taxes'){
//             allMunicipalTaxesArray=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");
//                 var billerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 MunicipalTaxes.addMunicipalTaxes(billerData,function(err, billerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(billerData);
//                         // console.log("Added Data");
//                     }
//                 });
//         }else if(allDataBillerArray[i].billerCategory._text=='Landline Postpaid'){
//             allLandlinePostpaidArray=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");
//                 var billerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 LandlinePostpaid.addlandlinePostpaid(billerData,function(err, billerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(billerData);
//                         // console.log("Added Data");
//                     }
//                 });
//         }else if(allDataBillerArray[i].billerCategory._text=='Housing Society'){
//             allHousingSocietyArray=allDataBillerArray[i];
//                 // console.log(allDataBillerArray[i]);
//                 console.log("Match");
//                 var billerData = {

//                     billerId:allDataBillerArray[i].billerId._text,
//                     billerName:allDataBillerArray[i].billerName._text,
//                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text

//                 }

//                 HousingSociety.addHousingSociety(billerData,function(err, billerData){
//                     if(err){
//                         console.log(err);
//                         console.log("Failed To add Data");
//                     //	throw err;
//                     }
//                     else{
//                         // console.log(billerData);
//                         // console.log("Added Data");
//                     }
//                 });
//         }
//         }
//         console.log("ends")
//       } catch (e) {
//           console.log("error"+e);
//         var decrypted = "";
//       }

//  });

//*********************************************************/
//Above is commented function after storing to db except water billers
//*********************************************************************/

//fetch Electricity billers here

apiRoutes.post('/electricityBiller', function (req, res) {
  var contactNum = req.body.contactNum;
  console.log(contactNum);

  if (contactNum != undefined) {
    User.findOne(
      {
        contactNum: contactNum,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          Electricity.find(function (err, electricityBillerInfo) {
            if (err) {
              console.log('error');
              console.log(err);
              res.json({
                success: false,
                msg: 'Unable To Fetch Electricity Biller',
                res: err,
              });
            } else {
              var arrayElectricityBiller = [];
              for (var i = 0; i < electricityBillerInfo.length; i++) {
                arrayElectricityBiller.push(electricityBillerInfo[i]._doc);
              }
              res.json({
                success: true,
                msg: 'Electricity Biller Request Sent Successfully',
                res: arrayElectricityBiller,
              });
              console.log(arrayElectricityBiller);
              console.log('check asap biller');
            }
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//fetch Broadband billers here

apiRoutes.post('/broadbandBiller', function (req, res) {
  var contactNum = req.body.contactNum;
  console.log(contactNum);

  if (contactNum != undefined) {
    User.findOne(
      {
        contactNum: contactNum,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          Broadband.find(function (err, broadbandBillerInfo) {
            if (err) {
              console.log('error');
              console.log(err);
              res.json({
                success: false,
                msg: 'Unable To Fetch Broadband Biller',
                res: err,
              });
            } else {
              var arrayBroadbandBiller = [];
              for (var i = 0; i < broadbandBillerInfo.length; i++) {
                arrayBroadbandBiller.push(broadbandBillerInfo[i]._doc);
              }
              res.json({
                success: true,
                msg: 'Broadband Biller Request Sent Successfully',
                res: arrayBroadbandBiller,
              });
              console.log(arrayBroadbandBiller);
              console.log('check asap biller');
            }
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//fetch Gas billers here

apiRoutes.post('/gasBiller', function (req, res) {
  var contactNum = req.body.contactNum;
  console.log(contactNum);

  if (contactNum != undefined) {
    User.findOne(
      {
        contactNum: contactNum,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          Gas.find(function (err, gasBillerInfo) {
            if (err) {
              console.log('error');
              console.log(err);
              res.json({
                success: false,
                msg: 'Unable To Fetch Gas Biller',
                res: err,
              });
            } else {
              var arrayGasBiller = [];
              for (var i = 0; i < gasBillerInfo.length; i++) {
                arrayGasBiller.push(gasBillerInfo[i]._doc);
              }
              res.json({
                success: true,
                msg: 'Gas Biller Request Sent Successfully',
                res: arrayGasBiller,
              });
              console.log(arrayGasBiller);
              console.log('check asap biller');
            }
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//fetch FastTag billers here

apiRoutes.post('/fastTagBiller', function (req, res) {
  var contactNum = req.body.contactNum;
  console.log(contactNum);

  if (contactNum != undefined) {
    User.findOne(
      {
        contactNum: contactNum,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          FastTag.find(function (err, fastTagBillerInfo) {
            if (err) {
              console.log('error');
              console.log(err);
              res.json({
                success: false,
                msg: 'Unable To Fetch FastTag Biller',
                res: err,
              });
            } else {
              var arrayFastTagBiller = [];
              for (var i = 0; i < fastTagBillerInfo.length; i++) {
                arrayFastTagBiller.push(fastTagBillerInfo[i]._doc);
              }
              res.json({
                success: true,
                msg: 'FastTag Biller Request Sent Successfully',
                res: arrayFastTagBiller,
              });
              console.log(arrayFastTagBiller);
              console.log('check asap biller');
            }
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//fetch Water billers here

apiRoutes.post('/waterBiller', function (req, res) {
  var contactNum = req.body.contactNum;
  console.log(contactNum);

  if (contactNum != undefined) {
    User.findOne(
      {
        contactNum: contactNum,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          Water.find(function (err, waterBillerInfo) {
            if (err) {
              console.log('error');
              console.log(err);
              res.json({
                success: false,
                msg: 'Unable To Fetch Water Biller',
                res: err,
              });
            } else {
              var arrayWaterBiller = [];
              for (var i = 0; i < waterBillerInfo.length; i++) {
                arrayWaterBiller.push(waterBillerInfo[i]._doc);
              }
              res.json({
                success: true,
                msg: 'Water Biller Request Sent Successfully',
                res: arrayWaterBiller,
              });
              console.log(arrayWaterBiller);
              console.log('check asap biller');
            }
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});
// MOBILE RECHARGE API

apiRoutes.post('/mobileRecharge', function (req, res) {
  var paythroughWallet = req.body.paythroughWallet;
  var SecurityKey = req.body.SecurityKey;
  var md5Key = req.body.ApiChkSum;
  var CorporateId = parseInt(req.body.CorporateId);
  console.log(CorporateId);
  console.log(typeof CorporateId);
  var AuthKey = req.body.AuthKey;
  var Mobile = req.body.Mobile;
  var Amount = req.body.Amount;
  var ServiceType = req.body.ServiceType;
  var IsPostpaid = req.body.IsPostpaid;
  var userId = req.body.userId;
  var SystemReference = req.body.SystemReference;

  var lowerCaseMd5 = md5(
    CorporateId + AuthKey + Mobile + Amount + SystemReference + md5Key
  );

  console.log(md5Key);

  console.log(lowerCaseMd5 + 'this is final');
  if (paythroughWallet === false) {
    axios
      .post(
        'https://spi.justrechargeit.com/JRICorporateRecharge.svc/Instantrecharge',
        {
          CorporateId: CorporateId,
          AuthKey: AuthKey,
          Mobile: Mobile,
          APIChkSum: lowerCaseMd5,
          SecurityKey: SecurityKey,
          ServiceType: ServiceType,
          IsPostpaid: IsPostpaid,

          SystemReference: SystemReference,
          Amount: Amount,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then((dataRes) => {
        //   console.log(`statusCode: ${res.statusCode}`)
        console.log(dataRes.data);
        console.log('THIS is RESOPONSE');

        var rechargeData = {
          rechargeType: ServiceType,
          contactNum: Mobile,
          paymentMethod: 'Razorpay',
          amount: Amount,
          transactionRef: SystemReference,
          userId: userId,
        };
        Recharge.addRecharge(rechargeData, function (err, recharge) {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Failed to add Recharge Detail',
            });
            //	throw err;
          } else {
            res.json({
              success: true,
              msg: 'Recharge Request Processed Successfully',
              res: dataRes.data,
              recharge: recharge,
            });
          }
        });
      })
      .catch((error) => {
        console.error(error + 'THIS is ERROR');
        res.json({
          success: false,
          msg: 'Recharge Request cannot Send Successfully',
          res: datares.data,
        });
      });
  } else {
    if (userId !== undefined) {
      User.findOne(
        {
          _id: userId,
        },
        function (err, user) {
          if (!user) {
            res.json({
              success: false,
              msg: 'No user with this user id exist',
            });
          } else {
            var prevBalance = user.wallet;
            var newBalance = prevBalance - Amount;
            User.update(
              {
                _id: userId,
              },
              {
                $set: {
                  wallet: newBalance,
                },
              },
              function (err, updateuser) {
                //handle it
                if (err) {
                  res.json({
                    success: false,
                    msg: 'Failed to Update',
                  });
                  //throw err;
                  console.log(err);
                } else {
                  axios
                    .post(
                      'https://spi.justrechargeit.com/JRICorporateRecharge.svc/Instantrecharge',
                      {
                        CorporateId: CorporateId,
                        AuthKey: AuthKey,
                        Mobile: Mobile,
                        APIChkSum: lowerCaseMd5,
                        SecurityKey: SecurityKey,
                        ServiceType: ServiceType,
                        IsPostpaid: IsPostpaid,

                        SystemReference: SystemReference,
                        Amount: Amount,
                      },
                      {
                        headers: {
                          'Content-Type': 'application/json',
                        },
                      }
                    )
                    .then((dataRes) => {
                      //   console.log(`statusCode: ${res.statusCode}`)
                      console.log(dataRes.data);
                      console.log('THIS is RESOPONSE');
                      var rechargeData = {
                        rechargeType: ServiceType,
                        contactNum: Mobile,
                        paymentMethod: 'Wallet',
                        amount: Amount,
                        transactionRef: SystemReference,
                        userId: userId,
                      };
                      Recharge.addRecharge(
                        rechargeData,
                        function (err, recharge) {
                          if (err) {
                            console.log(err);
                            res.json({
                              success: false,
                              msg: 'Failed to add Recharge Detail',
                            });
                            //	throw err;
                          } else {
                            res.json({
                              success: true,
                              msg: 'Recharge Request Processed Successfully',
                              res: dataRes.data,
                              recharge: recharge,
                              user: updateuser,
                            });
                          }
                        }
                      );
                    })
                    .catch((error) => {
                      console.error(error + 'THIS is ERROR');
                      res.json({
                        success: false,
                        msg: 'Recharge Request cannot Send Successfully',
                        res: error,
                      });
                    });
                }
              }
            );
          }
        }
      );
    } else {
      res.json({
        success: false,
        msg: 'Unable to fetch UserId failed',
      });
    }
  }
});

//Update user Image

apiRoutes.post('/updateUserImage', function (req, res) {
  var contactNum = req.body.contactNum;
  console.log(contactNum);
  var userImg = req.body.userImg;
  console.log(userImg);
  // var token = req.body.Authorization;
  // console.log(token);
  // token= "JWT "+token;
  // var isauth= route.memberinfo(token,userId);
  if (contactNum !== undefined) {
    User.findOne(
      {
        contactNum: contactNum,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          User.update(
            {
              contactNum: contactNum,
            },
            {
              $set: {
                user_img: userImg,
              },
            },
            function (err, user) {
              //handle it
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to Update',
                });
                //throw err;
                console.log(err);
              } else {
                res.json({
                  success: true,
                  msg: 'User Successfully Updated',
                  user: user,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Update user Password

// apiRoutes.post('/updatePassword', function(req, res){

//     var contactNum=req.body.contactNum;
//     console.log(contactNum);
//     var password=req.body.password;
//     console.log(password);
//     // var token = req.body.Authorization;
//     // console.log(token);
//    // token= "JWT "+token;
//     // var isauth= route.memberinfo(token,userId);
//     if(userId!==undefined){
//         User.findOne({ 'contactNum': contactNum }, function(err, user) {

//             if(!user){
//                 res.json({success: false, msg: 'No user with this Contact Number exist'});
//             }

//              else

//              {

//                       User.update({'contactNum': contactNum}, { $set: {password:password}}, function (err, user) {
//                        //handle it
//                                 if(err){
//                                 res.json({success: false,msg:'Failed to Update'});
//                                 //throw err;
//                                 console.log(err);
//                             }
//                                 else{
//                                     res.json({success: true,msg:'User Successfully Updated',user:user});
//                                 }
//                         });

//             }
//         });

//     }
//     else{
//         res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
//     }

// });

//Get User Details

// apiRoutes.post('/getUserDetails', function(req, res){

//     var contactNum=req.body.contactNum;

//     // var isauth= route.memberinfo(token,userId);

//     if(contactNum!==undefined){
//          User.findOne({ 'contactNum':contactNum }, function(err, user) {

//              if(err){

//                 res.json("failed to fetch data");

//              }
//             // City.find()

//              res.json({success:true,msg:'User details fetch successfully',user:user});

//     });
//     }
//     else{
//         res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
//     }

// });

//Get User transaction Details

apiRoutes.post('/getUserTransaction', function (req, res) {
  var contactNum = req.body.contactNum;

  // var isauth= route.memberinfo(token,userId);

  if (contactNum !== undefined) {
    Transaction.find(
      {
        contactNum: contactNum,
      },
      function (err, transaction) {
        if (err) {
          res.json('failed to fetch data');
        }
        // City.find()

        res.json({
          success: true,
          msg: 'User details fetch successfully',
          transaction: transaction,
        });
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Send money wallet

apiRoutes.post('/sendMoney', function (req, res) {
  var contactNum = req.body.contactNum;
  var sendingNumber = req.body.sendingNumber;
  var recievingNumber = req.body.recievingNumber;
  console.log(contactNum);

  console.log(sendingNumber);
  console.log(recievingNumber);
  var amount = req.body.amount;
  var transactionRef = req.body.transactionRef;
  console.log(amount);
  var transactionData = {
    userId: userId,
    paymentId: paymentId,
    amount: amount,
    transactionType: 'Send Money',
    transactionRef: transactionRef,
  };
  if (contactNum !== undefined) {
    User.findOne(
      {
        contactNum: recievingNumber,
      },
      function (err, ruser) {
        User.findOne(
          {
            contactNum: sendingNumber,
          },
          function (err, suser) {
            if (!ruser || !suser) {
              res.json({
                success: false,
                msg: 'No user with this Contact Number exist',
              });
            } else {
              var prevAmount = ruser.wallet;
              var recieverAmount = prevAmount + amount;
              var previousAmount = suser.wallet;
              var senderAmount = previousAmount - amount;
              User.update(
                {
                  contactNum: recievingNumber,
                },
                {
                  $set: {
                    wallet: recieverAmount,
                  },
                },
                function (err, ruser) {
                  User.update(
                    {
                      contactNum: sendingNumber,
                    },
                    {
                      $set: {
                        wallet: senderAmount,
                      },
                    },
                    function (err, suser) {
                      //handle it
                      if (err) {
                        res.json({
                          success: false,
                          msg: 'Failed to Update',
                        });
                        //throw err;
                        console.log(err);
                      } else {
                        Transaction.addTransaction(
                          transactionData,
                          function (err, transaction) {
                            if (err) {
                              console.log(err);
                              res.json({
                                success: false,
                                msg: 'Failed to add Transaction',
                              });
                              //	throw err;
                            } else {
                              res.json({
                                success: true,
                                msg: 'User Successfully Updated',
                                suser: suser,
                                transaction: transaction,
                              });
                            }
                          }
                        );
                      }
                    }
                  );
                }
              );
            }
          }
        );
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Forgot Password

apiRoutes.post('/forgotPassword', function (req, res) {
  // var userId=req.body.userId;
  var contactNum = req.body.contactNum;

  // var isauth= route.memberinfo(token,userId);

  if (contactNum !== undefined) {
    User.findOne(
      {
        contactNum: contactNum,
      },
      function (err, user) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var pass = user.password;
          var email = user.emailId;
          //  var transporter = nodemailer.createTransport({
          //     service: 'smtp',
          //     auth: {
          //       user: 'apikey',
          //       pass: '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'
          //     }
          //   });

          let transporter = nodemailer.createTransport({
            host: 'smtp.sendgrid.net',
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
              user: 'apikey',
              pass:
                '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',
            },
          });

          var mailOptions = {
            from: 'promoter@hobbyit.co',
            to: email,
            subject: 'This is your password',
            text: pass,
          };

          transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });
          res.json({
            success: true,
            msg: 'Password Sent to your Email.',
            user: user,
          });
        }
        // City.find()
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Prime Service Booking
apiRoutes.post('/bookPrimeService', function (req, res) {
  var token = req.body.token;
  var userId = req.body.userId;
  var userName = req.body.userName;

  var selectedItems = req.body.selectedItems;
  var isauth = route.memberinfo(token, userId);

  token = 'JWT ' + token;

  var randomstring2 = require('randomstring');
  var cdata = {
    orderId: randomstring2.generate(8),
    eventDate: selectedItems.dateofEvent,
    guests: selectedItems.noOfGuest,
    orderType: selectedItems.orderType,
    description: selectedItems.description,
    vendorName: 'Admin',
    vendorId: 'adminId',
    userId: userId,
    userName: userName,
    time: selectedItems.time,
  };

  if (isauth) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          cdata.add = user.add;
          cdata.contactNum = user.contactNum;
          Prime.addPrime(cdata, function (err, primeOrder) {
            if (err) {
              res.json({
                success: false,
                msg: 'Failed to add Request',
              });
              //	throw err;
            }

            res.json({
              success: true,
              msg: 'Request Sent Successfully',
              primeOrder: primeOrder,
            });
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to add Request Token Authentication failed',
    });
  }
});

//Prime Vendor Booking
apiRoutes.post('/bookPrimeVendor', function (req, res) {
  var token = req.body.token;
  var userId = req.body.userId;
  var userName = req.body.userName;

  var selectedItems = req.body.selectedItems;
  var isauth = route.memberinfo(token, userId);
  var vendorId = selectedItems.vendorId;

  token = 'JWT ' + token;

  var randomstring2 = require('randomstring');
  var cdata = {
    orderId: randomstring2.generate(8),
    eventDate: selectedItems.dateofEvent,
    guests: selectedItems.noOfGuest,
    orderType: selectedItems.orderType,
    description: selectedItems.description,
    vendorName: selectedItems.vendorName,
    vendorId: selectedItems.vendorId,
    userId: userId,
    userName: userName,
  };

  if (isauth) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          cdata.add = user.add;
          cdata.contactNum = user.contactNum;
          Prime.addPrime(cdata, function (err, primeOrder) {
            if (err) {
              res.json({
                success: false,
                msg: 'Failed to add Request',
              });
              //	throw err;
            }

            res.json({
              success: true,
              msg: 'Request Sent Successfully',
              primeOrder: primeOrder,
            });
          });
        }
      }
    );
    Vendor.findOne(
      {
        _id: vendorId,
      },
      function (err, vendor) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var cOrderId = cdata.orderId;
          var tAmt = cdata.totalAmount;
          var vendorDevice = vendor.deviceId;
          axios
            .post(
              'https://fcm.googleapis.com/fcm/send',
              {
                notification: {
                  title: 'New Order Recieved',
                  body: 'Order Id: ' + cOrderId + ' Amount: ' + tAmt,
                  sound: 'default',
                  click_action: 'FCM_PLUGIN_ACTIVITY',
                  icon: 'fcm_push_icon',
                },
                data: {
                  landing_page: 'tabs/tab2',
                  price: '$3,000.00',
                },
                to: vendorDevice,
                priority: 'high',
                restricted_package_name: '',
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization:
                    'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                },
              }
            )
            .then((res) => {
              console.log(`statusCode: ${res.statusCode}`);
              console.log(res);
            })
            .catch((error) => {
              console.error(error);
            });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to add Request Token Authentication failed',
    });
  }
});

//review vendor

apiRoutes.post('/review', function (req, res) {
  var vendorId = req.body.vendId;

  var token = req.body.token;

  var userId = req.body.userId;

  var rate = req.body.rate;

  var isauth = route.memberinfo(token, userId);

  if (isauth) {
    Vendor.findOne(
      {
        _id: vendorId,
      },
      function (err, vendor) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Review Failed To Update',
          });
          throw err;
        }

        var prevRate = vendor.rate;
        if (typeof prevRate !== 'undefined') {
          var updatedRate = (prevRate + rate) / 2;
          var n = updatedRate.toFixed(2);
        } else {
          var prevRate = 0;
        }

        Vendor.update(
          {
            _id: vendorId,
          },
          {
            $set: {
              rate: n,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Review Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Review Updated Successfully',
              });
            }
          }
        );
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//review order

apiRoutes.post('/reviewOrder', function (req, res) {
  var orderId = req.body.orderId;

  var token = req.body.token;
  var userId = req.body.userId;
  var userName = req.body.userName;
  if (req.body.feedback === undefined || req.body.feedback === null) {
    var feedback = 'No Feedback Given';
  } else {
    var feedback = req.body.feedback;
  }

  var rate = req.body.rate;
  var isauth = route.memberinfo(token, userId);
  token = 'JWT ' + token;

  if (isauth) {
    Order.update(
      {
        _id: orderId,
      },
      {
        $set: {
          rate: rate,
          feedback: feedback,
          isReviewed: true,
          isRate: true,
          isFeedback: true,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Review Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Review Updated Successfully',
          });
        }
      }
    );

    // }
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//review order

apiRoutes.post('/setOrder', function (req, res) {
  var orderId = req.body.orderId;

  var token = req.body.token;
  var userId = req.body.userId;
  var userName = req.body.userName;
  var feedback = req.body.feedback;
  var rate = req.body.rate;
  var isauth = route.memberinfo(token, userId);
  token = 'JWT ' + token;

  if (isauth) {
    Order.update(
      {
        _id: orderId,
      },
      {
        $set: {
          isReviewed: true,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Review Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Review Updated Successfully',
          });
        }
      }
    );

    // }
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//cancel order

apiRoutes.post('/cancelOrder', function (req, res) {
  var userId = req.body.userId;
  var vendorId = req.body.vendorId;

  var token = req.body.tokenstr;

  var orderId = req.body.orderId;

  var status = req.body.status;
  var review = true;

  var isauth = route.memberinfo(token, userId);

  if (isauth) {
    Order.update(
      {
        orderId: orderId,
      },
      {
        $set: {
          status: status,
          isReviewed: review,
          isCancelled: true,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Order Failed To Update',
          });
          throw err;
        }
      }
    );

    Vendor.findOne(
      {
        _id: vendorId,
      },
      function (err, vendor) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var orderId = req.body.orderId;

          var vendorDevice = vendor.deviceId;
          var tAmt = req.body.totalAmount;

          axios
            .post(
              'https://fcm.googleapis.com/fcm/send',
              {
                notification: {
                  title: 'Order CANCELLED',
                  body: 'Order Id: ' + orderId + ' Amount: ' + tAmt,
                  sound: 'default',
                  click_action: 'FCM_PLUGIN_ACTIVITY',
                  icon: 'fcm_push_icon',
                },
                data: {
                  landing_page: 'tabs/tab2',
                  price: '$3,000.00',
                },
                to: vendorDevice,
                priority: 'high',
                restricted_package_name: '',
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization:
                    'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                },
              }
            )
            .then((res) => {
              console.log(`statusCode: ${res.statusCode}`);
              console.log(res);
            })
            .catch((error) => {
              console.error(error);
            });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//cancel Appointment

apiRoutes.post('/cancelAppointment', function (req, res) {
  var userId = req.body.userId;
  var vendorId = req.body.vendorId;
  // console.log("HEY THIS IS VENDOR ID "+vendorId);
  var token = req.body.tokenstr;

  var orderId = req.body.orderId;

  var status = req.body.status;
  var review = true;

  var isauth = route.memberinfo(token, userId);

  if (isauth) {
    Prime.update(
      {
        orderId: orderId,
      },
      {
        $set: {
          status: status,
          isReviewed: review,
          isCancelled: true,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Order Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Order Updated Successfully',
          });
        }
      }
    );

    Vendor.findOne(
      {
        _id: vendorId,
      },
      function (err, vendor) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var orderId = req.body.orderId;

          var vendorDevice = vendor.deviceId;

          axios
            .post(
              'https://fcm.googleapis.com/fcm/send',
              {
                notification: {
                  title: 'Order CANCELLED',
                  body: 'Order Id: ' + orderId + '. ',
                  sound: 'default',
                  click_action: 'FCM_PLUGIN_ACTIVITY',
                  icon: 'fcm_push_icon',
                },
                data: {
                  landing_page: 'tabs/tab2',
                  price: '$3,000.00',
                },
                to: vendorDevice,
                priority: 'high',
                restricted_package_name: '',
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization:
                    'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                },
              }
            )
            .then((res) => {
              console.log(`statusCode: ${res.statusCode}`);
              console.log(res);
            })
            .catch((error) => {
              console.error(error);
            });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Accept order
apiRoutes.post('/acceptOrder/:vendorId/:token', function (req, res) {
  var vendorId = req.params.vendorId;

  var token = req.params.token;

  var orderId = req.body.orderId;

  var status = req.body.status;
  var userId = req.body.userId;

  var isauth = route.memberinfo(token, vendorId);

  if (isauth) {
    Order.update(
      {
        orderId: orderId,
      },
      {
        $set: {
          status: status,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Order Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Order Updated Successfully',
          });
        }
      }
    );

    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        var userDevice = user.deviceId;
        console.log(userDevice);
        console.log(userId);

        if (err) {
          res.json('failed to fetch data');
        } else {
          var orderId = req.body.orderId;
          var tAmt = req.body.totalAmount;

          console.log(userDevice);
          console.log(tAmt);

          axios
            .post(
              'https://fcm.googleapis.com/fcm/send',
              {
                notification: {
                  title: 'Order Accepted',
                  body: 'Order Id: ' + orderId + ' Amount: ' + tAmt,
                  sound: 'default',
                  click_action: 'FCM_PLUGIN_ACTIVITY',
                  icon: 'fcm_push_icon',
                },
                data: {
                  landing_page: 'tabs/tab2',
                  price: '$3,000.00',
                },
                to: userDevice,
                priority: 'high',
                restricted_package_name: '',
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization:
                    'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                },
              }
            )
            .then((res) => {
              console.log(`statusCode: ${res.statusCode}`);
              console.log(res);
            })
            .catch((error) => {
              console.error(error);
            });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Get Vendor device token

apiRoutes.post('/setVendorDeviceId/', function (req, res) {
  var vendorId = req.body.vendorId;

  var token = req.body.token;

  var tokenDevice = req.body.tokenDevice;

  var isauth = route.memberinfo(token, vendorId);

  if (isauth) {
    Vendor.update(
      {
        _id: vendorId,
      },
      {
        $set: {
          deviceId: tokenDevice,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Vendor Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Vendor Updated Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//RejectOrder
apiRoutes.post('/rejectOrder/:vendorId/:token', function (req, res) {
  var vendorId = req.params.vendorId;

  var token = req.params.token;

  var orderId = req.body.orderId;

  var status = req.body.status;
  var userId = req.body.userId;

  var isauth = route.memberinfo(token, vendorId);

  if (isauth) {
    Order.update(
      {
        orderId: orderId,
      },
      {
        $set: {
          status: status,
          isCancelled: true,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Order Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Order Updated Successfully',
          });
        }
      }
    );

    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var orderId = req.body.orderId;

          var userDevice = user.deviceId;
          var tAmt = req.body.totalAmount;

          axios
            .post(
              'https://fcm.googleapis.com/fcm/send',
              {
                notification: {
                  title: 'Order Rejected',
                  body: 'Order Id: ' + orderId + ' Amount: ' + tAmt,
                  sound: 'default',
                  click_action: 'FCM_PLUGIN_ACTIVITY',
                  icon: 'fcm_push_icon',
                },
                data: {
                  landing_page: 'tabs/tab2',
                  price: '$3,000.00',
                },
                to: userDevice,
                priority: 'high',
                restricted_package_name: '',
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization:
                    'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                },
              }
            )
            .then((res) => {
              console.log(`statusCode: ${res.statusCode}`);
              console.log(res);
            })
            .catch((error) => {
              console.error(error);
            });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//AskReview

apiRoutes.post('/askReview/:vendorId/:token', function (req, res) {
  var vendorId = req.params.vendorId;

  var token = req.params.token;

  var orderId = req.body.orderId;

  var status = req.body.status;

  var isauth = route.memberinfo(token, vendorId);

  if (isauth) {
    Order.update(
      {
        orderId: orderId,
      },
      {
        $set: {
          status: status,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Order Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Order Updated Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Completed Order
apiRoutes.post('/completeOrder/:vendorId/:token', function (req, res) {
  var vendorId = req.params.vendorId;

  var token = req.params.token;

  var orderId = req.body.orderId;

  var status = req.body.status;
  var userId = req.body.userId;

  var isauth = route.memberinfo(token, vendorId);

  if (isauth) {
    Order.update(
      {
        orderId: orderId,
      },
      {
        $set: {
          status: status,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Order Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Order Updated Successfully',
          });
        }
      }
    );

    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var orderId = req.body.orderId;
          var userDevice = user.deviceId;
          console.log(userDevice);
          var tAmt = req.body.totalAmount;

          axios
            .post(
              'https://fcm.googleapis.com/fcm/send',
              {
                notification: {
                  title: 'Order Delivered',
                  body: 'Order Id: ' + orderId + ' Amount: ' + tAmt,
                  sound: 'default',
                  click_action: 'FCM_PLUGIN_ACTIVITY',
                  icon: 'fcm_push_icon',
                },
                data: {
                  landing_page: 'tabs/tab2',
                  price: '$3,000.00',
                },
                to: userDevice,
                priority: 'high',
                restricted_package_name: '',
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization:
                    'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                },
              }
            )
            .then((res) => {
              console.log(`statusCode: ${res.statusCode}`);
              console.log(res);
            })
            .catch((error) => {
              console.error(error);
            });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Delivery Status
apiRoutes.post('/deliveryStatus/:vendorId/:token', function (req, res) {
  var vendorId = req.params.vendorId;
  var token = req.params.token;
  var storeId = req.body.storeId;
  var deliveryStatus = req.body.deliveryStatus;
  var delivery = deliveryStatus;
  var isauth = route.memberinfo(token, vendorId);

  if (isauth) {
    Store.update(
      {
        _id: storeId,
      },
      {
        $set: {
          deliveryStatus: delivery,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Status Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Status Updated Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//store settings

apiRoutes.post('/storeSettings/:vendorId/:token', function (req, res) {
  var vendorId = req.params.vendorId;
  var token = req.params.token;
  var isauth = route.memberinfo(token, vendorId);

  var storeId = req.body.storeId;
  var operationDays = req.body.obj;
  var timings = req.body.timings;

  if (isauth) {
    Store.update(
      {
        _id: storeId,
      },
      {
        $set: {
          operationDays: operationDays,
          Timings: timings,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Status Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Status Updated Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//get order

apiRoutes.get('/getOrders/:userId/:token', function (req, res) {
  var userId = req.params.userId;
  var token = req.params.token;
  var isauth = route.memberinfo(token, userId);
  token = 'JWT ' + token;

  if (isauth) {
    Order.find(
      {
        userId: userId,
      },
      function (err, orders) {
        res.json({
          success: true,
          orders: orders,
        });
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//get Prime Products

apiRoutes.get('/getAppointment/:userId/:token', function (req, res) {
  var userId = req.params.userId;
  var token = req.params.token;
  var isauth = route.memberinfo(token, userId);
  token = 'JWT ' + token;

  if (isauth) {
    Prime.find(function (err, primeOrders) {
      if (err) {
        console.log(err);
      } else {
        var odata = [];
        for (var k = 0; k < primeOrders.length; k++) {
          if (primeOrders[k].userId === userId) {
            console.log(primeOrders[k].userId);
            console.log(userId);
            odata.push(primeOrders[k]);
          } else {
            console.log('Dont add to list filter');
          }
        }
      }
      res.json({
        success: true,
        odata: odata,
      });
    });
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

apiRoutes.get('/getUserDashboard/:userId', function (req, res) {
  var token = getToken(req.headers);
  var userId = req.params.userId;
  var isauth = route.memberinfo(token, userId);
  token = 'JWT ' + token;

  if (isauth) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (err) {
          res.json('failed to fetch data');
        }
        if (!isEmpty(user)) {
          var accessLevelName = user.accessLevelName;

          if (user.isHod == true || accessLevelName == 'hod') {
            Product.find(function (err, products) {
              if (err) {
                console.log('error');
                res.send(err);
              }

              Category.find(function (err, category) {
                if (err) {
                  console.log('Error');
                }

                res.json({
                  success: true,
                  product: products,
                  category: category,
                  user: user,
                });
              });
            });
          }
          //for admin
          else if (user.accessLevelName == 'superadmin') {
            User.find(function (err, user) {
              // if there is an error retrieving, send the error. nothing after res.send(err) will execute
              if (err) {
                res.send(err);
              }
              Product.find(function (err, products) {
                // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                if (err) {
                  res.send(err);
                }

                Order.find(function (err, orders) {
                  Category.find(function (err, categories) {
                    if (err) {
                      console.log(err);
                    }

                    Prime.find(function (err, primeOrders) {
                      if (err) {
                        console.log(err);
                      }

                      ActiveSubscription.find(function (err, activeSub) {
                        if (err) {
                          console.log(err);
                        }
                        var activeSubscription = [];
                        for (var i = 0; i < activeSub.length; i++) {
                          var date = activeSub[i].startDate;
                          var a = moment(date).format('DD MMM YY, hh:mm a');
                          var dateEnd = activeSub[i].endDate;
                          var b = moment(dateEnd).format('DD MMM YY, hh:mm a');
                          console.log(a);
                          activeSub[i].styleStartDate = a;
                          activeSub[i].styleEndDate = b;
                          activeSubscription.push(activeSub[i]);
                          console.log(b);
                        }
                        Store.find(function (err, storeD) {
                          if (err) {
                            console.log(err);
                          }

                          //   Feed.find(function(err, feed) {

                          // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                          // if (err){
                          //     res.send(err);
                          // }

                          Trend.find(function (err, trend) {
                            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                            if (err) {
                              res.send(err);
                            }
                            Subscription.find(function (err, subscriptions) {
                              // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                              if (err) {
                                res.send(err);
                              }

                              SubCategory.find(function (err, subcategories) {
                                if (err) {
                                  console.log(err);
                                }

                                Vendor.find(function (err, vendors) {
                                  if (err) {
                                    console.log(err);
                                  }

                                  vendorCategory.find(function (
                                    err,
                                    vendorcat
                                  ) {
                                    if (err) {
                                      console.log(err);
                                    }

                                    res.json({
                                      success: true,
                                      users: user,
                                      products: products,
                                      orders: orders,
                                      categories: categories,
                                      vendors: vendors,
                                      vendorcat: vendorcat,
                                      subcategories: subcategories,
                                      stores: storeD,
                                      trends: trend,
                                      subscriptions: subscriptions,
                                      primeOrders: primeOrders,
                                      activeSub: activeSubscription,
                                    });
                                  });
                                });
                              });
                            });
                          });

                          // });
                        });
                      });
                    });
                  });
                });
              });

              // return all todos in JSON format
            });
          }
        } else {
          res.json({
            success: false,
            msg: 'User does not exist',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

apiRoutes.get('/getUserDashboard/:userId/:token', function (req, res) {
  var userId = req.params.userId;
  var token = req.params.token;
  var isauth = route.memberinfo(token, userId);
  token = 'JWT ' + token;

  if (isauth) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (err) {
          res.json('failed to fetch data');
        }
        if (!isEmpty(user)) {
          var accessLevelName = user.accessLevelName;

          if (user.isHod == true || accessLevelName == 'basic') {
            Product.find(function (err, data) {
              if (err) {
                console.log('error');
                res.send(err);
              } else {
                var products = [];
                for (var i = 0; i < data.length; i++) {
                  if (data[i].isActive === true) {
                    products.push(data[i]);
                  } else {
                    //no comment
                  }
                }
              }

              SubCategory.find(function (err, subcategories) {
                if (err) {
                  console.log(err);
                }
                Laundry.find(function (err, laundry) {
                  if (err) {
                    console.log(err);
                  }

                  Category.find(function (err, category) {
                    if (err) {
                      console.log('Error');
                    }
                    var fdata = [];
                    for (var j = 0; j < category.length; j++) {
                      var prod = [];
                      var cat = {};
                      for (var k = 0; k < products.length; k++) {
                        if (
                          category[j].categoryName == products[k].categoryName
                        ) {
                          prod.push(products[k]);
                        }
                      }
                      cat.category = category[j].categoryName;
                      cat.products = prod;
                      fdata.push(cat);
                    }

                    Order.find(function (err, orders) {
                      if (err) {
                        console.log(err);
                      } else {
                        var odata = [];
                        var comp = [];
                        var newOrder = [];
                        var reviewOrder = [];
                        for (var k = 0; k < orders.length; k++) {
                          if (orders[k].userId === userId) {
                            if (orders[k].status === 'Accepted') {
                              odata.push(orders[k]);
                            } else if (
                              orders[k].status === 'Completed' ||
                              orders[k].status === 'CANCELLED' ||
                              orders[k].status === 'Rejected'
                            ) {
                              comp.push(orders[k]);
                            } else {
                              newOrder.push(orders[k]);
                            }
                          } else {
                            console.log('Dont add to list filter');
                          }
                        }
                        for (var l = 0; l < comp.length; l++) {
                          if (comp[l].isReviewed === false) {
                            reviewOrder.push(comp[l]);
                          } else {
                          }
                        }
                      }

                      Vendor.find(function (err, vendors) {
                        if (err) {
                          console.log(err);
                        }

                        vendorCategory.find(function (err, vendorcat) {
                          if (err) {
                            console.log(err);
                          }
                          Store.find(function (err, storeD) {
                            if (err) {
                              console.log(err);
                            }

                            var stores = [];
                            var today = moment().format('dddd');
                            for (var i = 0; i < storeD.length; i++) {
                              if (
                                today === 'Monday' &&
                                storeD[i].operationDays.Monday === true
                              ) {
                                stores.push(storeD[i]);
                              } else if (
                                today === 'Tuesday' &&
                                storeD[i].operationDays.Tuesday === true
                              ) {
                                stores.push(storeD[i]);
                              } else if (
                                today === 'Wednesday' &&
                                storeD[i].operationDays.Wednesday === true
                              ) {
                                stores.push(storeD[i]);
                              } else if (
                                today === 'Thursday' &&
                                storeD[i].operationDays.Thursday === true
                              ) {
                                stores.push(storeD[i]);
                              } else if (
                                today === 'Friday' &&
                                storeD[i].operationDays.Friday === true
                              ) {
                              } else if (
                                today === 'Saturday' &&
                                storeD[i].operationDays.Saturday === true
                              ) {
                                stores.push(storeD[i]);
                              } else {
                                stores.push(storeD[i]);
                              }
                            }
                            console.log(stores);
                            Feed.find(function (err, feeds) {
                              if (err) {
                                console.log(err);
                              }
                              Trend.find(function (err, trend) {
                                if (err) {
                                  console.log(err);
                                } else {
                                  var topBanner = [];
                                  var latestUpdate = [];
                                  var primeService = [];
                                  for (i = 0; i < trend.length; i++) {
                                    if (trend[i].trendType === 'Top Banner') {
                                      topBanner.push(trend[i]);
                                    } else if (
                                      trend[i].trendType === 'Latest Update'
                                    ) {
                                      latestUpdate.push(trend[i]);
                                    } else if (
                                      trend[i].trendType === 'Prime Service'
                                    ) {
                                      primeService.push(trend[i]);
                                    } else {
                                    }
                                  }
                                }

                                res.json({
                                  success: true,
                                  reviewOrder: reviewOrder,
                                  products: products,
                                  category: category,
                                  user: user,
                                  vendorcat: vendorcat,
                                  vendors: vendors,
                                  subcategories: subcategories,
                                  stores: stores,
                                  odata: odata,
                                  comp: comp,
                                  newOrder: newOrder,
                                  laundryOrder: laundry,
                                  primeService: primeService,
                                  latestUpdate: latestUpdate,
                                  topBanner: topBanner,
                                  feeds: feeds,
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          }
          //for admin
          else if (user.isSuperAdmin == true) {
            User.find(function (err, user) {
              // if there is an error retrieving, send the error. nothing after res.send(err) will execute
              if (err) {
                res.send(err);
              }
              Product.find(function (err, products) {
                // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                if (err) {
                  res.send(err);
                }

                Category.find(function (err, categories) {
                  if (err) {
                    console.log(err);
                  }
                  Order.find(function (err, orders) {
                    res.json({
                      success: true,
                      users: user,
                      products: products,
                      orders: orders,
                      categories: categories,
                    });
                  });
                });
              });
            });
          }
        } else {
          res.json({
            success: false,
            msg: 'users does not exist',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//get vendor dashboard
apiRoutes.get('/getVendorDashboard/:vendorId/:token', function (req, res) {
  if (res.success === true) {
    var vendorId = req.params.vendorId;
  }
  var vendorId = req.params.vendorId;

  var token = req.params.token;
  var isauth = route.memberinfo(token, vendorId);
  token = 'JWT ' + token;

  if (isauth) {
    Vendor.findOne(
      {
        _id: vendorId,
      },
      function (err, vendor) {
        if (err) {
          res.json('failed to fetch data');
        }
        if (!isEmpty(vendor)) {
          var accessLevelName = vendor.accessLevelName;

          if (accessLevelName == 'vendor') {
            Product.find(function (err, products) {
              if (err) {
                console.log('error');
                res.send(err);
              }
              var produ = [];
              for (var f = 0; f < products.length; f++) {
                if (products[f].productVendorId === vendorId) {
                  produ.push(products[f]);
                }
              }

              SubCategory.find(function (err, subcategories) {
                if (err) {
                  console.log(err);
                }
                var subcat = [];
                for (var v = 0; v < subcategories.length; v++) {
                  if (subcategories[v].subCategoryVendorId === vendorId) {
                    subcat.push(subcategories[v]);
                  } else {
                    console.log('server Error');
                  }
                }

                Category.find(function (err, category) {
                  if (err) {
                    console.log('Error');
                  }
                  var categ = [];
                  for (var e = 0; e < category.length; e++) {
                    if (category[e].categoryVendorId === vendorId) {
                      categ.push(category[e]);
                    }
                  }
                  var fdata = [];
                  for (var j = 0; j < categ.length; j++) {
                    var prod = [];
                    var cat = {};
                    for (var k = 0; k < produ.length; k++) {
                      if (categ[j].categoryName == produ[k].categoryName) {
                        prod.push(products[k]);
                      }
                    }
                    cat.categ = category[j].categoryName;
                    cat.produ = prod;
                    fdata.push(cat);
                  }

                  Store.find(function (err, storeD) {
                    if (err) {
                      console.log(err);
                    }
                    var storeDet = [];
                    for (var u = 0; u < storeD.length; u++) {
                      if (storeD[u].storeVendorId === vendorId) {
                        storeDet.push(storeD[u]);
                      } else {
                        console.log('server Error');
                      }
                    }

                    if (
                      vendor.vendorCategoryName === 'Home Cleaning' ||
                      vendor.vendorCategoryName === 'Home Cleaning' ||
                      vendor.vendorCategoryName === 'Auto-Service' ||
                      vendor.vendorCategoryName === 'Laundry'
                    ) {
                      var pOrder = [];
                      var odata = [];
                      var comp = [];
                      var newOrder = [];

                      Prime.find(function (err, primeOrders) {
                        if (err) {
                          console.log(err);
                        }
                        for (var x = 0; x < primeOrders.length; x++) {
                          if (primeOrders[x].vendorId === vendorId) {
                            pOrder.push(primeOrders[x]);
                          } else {
                          }
                        }
                        for (var k = 0; k < pOrder.length; k++) {
                          if (pOrder[k].status === 'Accepted') {
                            odata.push(pOrder[k]);
                          } else if (pOrder[k].status === 'Completed') {
                            comp.push(pOrder[k]);
                          } else {
                            newOrder.push(pOrder[k]);
                          }
                        }

                        res.json({
                          success: true,
                          produ: produ,
                          categ: categ,
                          fdata: fdata,
                          subcat: subcat,
                          storeDet: storeDet,
                          odata: odata,
                          comp: comp,
                          newOrder: newOrder,
                          vendor: vendor,
                        });
                      });
                    } else {
                      Order.find(function (err, orders) {
                        if (err) {
                          console.log(err);
                        }
                        var filterOrder = [];
                        for (var x = 0; x < orders.length; x++) {
                          for (var y = 0; y < orders[x].orderDesc.length; y++) {
                            if (
                              orders[x].orderDesc[y].productVendorId ===
                              vendorId
                            ) {
                              filterOrder.push(orders[x]);
                            } else {
                              console.log('Server Error');
                            }
                          }
                        }

                        var odata = [];
                        var comp = [];
                        var newOrder = [];
                        for (var k = 0; k < filterOrder.length; k++) {
                          if (filterOrder[k].status === 'Accepted') {
                            odata.push(filterOrder[k]);
                          } else if (
                            filterOrder[k].status === 'Completed' ||
                            filterOrder[k].status === 'CANCELLED' ||
                            filterOrder[k].status === 'Rejected'
                          ) {
                            comp.push(filterOrder[k]);
                          } else {
                            newOrder.push(filterOrder[k]);
                          }
                        }

                        res.json({
                          success: true,
                          produ: produ,
                          categ: categ,
                          fdata: fdata,
                          subcat: subcat,
                          storeDet: storeDet,
                          odata: odata,
                          comp: comp,
                          newOrder: newOrder,
                          fOrder: filterOrder,
                          vendor: vendor,
                        });
                      });
                    }
                  });
                });
              });
            });
          }
          //for admin
          else if (user.isSuperAdmin == true) {
            User.find(function (err, user) {
              // if there is an error retrieving, send the error. nothing after res.send(err) will execute
              if (err) {
                res.send(err);
              }
              Product.find(function (err, products) {
                // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                if (err) {
                  res.send(err);
                }

                Category.find(function (err, categories) {
                  if (err) {
                    console.log(err);
                  }
                  Order.find(function (err, orders) {
                    res.json({
                      success: true,
                      users: user,
                      products: products,
                      orders: orders,
                      categories: categories,
                    });
                  });
                });
              });
            });
          }
        } else {
          res.json({
            success: false,
            msg: 'Vendor does not exist',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});
//end

//get graph order details
// apiRoutes.post('/getOrderDetails/', function(req, res){

//     var token = req.body.token;
//     var vendorId=req.body.vendorId;
//     var isauth= route.memberinfo(token,vendorId);

//     if(isauth){
//          Vendor.findOne({ '_id':vendorId }, function(err, vendor) {

//              if(err){

//                 res.json("failed to fetch data");

//              }

//     var dateStart = new Date();
//    var ds=  moment(dateStart).format();

//     var dateEnd = dateStart.setDate(dateStart.getDate()-1);
//     var de= moment(dateEnd).format();
//     var dateEnd2 = dateStart.setDate(dateStart.getDate()-1);
//     var de2= moment(dateEnd2).format();
//     var dateEnd3 = dateStart.setDate(dateStart.getDate()-1);
//     var de3= moment(dateEnd3).format();
//     var dateEnd4 = dateStart.setDate(dateStart.getDate()-1);
//     var de4= moment(dateEnd4).format();
//     var dateEnd5 = dateStart.setDate(dateStart.getDate()-1);
//     var de5= moment(dateEnd5).format();
//     var dateEnd6 = dateStart.setDate(dateStart.getDate()-1);
//     var de6= moment(dateEnd6).format();
//     var dateEnd7 = dateStart.setDate(dateStart.getDate()-1);
//     var de7= moment(dateEnd7).format();

//     Order.find( //query today up to tonight

//         {createdDate: {"$gte": de, "$lt": ds}}, function(err, orders){
//             if(err){
//                 res.json("failed to fetch data");
//             }

//                 var filterOrder = [];
//                              for(var x=0;x<orders.length;x++){
//                                  for(var y=0;y<orders[x].orderDesc.length;y++){

//                                     if(orders[x].orderDesc[y].productVendorId===vendorId && orders[x].isCancelled===false && orders[x].status==="Completed"){

//                                         filterOrder.push(orders[x]);
//                                     }else{
//                                         console.log("Server Error");
//                                     }
//                                 }

//                             }
//                                         var newAmt=0;
//                                         for(var z=0;z<filterOrder.length;z++){
//                                             var length=filterOrder.length;
//                                             var toNum=Number(filterOrder[z].totalAmount);
//                                           var cd = filterOrder[z].createdDate;
//                                           newAmt += toNum;
//                                         }
//                                         var Day1=[{"amt":newAmt,"createdDate":cd,"todayOrders":length}];

//                                         Order.find( //query today up to tonight

//                                             {createdDate: {"$gte": de2, "$lt": de}}, function(err, orders){
//                                                 if(err){
//                                                     res.json("failed to fetch data");
//                                                 }

//                                                     var filterOrder = [];
//                                                                  for(var x=0;x<orders.length;x++){
//                                                                      for(var y=0;y<orders[x].orderDesc.length;y++){

//                                                                         if(orders[x].orderDesc[y].productVendorId===vendorId){

//                                                                             filterOrder.push(orders[x]);
//                                                                         }else{
//                                                                             console.log("Server Error");
//                                                                         }
//                                                                     }

//                                                                 }
//                                                                             var newAmt=0;
//                                                                             for(var z=0;z<filterOrder.length;z++){

//                                                                                 var toNum=Number(filterOrder[z].totalAmount);
//                                                                               var cd = filterOrder[z].createdDate;
//                                                                               newAmt += toNum;
//                                                                             }
//                                                                             var Day2=[{"amt":newAmt,"createdDate":cd}];

//                                                                             Order.find( //query today up to tonight

//                                                                                 {createdDate: {"$gte": de3, "$lt": de2}}, function(err, orders){
//                                                                                     if(err){
//                                                                                         res.json("failed to fetch data");
//                                                                                     }

//                                                                                         var filterOrder = [];
//                                                                                                      for(var x=0;x<orders.length;x++){
//                                                                                                          for(var y=0;y<orders[x].orderDesc.length;y++){

//                                                                                                             if(orders[x].orderDesc[y].productVendorId===vendorId){

//                                                                                                                 filterOrder.push(orders[x]);
//                                                                                                             }else{
//                                                                                                                 console.log("Server Error");
//                                                                                                             }
//                                                                                                         }

//                                                                                                     }
//                                                                                                                 var newAmt=0;
//                                                                                                                 for(var z=0;z<filterOrder.length;z++){

//                                                                                                                     var toNum=Number(filterOrder[z].totalAmount);
//                                                                                                                   var cd = filterOrder[z].createdDate;
//                                                                                                                   newAmt += toNum;
//                                                                                                                 }
//                                                                                                                 var Day3=[{"amt":newAmt,"createdDate":cd}];

//                                                                                                                 Order.find( //query today up to tonight

//                                                                                                                     {createdDate: {"$gte": de4, "$lt": de3}}, function(err, orders){
//                                                                                                                         if(err){
//                                                                                                                             res.json("failed to fetch data");
//                                                                                                                         }

//                                                                                                                             var filterOrder = [];
//                                                                                                                                          for(var x=0;x<orders.length;x++){
//                                                                                                                                              for(var y=0;y<orders[x].orderDesc.length;y++){

//                                                                                                                                                 if(orders[x].orderDesc[y].productVendorId===vendorId){

//                                                                                                                                                     filterOrder.push(orders[x]);
//                                                                                                                                                 }else{
//                                                                                                                                                     console.log("Server Error");
//                                                                                                                                                 }
//                                                                                                                                             }

//                                                                                                                                         }
//                                                                                                                                                     var newAmt=0;
//                                                                                                                                                     for(var z=0;z<filterOrder.length;z++){

//                                                                                                                                                         var toNum=Number(filterOrder[z].totalAmount);
//                                                                                                                                                       var cd = filterOrder[z].createdDate;
//                                                                                                                                                       newAmt += toNum;
//                                                                                                                                                     }
//                                                                                                                                                     var Day4=[{"amt":newAmt,"createdDate":cd}];

//                                                                                                                                                     Order.find( //query today up to tonight

//                                                                                                                                                         {createdDate: {"$gte": de5, "$lt": de4}}, function(err, orders){
//                                                                                                                                                             if(err){
//                                                                                                                                                                 res.json("failed to fetch data");
//                                                                                                                                                             }

//                                                                                                                                                                 var filterOrder = [];
//                                                                                                                                                                              for(var x=0;x<orders.length;x++){
//                                                                                                                                                                                  for(var y=0;y<orders[x].orderDesc.length;y++){

//                                                                                                                                                                                     if(orders[x].orderDesc[y].productVendorId===vendorId){

//                                                                                                                                                                                         filterOrder.push(orders[x]);
//                                                                                                                                                                                     }else{
//                                                                                                                                                                                         console.log("Server Error");
//                                                                                                                                                                                     }
//                                                                                                                                                                                 }

//                                                                                                                                                                             }
//                                                                                                                                                                                         var newAmt=0;
//                                                                                                                                                                                         for(var z=0;z<filterOrder.length;z++){

//                                                                                                                                                                                             var toNum=Number(filterOrder[z].totalAmount);
//                                                                                                                                                                                           var cd = filterOrder[z].createdDate;
//                                                                                                                                                                                           newAmt += toNum;
//                                                                                                                                                                                         }
//                                                                                                                                                                                         var Day5=[{"amt":newAmt,"createdDate":cd}];

//                                                                                                                                                                                         Order.find( //query today up to tonight

//                                                                                                                                                                                             {createdDate: {"$gte": de6, "$lt": de5}}, function(err, orders){
//                                                                                                                                                                                                 if(err){
//                                                                                                                                                                                                     res.json("failed to fetch data");
//                                                                                                                                                                                                 }

//                                                                                                                                                                                                     var filterOrder = [];
//                                                                                                                                                                                                                  for(var x=0;x<orders.length;x++){
//                                                                                                                                                                                                                      for(var y=0;y<orders[x].orderDesc.length;y++){

//                                                                                                                                                                                                                         if(orders[x].orderDesc[y].productVendorId===vendorId){

//                                                                                                                                                                                                                             filterOrder.push(orders[x]);
//                                                                                                                                                                                                                         }else{
//                                                                                                                                                                                                                             console.log("Server Error");
//                                                                                                                                                                                                                         }
//                                                                                                                                                                                                                     }

//                                                                                                                                                                                                                 }
//                                                                                                                                                                                                                             var newAmt=0;
//                                                                                                                                                                                                                             for(var z=0;z<filterOrder.length;z++){

//                                                                                                                                                                                                                                 var toNum=Number(filterOrder[z].totalAmount);
//                                                                                                                                                                                                                               var cd = filterOrder[z].createdDate;
//                                                                                                                                                                                                                               newAmt += toNum;
//                                                                                                                                                                                                                             }
//                                                                                                                                                                                                                             var Day6=[{"amt":newAmt,"createdDate":cd}];

//                                                                                                                                                                                                                             Order.find( //query today up to tonight

//                                                                                                                                                                                                                                 {createdDate: {"$gte": de7, "$lt": de6}}, function(err, orders){
//                                                                                                                                                                                                                                     if(err){
//                                                                                                                                                                                                                                         res.json("failed to fetch data");
//                                                                                                                                                                                                                                     }

//                                                                                                                                                                                                                                         var filterOrder = [];
//                                                                                                                                                                                                                                                      for(var x=0;x<orders.length;x++){
//                                                                                                                                                                                                                                                          for(var y=0;y<orders[x].orderDesc.length;y++){

//                                                                                                                                                                                                                                                             if(orders[x].orderDesc[y].productVendorId===vendorId){

//                                                                                                                                                                                                                                                                 filterOrder.push(orders[x]);
//                                                                                                                                                                                                                                                             }else{
//                                                                                                                                                                                                                                                                 console.log("Server Error");
//                                                                                                                                                                                                                                                             }
//                                                                                                                                                                                                                                                         }

//                                                                                                                                                                                                                                                     }
//                                                                                                                                                                                                                                                                 var newAmt=0;
//                                                                                                                                                                                                                                                                 for(var z=0;z<filterOrder.length;z++){

//                                                                                                                                                                                                                                                                     var toNum=Number(filterOrder[z].totalAmount);
//                                                                                                                                                                                                                                                                   var cd = filterOrder[z].createdDate;
//                                                                                                                                                                                                                                                                   newAmt += toNum;
//                                                                                                                                                                                                                                                                 }
//                                                                                                                                                                                                                                                                 var Day7=[{"amt":newAmt,"createdDate":cd}];

//             res.json({success:true,msg:'Order details fetch successfully',filterOrder:filterOrder,vendor:vendor,Day1:Day1,Day2:Day2,Day3:Day3,Day4:Day4,Day5:Day5,Day6:Day6,Day7:Day7})
//         });
//     });
//     });
// });
// });
// });
// });
// });
//     }
//     else{
//         res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
//     }

// });

//end

//get subscriptionDetails

apiRoutes.get('/getSubscriptionDetails/:userId/:tokenstr', function (req, res) {
  var userId = req.params.userId;
  var token = req.params.tokenstr;
  var isauth = route.memberinfo(token, userId);

  if (isauth) {
    ActiveSubscription.find(
      {
        userId: userId,
      },
      function (error, activesubscription) {
        Subscription.find({}, function (err, subscriptions) {
          if (err || error) {
            res.json('failed to fetch data');
          } else {
            res.json({
              success: true,
              msg: 'Subscription List fetched successfully',
              subscriptions: subscriptions,
            });
          }
        });
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//get products

apiRoutes.post('/getProducts', function (req, res) {
  var userId = req.body.userId;
  var token = req.body.token;
  var isauth = route.memberinfo(token, userId);
  var vendorId = req.body.vendorId;
  var productName = req.body.productName;

  if (isauth) {
    Product.findOne(
      {
        productVendorId: vendorId,
        productName: productName,
      },
      function (error, products) {
        if (error) {
          res.json('failed to fetch data');
        } else {
          res.json({
            success: true,
            msg: 'Subscription List fetched successfully',
            products: products,
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//get all product

apiRoutes.post('/getAllProducts', function (req, res) {
  var userId = req.body.userId;
  var token = req.body.token;
  var vendorId = req.body.vendorId;
  var isauth = route.memberinfo(token, userId);

  if (isauth) {
    Product.find(
      {
        productVendorId: vendorId,
      },
      function (err, data) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var products = [];
          for (var i = 0; i < data.length; i++) {
            if (data[i].isActive === true) {
              products.push(data[i]);
            } else {
              //no comment
            }
          }
        }

        res.json({
          success: true,
          msg: 'User details fetch successfully',
          products: products,
        });
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//get products by category

apiRoutes.post('/getProductsByCategory', function (req, res) {
  var userId = req.body.userId;
  var token = req.body.token;
  var vendorId = req.body.vendorId;
  var subCategoryId = req.body.subCategoryId;
  var isauth = route.memberinfo(token, userId);

  if (isauth) {
    Product.find(
      {
        productVendorId: vendorId,
        productSubCategoryId: subCategoryId,
      },
      function (err, data) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var products = [];
          for (var i = 0; i < data.length; i++) {
            if (data[i].isActive === true) {
              products.push(data[i]);
            } else {
              //no comment
            }
          }
        }

        res.json({
          success: true,
          msg: 'User details fetch successfully',
          products: products,
        });
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//get products by category
//get all product

apiRoutes.post('/getVendorProducts', function (req, res) {
  var vendorId = req.body.vendorId;
  var token = req.body.token;
  var subCategoryName = req.body.subCategoryName;
  var isauth = route.memberinfo(token, vendorId);

  if (isauth) {
    Product.find(
      {
        productVendorId: vendorId,
        productSubCategory: subCategoryName,
      },
      function (err, products) {
        if (err) {
          res.json('failed to fetch data');
        }

        res.json({
          success: true,
          msg: 'User details fetch successfully',
          products: products,
        });
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//active subscription

apiRoutes.get('/getActiveSubscription/:userId/:tokenstr', function (req, res) {
  var userId = req.params.userId;
  var token = req.params.tokenstr;
  var isauth = route.memberinfo(token, userId);

  if (isauth) {
    ActiveSubscription.find(
      {
        userId: userId,
      },
      function (err, activeSubscription) {
        if (err) {
          res.json('failed to fetch data');
        }
        var newSub = [];
        var today = moment();
        for (var i = 0; i < activeSubscription.length; i++) {
          if (activeSubscription[i].endDate > today) {
            activeSubscription.isActive = false;
            newSub.push(activeSubscription[i].orderDesc[0]);
          } else {
            newSub.push(activeSubscription[i].orderDesc[0]);
          }
        }

        res.json({
          success: true,
          msg: 'User details fetch successfully',
          subscriptions: activeSubscription,
          newSub: newSub,
        });
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Pause subscription

apiRoutes.post('/pauseSubscription', function (req, res) {
  var userId = req.body.userId;
  var token = req.body.token;

  var isauth = route.memberinfo(token, userId);
  var subscriptionId = req.body.subscriptionId;
  var vendorId = req.body.vendorId;
  var isPause = req.body.isPause;
  var tDate = Date.now();
  var todayDate = moment(tDate).toISOString();
  var showtDate = moment(todayDate).format('DD-MM-YYYY');

  if (isauth) {
    ActiveSubscription.update(
      {
        subscriptionId: subscriptionId,
      },
      {
        $set: {
          pauseDate: todayDate,
          isPause: isPause,
        },
      },
      function (err, activeSubscription) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'Subscription Failed to Pause',
          });
          //throw err;
          console.log(err);
        } else {
          res.json({
            success: true,
            msg: 'Subscription Pause Successfully',
          });
        }
      }
    );

    Vendor.findOne(
      {
        _id: vendorId,
      },
      function (err, vendor) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var vendorDevice = vendor.deviceId;
          axios
            .post(
              'https://fcm.googleapis.com/fcm/send',
              {
                notification: {
                  title: 'Subscription Pause Alert!',
                  body:
                    'The active Subscription Id: ' +
                    subscriptionId +
                    ' has been paused Date: ' +
                    showtDate,
                  sound: 'default',
                  click_action: 'FCM_PLUGIN_ACTIVITY',
                  icon: 'fcm_push_icon',
                },
                data: {
                  landing_page: 'tabs/tab2',
                  price: '$3,000.00',
                },
                to: vendorDevice,
                priority: 'high',
                restricted_package_name: '',
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization:
                    'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                },
              }
            )
            .then((res) => {
              console.log(`statusCode: ${res.statusCode}`);
              console.log(res);
            })
            .catch((error) => {
              console.error(error);
            });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Resume Subscription

apiRoutes.post('/resumeSubscription/', function (req, res) {
  var userId = req.body.userId;
  var token = req.body.token;
  var isauth = route.memberinfo(token, userId);
  var subscriptionId = req.body.subscriptionId;
  var vendorId = req.body.vendorId;
  var tDate = Date.now();
  var todayDate = moment(tDate);
  var toDate = moment(todayDate).format('DD-MM-YYYY');

  if (isauth) {
    console.log(subscriptionId);
    ActiveSubscription.findOne(
      {
        subscriptionId: subscriptionId,
      },
      function (err, subscriptionOrder) {
        if (err) {
          res.json('failed to fetch data');
        }
        var pdate = new Date(subscriptionOrder.pauseDate);
        var pauseDate = moment(pdate);
        var paDate = moment(pauseDate).format('DD-MM-YYYY');
        var eDate = new Date(subscriptionOrder.endDate);
        var endDate = moment(eDate);
        var ene = moment(endDate).format('DD-MM-YYYY');
        var days = todayDate.diff(pauseDate, 'days');

        var updatedEndDate = moment(endDate).add(days, 'days').toISOString();
        var showEDate = moment(updatedEndDate).format('DD-MM-YYYY');

        ActiveSubscription.update(
          {
            subscriptionId: subscriptionId,
          },
          {
            $set: {
              endDate: updatedEndDate,
              isPause: true,
            },
          },
          function (err, subscriptionOrder) {
            //handle it
            if (err) {
              res.json({
                success: false,
                msg: 'Subscription Failed to Update ',
              });
              //throw err;
              console.log(err);
            } else {
              res.json({
                success: true,
                msg: 'Subscription Resumed Successfully Updated',
              });
            }
          }
        );

        Vendor.findOne(
          {
            _id: vendorId,
          },
          function (err, vendor) {
            if (err) {
              res.json('failed to fetch data');
            } else {
              // var tAmt = cdata.totalAmount;
              var vendorDevice = vendor.deviceId;
              axios
                .post(
                  'https://fcm.googleapis.com/fcm/send',
                  {
                    notification: {
                      title: 'Subscription Reactivated',
                      body:
                        'The subscription of Subscription Id: ' +
                        subscriptionId +
                        ' is now Resumed. This Subscription will expire on' +
                        showEDate,
                      sound: 'default',
                      click_action: 'FCM_PLUGIN_ACTIVITY',
                      icon: 'fcm_push_icon',
                    },
                    data: {
                      landing_page: 'tabs/tab2',
                      price: '$3,000.00',
                    },
                    to: vendorDevice,
                    priority: 'high',
                    restricted_package_name: '',
                  },
                  {
                    headers: {
                      'Content-Type': 'application/json',
                      Authorization:
                        'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                    },
                  }
                )
                .then((res) => {
                  console.log(`statusCode: ${res.statusCode}`);
                  console.log(res);
                })
                .catch((error) => {
                  console.error(error);
                });
            }
          }
        );
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

// upade userDetails
apiRoutes.post('/updateUserDetails/:userId', function (req, res) {
  var userId = req.params.userId;
  console.log(userId);
  var isAddress = req.body.isAddress;
  var iscno = req.body.cno;
  var token = req.body.Authorization;
  console.log(token);
  // token= "JWT "+token;
  var isauth = route.memberinfo(token, userId);
  if (isauth) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this Contact Number exist',
          });
        } else {
          if (isAddress === false) {
            var address = req.body.add;
            console.log(address);

            User.update(
              {
                _id: userId,
              },
              {
                $set: {
                  add: address,
                },
              },
              function (err, user) {
                //handle it
                if (err) {
                  res.json({
                    success: false,
                    msg: 'Failed to Update Address',
                  });
                  //throw err;
                  console.log(err);
                } else {
                  res.json({
                    success: true,
                    msg: 'Address Successfully Updated',
                  });
                }
              }
            );
          } else {
            var contactNumber = req.body.contactNumber;

            User.update(
              {
                _id: userId,
              },
              {
                $set: {
                  contactNum: contactNumber,
                },
              },
              function (err, user) {
                //handle it
                if (err) {
                  res.json({
                    success: false,
                    msg: 'Failed to Update',
                  });
                  //throw err;
                  console.log(err);
                } else {
                  res.json({
                    success: true,
                    msg: 'User Successfully Updated',
                  });
                }
              }
            );
          }
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

apiRoutes.post('/updateUserDetails/:userId', function (req, res) {
  var token = getToken(req.headers);
  var userIdupdate = req.body.userId;
  var userId = req.params.userId;
  var isauth = route.memberinfo(token, userId);
  var user = req.body;
  var isPasswordChange = req.body.pwdmode;

  if (isauth) {
    if (isPasswordChange) {
      User.findOne(
        {
          _id: userIdupdate,
        },
        function (err, user) {
          if (!user) {
            res.json({
              success: false,
              msg: 'No user with this email id exist',
            });
          } else {
            var pass = req.body.pwd;

            user.password = pass;

            user.save(function (err, user) {
              User.update(
                {
                  _id: user._id,
                },
                {
                  $set: {
                    password: user.password,
                  },
                },
                function (err, user) {
                  if (err) throw err;
                  else {
                    res.json({
                      success: true,
                      msg: 'Password Successfully Updated',
                    });
                  }
                }
              );
            });
          }
        }
      );
    } else {
      User.update(
        {
          _id: userIdupdate,
        },
        user,
        function (err, numberAffected, rawResponse) {
          //handle it
          if (err) {
            res.json({
              success: false,
              msg: 'Failed to Update',
            });
            //throw err;
            console.log(err);
          } else {
            res.json({
              success: true,
              msg: 'User Successfully Updated',
            });
          }
        }
      );
    }
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//update vendorDetails

apiRoutes.post('/updateVendorDetails/:vendorId', function (req, res) {
  var vendorId = req.params.vendorId;
  console.log(vendorId);
  var isAddress = req.body.isAddress;
  var token = req.body.Authorization;
  console.log(token);
  // token= "JWT "+token;
  var isauth = route.memberinfo(token, vendorId);
  if (isauth) {
    Vendor.findOne(
      {
        _id: vendorId,
      },
      function (err, vendor) {
        if (!vendor) {
          res.json({
            success: false,
            msg: 'No vendor with this Contact Number exist',
          });
        } else {
          if (isAddress === false) {
            var address = req.body.add;

            Vendor.update(
              {
                _id: vendorId,
              },
              {
                $set: {
                  vendorAddress: address,
                },
              },
              function (err, vendor) {
                //handle it
                if (err) {
                  res.json({
                    success: false,
                    msg: 'Failed to Update Address',
                  });
                  //throw err;
                  console.log(err);
                } else {
                  res.json({
                    success: true,
                    msg: 'Address Successfully Updated',
                  });
                }
              }
            );
          }
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//update KYC

apiRoutes.post('/updateVendorKYC/:vendorId', function (req, res) {
  var vendorId = req.params.vendorId;
  console.log(vendorId);

  var token = req.body.Authorization;
  console.log(token);
  var KYCNumber = req.body.kyc;
  // token= "JWT "+token;
  var isauth = route.memberinfo(token, vendorId);
  if (isauth) {
    Vendor.findOne(
      {
        _id: vendorId,
      },
      function (err, vendor) {
        if (!vendor) {
          res.json({
            success: false,
            msg: 'No vendor with this ID exist',
          });
        } else {
          Vendor.update(
            {
              _id: vendorId,
            },
            {
              $set: {
                kyc: KYCNumber,
              },
            },
            function (err, vendor) {
              //handle it
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to Update KYC',
                });
                //throw err;
                console.log(err);
              } else {
                res.json({
                  success: true,
                  msg: 'KYC Successfully Updated',
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

apiRoutes.post('/setDeviceId/', function (req, res) {
  var userId = req.body.userId;
  var token = req.body.token;
  var isauth = route.memberinfo(token, userId);
  var deviceId = req.body.tokenDevice;
  console.log(deviceId);

  if (isauth) {
    User.update(
      {
        _id: userId,
      },
      {
        $set: {
          deviceId: deviceId,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Status Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Status Updated Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

apiRoutes.post(
  '/updateProduct',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;

    var productId = req.body.productId;
    var productName = req.body.productName;
    var productCost = parseInt(req.body.productCost);
    var productSell = parseInt(req.body.productsell);
    var productQuantity = req.body.productQuantity;
    var productCategoryId = req.body.productCategoryId;
    var productCategoryName = req.body.productCategoryName;

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      if (req.body.subCatImg == 'false') {
        Product.update(
          {
            _id: productId,
          },
          {
            $set: {
              productName: productName,
              productCost: productCost,
              productsell: productSell,
              productQuantity: productQuantity,
              categoryName: productCategoryName,
              categoryId: productCategoryId,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Product Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Product Updated Successfully',
              });
            }
          }
        );
      } else {
        Product.update(
          {
            _id: productId,
          },
          {
            $set: {
              productName: productName,
              productCost: productCost,
              productsell: productSell,
              productQuantity: productQuantity,
              categoryName: productCategoryName,
              categoryId: productCategoryId,
              productUrl: baseUrl + req.file.filename,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Product Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Product Updated Successfully',
              });
            }
          }
        );
      }
    } else {
      res.json({
        success: false,
        msg: 'Unable to fetch Token Authentication failed',
      });
    }
  }
);

//Add Latest News And Updates

apiRoutes.post(
  '/addNews',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var news = req.body;

    var isauth = route.memberinfo(token, userId);
    // token= "JWT "+token;
    var newsData = {
      newsTitle: news.newsTitle,
      newsDescription: news.newsDescription,
      newsType: news.newsType,
      newsVendorId: news.newsVendorId,
      newsVendorName: news.newsVendorName,
      newsUrl: baseUrl + req.file.filename,
    };

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      Feed.addFeed(newsData, function (err, newsData) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to add Request',
          });
          //	throw err;
        } else {
          res.json({
            success: true,
            msg: 'News and Updates Added Successfully',
            data: newsData,
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: 'Failed to update Token Authentication failed',
      });
    }
  }
);

//update Feed
apiRoutes.post(
  '/updateNews',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var feed = req.body;
    var feedId = req.body.feedId;
    console.log(feed);

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      if (req.body.subCatImg == 'false') {
        var feedData = {
          newsTitle: feed.newsTitle,
          newsDescription: feed.newsDescription,
        };

        Feed.update(
          {
            _id: feedId,
          },
          {
            $set: {
              newsTitle: feedData.newsTitle,
              newsDescription: feedData.newsDescription,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Store Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Store Updated Successfully',
              });
            }
          }
        );
      } else {
        var feedData = {
          newsTitle: feed.newsTitle,
          newsDescription: feed.newsDescription,
          feedUrl: baseUrl + req.file.filename,
        };

        Feed.update(
          {
            _id: trendId,
          },
          {
            $set: {
              newsTitle: feedData.newsTitle,
              newsDescription: feedData.newsDescription,
              newsUrl: feedData.feedUrl,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Store Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Store Updated Successfully',
              });
            }
          }
        );
      }
    } else {
      res.json({
        success: false,
        msg: 'Unable to fetch Token Authentication failed',
      });
    }
  }
);

//Update Store

apiRoutes.post(
  '/updateStore',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var store = req.body;
    var storeId = store.storeId;
    console.log(store);
    console.log(userId);
    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      if (req.body.subCatImg == 'false') {
        var storeData = {
          storeId: store.storeId,
          storeName: store.storeName,
          storeContactNumber: store.storeContactNumber,
          storeAddress: store.storeAddress,
        };

        Store.update(
          {
            _id: storeId,
          },
          {
            $set: {
              storeName: storeData.storeName,
              storeContactNumber: storeData.storeContactNumber,
              storeAddress: storeData.storeAddress,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Store Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Store Updated Successfully',
              });
            }
          }
        );
      } else {
        var storeData = {
          storeId: store.storeId,
          storeName: store.storeName,
          storeContactNumber: store.storeContactNumber,
          storeUrl: baseUrl + req.file.filename,
          storeAddress: store.storeAddress,
        };

        Store.update(
          {
            _id: storeId,
          },
          {
            $set: {
              storeName: storeData.storeName,
              storeContactNumber: storeData.storeContactNumber,
              storeAddress: storeData.storeAddress,
              storeUrl: storeData.storeUrl,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Store Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Store Updated Successfully',
              });
            }
          }
        );
      }
    } else {
      res.json({
        success: false,
        msg: 'Unable to fetch Token Authentication failed',
      });
    }
  }
);

//Update Vendor Cat

apiRoutes.post(
  '/updateVendorCat',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var vendorCat = req.body;
    var vendorCategoryId = req.body.vendorCategoryId;

    console.log(userId);
    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      if (req.body.subCatImg == 'false') {
        var vendCatData = {
          vendorCategoryName: vendorCat.vendorCategoryName,
        };

        vendorCategory.update(
          {
            _id: vendorCategoryId,
          },
          {
            $set: {
              vendorCategoryName: vendCatData.vendorCategoryName,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Store Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Store Updated Successfully',
              });
            }
          }
        );
      } else {
        var vendCatData = {
          vendorCategoryName: vendorCat.vendorCategoryName,

          vendorCategoryUrl: baseUrl + req.file.filename,
        };

        vendorCategory.update(
          {
            _id: vendorCategoryId,
          },
          {
            $set: {
              vendorCategoryName: vendCatData.vendorCategoryName,

              vendorCategoryUrl: vendCatData.vendorCategoryUrl,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Store Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Store Updated Successfully',
              });
            }
          }
        );
      }
    } else {
      res.json({
        success: false,
        msg: 'Unable to fetch Token Authentication failed',
      });
    }
  }
);

//Delete Feed

//delete expensehead using expensehead  id
apiRoutes.post('/deleteFeed', function (req, res) {
  var feedId = req.body.feedId;
  var token = getToken(req.headers);
  var userId = req.body.userId;
  var isauth = route.memberinfo(token, userId);
  if (isauth) {
    Feed.findByIdAndRemove(
      {
        _id: feedId,
      },
      {
        isActive: false,
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'No such Feed',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Feed Successfully Deactivated',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Not Authorised',
    });
  }
});

//delete Product

apiRoutes.post('/deleteProduct', function (req, res) {
  var productId = req.body.productId;
  var token = getToken(req.headers);
  var userId = req.body.userId;
  var isauth = route.memberinfo(token, userId);
  if (isauth) {
    Product.findByIdAndRemove(
      {
        _id: productId,
      },
      {
        isActive: false,
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'No such Product',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Product Removed Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Not Authorised',
    });
  }
});

//Delete Store

apiRoutes.post('/deleteStore', function (req, res) {
  var storeId = req.body.storeId;
  var token = getToken(req.headers);
  var userId = req.body.userId;
  var isauth = route.memberinfo(token, userId);
  if (isauth) {
    Store.findByIdAndRemove(
      {
        _id: storeId,
      },
      {
        isActive: false,
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'No Such Store',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Store Removed Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Not Authorised',
    });
  }
});

//Delete CAtegory

// apiRoutes.post('/deleteCategory', function(req, res){
// 	var categoryId = req.body.categoryId;
//       var token = getToken(req.headers);
//     var userId=req.body.userId;
//     var isauth= route.memberinfo(token,userId);
//     if(isauth){
//         Category.findByIdAndRemove({_id: categoryId}, {
//     isActive: false
// }, function(err, numberAffected, rawResponse) {
//    //handle it
//             if(err){
//             res.json({success: false,msg:'No such Feed'});
// 			throw err;
// 		}
//             else{
//                 res.json({success: true,msg:'Category Removed Successfully'});
//             }
// });

//     }

//     else{
//         res.json({success: false,msg:'Not Authorised'});
//     }
// });

//delete Subcategory

apiRoutes.post('/deleteSubCategory', function (req, res) {
  var subCategoryId = req.body.subCategoryId;
  var token = getToken(req.headers);
  var userId = req.body.userId;
  var isauth = route.memberinfo(token, userId);
  if (isauth) {
    SubCategory.findByIdAndRemove(
      {
        _id: subCategoryId,
      },
      {
        isActive: false,
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'No such Feed',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'SubCategory Removed Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Not Authorised',
    });
  }
});

//delete vendorCategory

apiRoutes.post('/deleteVendorCategory', function (req, res) {
  var vendorCategoryId = req.body.vendorCategoryId;
  var token = getToken(req.headers);
  var userId = req.body.userId;
  var isauth = route.memberinfo(token, userId);
  if (isauth) {
    vendorCategory.findByIdAndRemove(
      {
        _id: vendorCategoryId,
      },
      {
        isActive: false,
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'No such Feed',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Vendor Category Removed Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Not Authorised',
    });
  }
});

apiRoutes.post('/deleteVendor', function (req, res) {
  var vendorId = req.body.vendorId;
  var token = getToken(req.headers);
  var userId = req.body.userId;
  var isauth = route.memberinfo(token, userId);
  if (isauth) {
    Vendor.findByIdAndRemove(
      {
        _id: vendorId,
      },
      {
        isActive: false,
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'No such Feed',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Vendor  Removed Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Not Authorised',
    });
  }
});
//Add Type and trends

apiRoutes.post(
  '/addTrend',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var trends = req.body;

    var isauth = route.memberinfo(token, userId);
    var trendData = {
      trendTitle: trends.trendTitle,
      trendType: trends.trendType,
      trendDescription: trends.trendDescription,
      trendVendorId: trends.trendVendorId,
      trendVendorName: trends.trendVendorName,

      trendUrl: baseUrl + req.file.filename,
    };

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      Trend.addTrend(trendData, function (err, trendData) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to add Request',
          });
          //	throw err;
        } else {
          res.json({
            success: true,
            msg: 'News and Updates Added Successfully',
            data: trendData,
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: 'Failed to update Token Authentication failed',
      });
    }
  }
);

//update Trend
apiRoutes.post(
  '/updateTrend',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var trend = req.body;
    var trendId = req.body.trendId;
    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      if (req.body.subCatImg == 'false') {
        var trendData = {
          trendTitle: trend.trendTitle,
          trendDescription: trend.trendDescription,
        };

        Trend.update(
          {
            _id: trendId,
          },
          {
            $set: {
              trendDescription: trendData.trendDescription,
              trendTitle: trendData.trendTitle,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Store Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Store Updated Successfully',
              });
            }
          }
        );
      } else {
        var trendData = {
          trendTitle: trend.trendTitle,
          trendDescription: trend.trendDescription,
          trendUrl: baseUrl + req.file.filename,
        };

        Trend.update(
          {
            _id: trendId,
          },
          {
            $set: {
              trendTitle: trendData.trendTitle,
              trendDescription: trendData.trendDescription,
              trendUrl: trendData.trendUrl,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Store Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Store Updated Successfully',
              });
            }
          }
        );
      }
    } else {
      res.json({
        success: false,
        msg: 'Unable to fetch Token Authentication failed',
      });
    }
  }
);

//Delete Feed

//delete expensehead using expensehead  id
apiRoutes.post('/deleteTrend', function (req, res) {
  var trendId = req.body.trendId;
  var token = getToken(req.headers);
  var userId = req.body.userId;
  var isauth = route.memberinfo(token, userId);
  if (isauth) {
    Trend.findByIdAndRemove(
      {
        _id: trendId,
      },
      {
        isActive: false,
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'No such Trend',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Trend Successfully Deactivated',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Not Authorised',
    });
  }
});

//Subscription

apiRoutes.post(
  '/addSubscription',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;
    var subscriptions = req.body;

    var isauth = route.memberinfo(token, userId);
    // token= "JWT "+token;
    var subscriptionData = {
      subscriptionVendorId: req.body.subscriptionVendorId,
      subscriptionVendorName: req.body.subscriptionVendorName,
      subscriptionName: subscriptions.subscriptionName,
      subscriptionDescription: subscriptions.subscriptionDescription,
      subscriptionAmount: subscriptions.subscriptionAmount,
      subscriptionOfferPrice: subscriptions.subscriptionOfferPrice,

      subscriptionUrl: baseUrl + req.file.filename,
    };

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      Subscription.addSubscription(
        subscriptionData,
        function (err, subscriptionData) {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Failed to add Request',
            });
            //	throw err;
          } else {
            res.json({
              success: true,
              msg: 'News and Updates Added Successfully',
              data: subscriptionData,
            });
          }
        }
      );
    } else {
      res.json({
        success: false,
        msg: 'Failed to update Token Authentication failed',
      });
    }
  }
);

//update Subscription
apiRoutes.post(
  '/updateSubscription',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    var userId = req.body.userId;

    var subscriptionId = req.body.subscriptionId;
    var subscription = req.body;

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      if (req.body.subCatImg == 'false') {
        var subscriptionData = {
          subscriptionName: subscription.subscriptionName,
          subscriptionDescription: subscription.subscriptionDescription,
          subscriptionAmount: subscription.subscriptionAmount,
          subscriptionOfferPrice: subscription.subscriptionOfferPrice,
        };

        Subscription.update(
          {
            _id: subscriptionId,
          },
          {
            $set: {
              subscriptionName: subscriptionData.subscriptionName,
              subscriptionDescription: subscriptionData.subscriptionDescription,
              subscriptionAmount: subscriptionData.subscriptionAmount,
              subscriptionOfferPrice: subscriptionData.subscriptionOfferPrice,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Store Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Store Updated Successfully',
              });
            }
          }
        );
      } else {
        var subscriptionData = {
          subscriptionName: subscription.subscriptionName,
          subscriptionDescription: subscription.subscriptionDescription,
          subscriptionAmount: subscription.subscriptionAmount,
          subscriptionOfferPrice: subscription.subscriptionOfferPrice,
          subscriptionUrl: subscription.subscriptionUrl,
        };
        Subscription.update(
          {
            _id: subscriptionId,
          },
          {
            $set: {
              subscriptionName: subscriptionData.subscriptionName,
              subscriptionDescription: subscriptionData.subscriptionDescription,
              subscriptionAmount: subscriptionData.subscriptionAmount,
              subscriptionOfferPrice: subscriptionData.subscriptionOfferPrice,
              subscriptionUrl: subscriptionData.subscriptionUrl,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Store Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Store Updated Successfully',
              });
            }
          }
        );
      }
    } else {
      res.json({
        success: false,
        msg: 'Unable to fetch Token Authentication failed',
      });
    }
  }
);

//Delete Feed

//delete expensehead using expensehead  id
apiRoutes.post('/deleteSubscription', function (req, res) {
  var subscriptionId = req.body.subscriptionId;
  var token = getToken(req.headers);
  var userId = req.body.userId;
  var isauth = route.memberinfo(token, userId);
  if (isauth) {
    Subscription.findByIdAndRemove(
      {
        _id: subscriptionId,
      },
      {
        isActive: false,
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'No such Subscription',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Subscription Successfully Deactivated',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Not Authorised',
    });
  }
});

//SubscriptionOrder

apiRoutes.post('/subscriptionOrder', function (req, res) {
  var txamt = req.body.txAmt;

  var token = req.body.token;
  var userId = req.body.userId;
  var userName = req.body.userName;
  var count = req.body.count;
  var selectedItems = req.body.selectedItems;
  var isauth = route.memberinfo(token, userId);
  var subscriptionVendorId = selectedItems[0].subscriptionVendorId;
  var sdate = Date.now();
  var startDate = moment(sdate);

  var endDate = moment(startDate).add(1, 'months').calendar();
  var ea = moment(endDate);
  var showea = moment(ea).format('MMM Do YY');
  var randomstring2 = require('randomstring');
  var subId = randomstring2.generate(8);
  selectedItems[0].subId = subId;
  var cdata = {
    subscriptionId: subId,

    orderDesc: selectedItems,

    totalAmount: txamt,
    startDate: startDate,
    endDate: ea,
    pauseDate: sdate,
    userId: userId,
    userName: userName,
  };
  if (isauth) {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          cdata.add = user.add;
          ActiveSubscription.addsubscriptionOrder(
            cdata,
            function (err, suborder) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to add Request',
                });
                //	throw err;
              }

              res.json({
                success: true,
                msg: 'Request Sent Successfully',
                subscriptionorder: suborder,
              });
            }
          );
        }
      }
    );

    Vendor.findOne(
      {
        _id: subscriptionVendorId,
      },
      function (err, vendor) {
        if (err) {
          res.json('failed to fetch data');
        } else {
          var subscriptionId = cdata.subscriptionId;
          var tAmt = cdata.totalAmount;
          var vendorDevice = vendor.deviceId;
          axios
            .post(
              'https://fcm.googleapis.com/fcm/send',
              {
                notification: {
                  title: 'New Active Subscription',
                  body:
                    'Subscription Id: ' +
                    subscriptionId +
                    ' Amount: ' +
                    tAmt +
                    '.This subscription will expire on ' +
                    showea +
                    '.',
                  sound: 'default',
                  click_action: 'FCM_PLUGIN_ACTIVITY',
                  icon: 'fcm_push_icon',
                },
                data: {
                  landing_page: 'tabs/tab2',
                  price: '$3,000.00',
                },
                to: vendorDevice,
                priority: 'high',
                restricted_package_name: '',
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization:
                    'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                },
              }
            )
            .then((res) => {
              console.log(`statusCode: ${res.statusCode}`);
              console.log(res);
            })
            .catch((error) => {
              console.error(error);
            });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed to add Request Token Authentication failed',
    });
  }
});

//Pause subscription

apiRoutes.post('/pauseProduct', function (req, res) {
  var token = req.body.token;
  var vendorId = req.body.vendorId;
  var isauth = route.memberinfo(token, vendorId);
  var productId = req.body.productId;

  var isActive = req.body.isActive;

  if (isauth) {
    Product.update(
      {
        _id: productId,
      },
      {
        $set: {
          isActive: isActive,
        },
      },
      function (err, product) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'Product Failed to Pause',
          });
          //throw err;
          console.log(err);
        } else {
          res.json({
            success: true,
            msg: 'Product Paused Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//Resume Subscription

apiRoutes.post('/resumeProduct/', function (req, res) {
  var token = req.body.token;
  var vendorId = req.body.vendorId;
  var isauth = route.memberinfo(token, vendorId);
  var productId = req.body.productId;

  var isActive = req.body.isActive;

  if (isauth) {
    console.log(productId);

    ActiveSubscription.update(
      {
        _id: productId,
      },
      {
        $set: {
          isActive: isActive,
        },
      },
      function (err, product) {
        //handle it
        if (err) {
          res.json({
            success: false,
            msg: 'Product Failed to Update ',
          });
          //throw err;
          console.log(err);
        } else {
          res.json({
            success: true,
            msg: 'Product Resumed Successfully Updated',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

//raw

apiRoutes.post('/updateBrand', function (req, res) {
  var token = getToken(req.headers);
  var userId = req.body.userId;

  var brandId = req.body.brandId;
  var brandName = req.body.brandName;

  var isauth = route.memberinfo(token, userId);

  if (isauth) {
    Brand.update(
      {
        _id: brandId,
      },
      {
        $set: {
          brandName: brandName,
        },
      },
      function (err, numberAffected, rawResponse) {
        //handle it
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Brand Failed To Update',
          });
          throw err;
        } else {
          res.json({
            success: true,
            msg: 'Brand Updated Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

apiRoutes.post(
  '/updatevendor',
  uploadMiddleware.single('attachment'),
  function (req, res, next) {
    var token = getToken(req.headers);
    console.log(JSON.stringify(req.body));
    var userId = req.body.userId;
    var vendorId = req.body.vendorId;
    var vendorCategoryId = req.body.vendorCategoryId;
    var vendorCategoryName = req.body.vendorCategoryName;

    var isauth = route.memberinfo(token, userId);

    if (isauth) {
      if (req.body.subCatImg == 'false') {
        Vendor.update(
          {
            _id: vendorId,
          },
          {
            $set: {
              vendorCategoryId: vendorCategoryId,
              vendorCategoryName: vendorCategoryName,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Vendor Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Vendor Updated Successfully',
              });
            }
          }
        );
      } else {
        Vendor.update(
          {
            _id: vendorId,
          },
          {
            $set: {
              vendorCategoryId: vendorCategoryId,
              vendorCategoryName: vendorCategoryName,
              vendorUrl: baseUrl + req.file.filename,
            },
          },
          function (err, numberAffected, rawResponse) {
            //handle it
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Vendor Failed To Update',
              });
              throw err;
            } else {
              res.json({
                success: true,
                msg: 'Vendor Updated Successfully',
              });
            }
          }
        );
      }
    } else {
      res.json({
        success: false,
        msg: 'Unable to fetch Token Authentication failed',
      });
    }
  }
);

// user forget password

// apiRoutes.post('/getotp', function(req, res){

//     if(req.body.contactNum)
//         {
//             User.findOne({ 'contactNum': req.body.contactNum }, function(err, user) {

//          if(!user){
//              res.json({success: false, msg: 'No user with this mobile number exist'});
//          }

//          else{

//             options = {
//                  min:0000,
//                  max:  9999
//               ,
//               integer: true
//               }
//               var pass =  randomNumber(options)

//              user.password=pass;

//                 User.update({'contactNum': req.body.contactNum}, { $set: {password:user.password}}, function (err, userupdate) {
//                     //handle it
//                              if(err){
//                              res.json({success: false,msg:'Failed to Update Address'});
//                              //throw err;
//                              console.log(err);
//                          }

//          //push nitification
//          var deviceId=user.deviceId;
//          var otp = user.password;
//          var userName = user.userName;
//          axios
//          .post('https://fcm.googleapis.com/fcm/send', {
//            "notification":{
//              "title":"OTP",
//              "body":"This is your otp "+otp+".",
//              "sound":"default",
//              "click_action":"FCM_PLUGIN_ACTIVITY",
//              "icon":"fcm_push_icon"
//            },
//            "data":{
//              "landing_page":"login",

//            },
//              "to":deviceId,
//              "priority":"high",
//              "restricted_package_name":""
//          },
//          {
//            headers: {
//             'Content-Type':'application/json',
//              'Authorization': 'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y'
//            }
//        })
//          .then(res => {
//            console.log(`statusCode: ${res.statusCode}`)
//            console.log(res)
//          })
//          .catch(error => {
//            console.error(error)
//          })

//          res.json({success: true,msg:'otp sent Successfully',user:userupdate});
//  });

//     }
// });
//         }else{
//             res.json({success: false,msg:'Failed To send otp'});
//         }

//     });

apiRoutes.post('/verifyOtp', function (req, res) {
  if (req.body.pass) {
    User.findOne(
      {
        contactNum: req.body.contactNum,
        password: req.body.pass,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this mobile number exist',
          });
        } else {
          res.json({
            success: true,
            msg: 'Password set',
          });
        }
      }
    );
  }
});

apiRoutes.post('/setUserPass', function (req, res) {
  if (req.body.contactNum) {
    User.findOne(
      {
        contactNum: req.body.contactNum,
      },
      function (err, user) {
        if (!user) {
          res.json({
            success: false,
            msg: 'No user with this mobile number exist',
          });
        } else {
          var pass = req.body.pass;

          user.password = pass;

          user.save(function (err, user) {
            User.update(
              {
                contactNum: req.body.contactNum,
              },
              {
                $set: {
                  password: user.password,
                },
              },
              function (err, userupdate) {
                //handle it
                if (err) {
                  res.json({
                    success: false,
                    msg: 'Failed to Update password',
                  });
                  //throw err;
                  console.log(err);
                }
              }
            );
          });
          res.json({
            success: true,
            msg: 'Password set',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'No contact exist',
    });
  }
});

//vendor forget password

apiRoutes.post('/getVendorOtp', function (req, res) {
  if (req.body.contactNum) {
    Vendor.findOne(
      {
        vendorContactNumber: req.body.contactNum,
      },
      function (err, vendor) {
        if (!vendor) {
          res.json({
            success: false,
            msg: 'No user with this mobile number exist',
          });
        } else {
          options = {
            min: 0000,
            max: 9999,
            integer: true,
          };
          var pass = randomNumber(options);

          vendor.password = pass;

          Vendor.update(
            {
              vendorContactNumber: req.body.contactNum,
            },
            {
              $set: {
                password: vendor.password,
              },
            },
            function (err, vendorupdate) {
              //handle it
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to Update Address',
                });
                //throw err;
                console.log(err);
              }

              //push nitification
              var deviceId = vendor.deviceId;
              var otp = vendor.password;

              axios
                .post(
                  'https://fcm.googleapis.com/fcm/send',
                  {
                    notification: {
                      title: 'OTP',
                      body: 'This is your otp ' + otp + '.',
                      sound: 'default',
                      click_action: 'FCM_PLUGIN_ACTIVITY',
                      icon: 'fcm_push_icon',
                    },
                    data: {
                      landing_page: 'tabs/tab1',
                    },
                    to: deviceId,
                    priority: 'high',
                    restricted_package_name: '',
                  },
                  {
                    headers: {
                      'Content-Type': 'application/json',
                      Authorization:
                        'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y',
                    },
                  }
                )
                .then((res) => {
                  console.log(`statusCode: ${res.statusCode}`);
                  console.log(res);
                })
                .catch((error) => {
                  console.error(error);
                });

              res.json({
                success: true,
                msg: 'otp sent Successfully',
                vendor: vendorupdate,
              });
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Failed To send otp',
    });
  }
});

apiRoutes.post('/verifyVendorOtp', function (req, res) {
  if (req.body.pass) {
    Vendor.findOne(
      {
        vendorContactNumber: req.body.contactNum,
        password: req.body.pass,
      },
      function (err, vendor) {
        if (!vendor) {
          res.json({
            success: false,
            msg: 'No user with this mobile number exist',
          });
        } else {
          res.json({
            success: true,
            msg: 'Password set',
          });
        }
      }
    );
  }
});

apiRoutes.post('/setVendorPass', function (req, res) {
  if (req.body.contactNum) {
    Vendor.findOne(
      {
        vendorContactNumber: req.body.contactNum,
      },
      function (err, vendor) {
        if (!vendor) {
          res.json({
            success: false,
            msg: 'No user with this mobile number exist',
          });
        } else {
          var pass = req.body.pass;

          vendor.password = pass;

          vendor.save(function (err, user) {
            Vendor.update(
              {
                vendorContactNumber: req.body.contactNum,
              },
              {
                $set: {
                  password: vendor.password,
                },
              },
              function (err, userupdate) {
                //handle it
                if (err) {
                  res.json({
                    success: false,
                    msg: 'Failed to Update password',
                  });
                  //throw err;
                  console.log(err);
                }
              }
            );
          });
          res.json({
            success: true,
            msg: 'Password set',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'No contact exist',
    });
  }
});

//getUserReviews

apiRoutes.post('/getUserReviews', function (req, res) {
  var vendorId = req.body.vendorId;
  var token = req.body.token;
  var isauth = route.memberinfo(token, vendorId);

  // var productName = req.body.productName;

  if (isauth) {
    Order.find({}, function (error, orders) {
      if (error) {
        res.json('failed to fetch data');
      } else {
        var review = [];
        var filterOrder = [];
        for (var x = 0; x < orders.length; x++) {
          for (var y = 0; y < orders[x].orderDesc.length; y++) {
            if (
              orders[x].orderDesc[y].productVendorId === vendorId &&
              orders[x].isCancelled === false &&
              orders[x].isRate === true
            ) {
              filterOrder.push(orders[x]);
            } else {
              console.log('Order id does not match');
            }
          }
        }
        var odata = [];
        var comp = [];
        var newOrder = [];
        for (var k = 0; k < filterOrder.length; k++) {
          if (filterOrder[k].status === 'Accepted') {
            odata.push(filterOrder[k]);
          } else if (
            filterOrder[k].status === 'Completed' &&
            filterOrder[k].rate != 0
          ) {
            comp.push(filterOrder[k]);
          } else {
            newOrder.push(filterOrder[k]);
          }
        }

        res.json({
          success: true,
          msg: 'Subscription List fetched successfully',
          review: comp,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Unable to fetch Token Authentication failed',
    });
  }
});

// ALDER ERP API's Start

// signup api

apiRoutes.post('/signUserApi', function (req, res) {
  var userName = req.body.userName;
  var password = req.body.password;
  var contactNum = req.body.contactNum;
  var userType = req.body.userType;
  var email = req.body.email;
  var userAddress = req.body.userAddress;
  var newUser = {
    userName: userName,
    password: password,
    contactNum: contactNum,
    userType: userType,
    email: email,
    userAddress: userAddress,
  };
  NewUser.findOne(
    {
      contactNum: contactNum,
    },
    function (err, user) {
      if (user === null) {
        NewUser.addNewUser(newUser, function (err, newUser) {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Failed to add user',
            });
          } else {
            res.json({
              success: true,
              msg: 'User added',
              data: newUser,
            });
          }
        });
        // console.log(user);
        // res.json({success: true, msg: 'User details sent successfully.',data:user});
      } else {
        res.json({
          success: false,
          msg: 'User exists.',
        });
      }
    }
  );
});

// add user api

apiRoutes.post('/addUserApi', function (req, res) {
  var userName = req.body.userName;
  var password = req.body.password;
  var contactNum = req.body.contactNum;
  var userType = req.body.userType;
  var email = req.body.email;
  var userAddress = req.body.userAddress;
  var newUser = {
    userName: userName,
    password: password,
    contactNum: contactNum,
    userType: userType,
    email: email,
    userAddress: userAddress,
  };
  NewUser.findOne(
    {
      contactNum: contactNum,
    },
    function (err, user) {
      if (user === null) {
        NewUser.addNewUser(newUser, function (err, newUser) {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Failed to add user',
            });
          } else {
            res.json({
              success: true,
              msg: 'User added',
              data: newUser,
            });
          }
        });
      } else {
        res.json({
          success: false,
          msg: 'User exists.',
          data: user,
        });
      }
    }
  );
});

// edit user api

apiRoutes.post('/editUserApi', function (req, res) {
  var userName = req.body.userName;
  var password = req.body.password;
  var contactNum = req.body.contactNum;
  var userType = req.body.userType;
  var email = req.body.email;
  var userAddress = req.body.userAddress;
  var userId = req.body.userId;
  if (userId != undefined) {
    NewUser.findOneAndUpdate(
      {
        _id: userId,
      },
      {
        $set: {
          userName: userName,
          password: password,
          contactNum: contactNum,
          userType: userType,
          email: email,
          userAddress: userAddress,
        },
      },
      function (err, user) {
        if (err) {
          res.json({
            success: false,
            msg: 'Failed to edit user',
          });
        } else {
          NewUser.findOne(
            {
              _id: userId,
            },
            function (err, updatedUser) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to edit user.',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'User edited successfully.',
                  data: updatedUser,
                });
              }
            }
          );
        }
      }
    );
  }
});

// get all user api

apiRoutes.post('/getAllUserApi', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    NewUser.find({}, function (err, users) {
      if (err) {
        res.json({
          success: false,
          msg: 'Failed to get user.',
        });
      } else {
        res.json({
          success: true,
          msg: 'User request sent successfully.',
          data: users,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

//delete user api

apiRoutes.post('/deleteUserApi', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    NewUser.findByIdAndRemove(
      {
        _id: userId,
      },
      function (err, numberAffected, rawResponse) {
        if (err) {
          res.json({
            success: false,
            msg: 'No User Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'User deleted Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed',
    });
  }
});

// login api

apiRoutes.post('/loginApi', function (req, res) {
  var contactNum = req.body.contactNum;
  var password = req.body.password;
  var deviceId = req.body.deviceId;
  console.log(req.body);
  NewUser.findOne(
    {
      contactNum: contactNum,
    },
    function (err, user) {
      if (!user) {
        console.log(err);
        res.json({
          success: false,
          msg: 'No User Found.',
        });
      } else {
        // NewUser.findOneAndUpdate({"contactNum":contactNum},{$set:{"userName":user.userName,
        // "userType":user.userType,"contactNum":contactNum,"email":user.email,"password":user.password,
        // "userAddress":user.userAddress,"deviceId":deviceId}},function(err,user1){
        //     if (err){
        //         console.log(err);
        //         res.json({success: false, msg: 'Failed to update device id'});
        //     }else {
        //         NewUser.findOne({"contactNum":contactNum},function(err, updatedUser) {
        //             if (err){
        //                 console.log(err);
        //                 res.json({success: false, msg: 'No User Found.'});
        //             }else {
        if (user.password == password) {
          res.json({
            success: true,
            msg: 'Sucessfully Logged in',
            data: user,
          });
        } else {
          res.json({
            success: false,
            msg: 'Authentication failed. Wrong password.',
          });
        }
        //             }
        //         });
        //     }
        // });
      }
    }
  );
});

// update user api

apiRoutes.post('/updateUserApi', function (req, res) {
  var userId = req.body.userId;
  var deviceId = req.body.deviceId;
  NewUser.findOne(
    {
      _id: userId,
    },
    function (err, user) {
      if (!user) {
        console.log(err);
        res.json({
          success: false,
          msg: 'No User Found.',
        });
      } else {
        NewUser.findOneAndUpdate(
          {
            _id: userId,
          },
          {
            $set: {
              userName: user.userName,
              email: user.email,
              userType: user.userType,
              contactNum: user.contactNum,
              password: user.password,
              userAddress: user.userAddress,
              deviceId: deviceId,
            },
          },
          function (err, user1) {
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Failed to update device id',
              });
            } else {
              NewUser.findOne(
                {
                  _id: userId,
                },
                function (err, updatedUser) {
                  if (err) {
                    console.log(err);
                    res.json({
                      success: false,
                      msg: 'No User Found.',
                    });
                  } else {
                    res.json({
                      success: true,
                      msg: 'Successfully updated device id',
                      data: updatedUser,
                    });
                  }
                }
              );
            }
          }
        );
      }
    }
  );
});

// request otp api

apiRoutes.post('/getOtp', function (req, res) {
  var contactNum = req.body.contactNum;
  var OTP = makeid(6);
  if (contactNum != undefined) {
    NewUser.findOne(
      {
        contactNum: contactNum,
      },
      function (err, user) {
        if (err) {
          res.json({
            success: false,
            msg: 'No User Found.',
          });
        } else {
          var options = {
            authorization:
              'xwKGnLJ9D8bmqkQ9oL5OEj3oXXBlR5hBUSbZxVyeiEN0Vi0Bw1PRlexCcNL9',
            message: 'Your OTP is ' + OTP,
            numbers: [contactNum],
          };
          fast2sms.sendMessage(options).then((response) => {
            console.log(response);
            console.log(response.return);
            res.json({
              success: true,
              msg: 'Otp Sent',
              data: user,
              otp: OTP,
            });
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// forgot password api

apiRoutes.post('/forgetPassword', function (req, res) {
  var contactNum = req.body.contactNum;
  var password = req.body.newPassword;
  if (contactNum != undefined) {
    NewUser.findOneAndUpdate(
      {
        contactNum: contactNum,
      },
      {
        $set: {
          password: password,
        },
      },
      function (err, user) {
        if (err) {
          res.json({
            success: false,
            msg: 'No User Found.',
          });
        } else {
          console.log(user);
          NewUser.findOne(
            {
              contactNum: contactNum,
            },
            function (err, updatedUser) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'No User Found.',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'Password Updated Sucessfully',
                  data: updatedUser,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// add product part api

apiRoutes.post('/addProductPart', function (req, res) {
  var partName = req.body.partName;
  var code = req.body.code;
  var status = req.body.status;
  var length = req.body.length;
  var width = req.body.width;
  var height = req.body.height;
  var weight = req.body.weight;
  var partDetails = {
    partName: partName,
    code: code,
    status: status,
    length: length,
    width: width,
    height: height,
    weight: weight,
  };
  ProductPart.addParts(partDetails, function (err, part) {
    if (err) {
      res.json({
        success: false,
        msg: 'Failed to add product part',
      });
    } else {
      res.json({
        success: true,
        msg: 'Product part added',
        data: part,
      });
    }
  });
});

// edit product part api

apiRoutes.post('/editProductPart', function (req, res) {
  var partId = req.body.partId;
  var partName = req.body.partName;
  var code = req.body.code;
  var status = req.body.status;
  var length = req.body.length;
  var width = req.body.width;
  var height = req.body.height;
  var weight = req.body.weight;
  if (partId != undefined) {
    ProductPart.findOneAndUpdate(
      {
        _id: partId,
      },
      {
        $set: {
          partName: partName,
          code: code,
          status: status,
          length: length,
          width: width,
          height: height,
          weight: weight,
        },
      },
      function (err, part) {
        if (err) {
          res.json({
            success: false,
            msg: 'Failed to edit product part',
          });
        } else {
          ProductPart.findOne(
            {
              _id: partId,
            },
            function (err, updatedPart) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to edit product part',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'Part edited successfully',
                  data: updatedPart,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// get all product part api

apiRoutes.post('/getAllProductPart', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    ProductPart.find({}, function (err, parts) {
      if (err) {
        res.json({
          success: false,
          msg: 'No product parts',
        });
      } else {
        res.json({
          success: true,
          msg: 'Request sent successfully',
          data: parts,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// delete product parts api

apiRoutes.post('/deleteProductPart', function (req, res) {
  var partId = req.body.partId;
  if (partId != undefined) {
    ProductPart.findByIdAndRemove(
      {
        _id: partId,
      },
      function (err, numberAffected, rawResponse) {
        if (err) {
          res.json({
            success: false,
            msg: 'No Parts Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Parts deleted Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// add series api

apiRoutes.post('/addSeries', function (req, res) {
  var seriesName = req.body.seriesName;
  var code = req.body.code;
  var status = req.body.status;
  var seriesDetails = {
    seriesName: seriesName,
    code: code,
    status: status,
  };
  Series.addSeries(seriesDetails, function (err, series) {
    if (err) {
      res.json({
        success: false,
        msg: 'Failed to add series',
      });
    } else {
      res.json({
        success: true,
        msg: 'Series added',
        data: series,
      });
    }
  });
});

// edit series api

apiRoutes.post('/editSeries', function (req, res) {
  var seriesId = req.body.seriesId;
  var seriesName = req.body.seriesName;
  var code = req.body.code;
  var status = req.body.status;
  if (seriesId != undefined) {
    Series.findOneAndUpdate(
      {
        _id: seriesId,
      },
      {
        $set: {
          seriesName: seriesName,
          code: code,
          status: status,
        },
      },
      function (err, series) {
        if (err) {
          res.json({
            success: false,
            msg: 'Failed to edit series',
          });
        } else {
          Series.findOne(
            {
              _id: seriesId,
            },
            function (err, updatedData) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to edit series',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'Series edited successfully',
                  data: updatedData,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// get all series api

apiRoutes.post('/getAllSeries', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    Series.find({}, function (err, series) {
      if (err) {
        res.json({
          success: false,
          msg: 'No series found',
        });
      } else {
        res.json({
          success: true,
          msg: 'Request sent successfully',
          data: series,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// delete series api

apiRoutes.post('/deleteSeries', function (req, res) {
  var seriesId = req.body.seriesId;
  if (seriesId != undefined) {
    Series.findByIdAndRemove(
      {
        _id: seriesId,
      },
      function (err, numberAffected, rawResponse) {
        if (err) {
          res.json({
            success: false,
            msg: 'No Series Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Series deleted Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// add category api

apiRoutes.post('/addCategory', function (req, res) {
  var categoryName = req.body.categoryName;
  var code = req.body.code;
  var status = req.body.status;
  var categoryDetails = {
    categoryName: categoryName,
    code: code,
    status: status,
  };
  Categories.addCategories(categoryDetails, function (err, category) {
    if (err) {
      res.json({
        success: false,
        msg: 'Failed to add category',
      });
    } else {
      res.json({
        success: true,
        msg: 'Category added',
        data: category,
      });
    }
  });
});

// edit category api

apiRoutes.post('/editCategory', function (req, res) {
  var categoryId = req.body.categoryId;
  var categoryName = req.body.categoryName;
  var code = req.body.code;
  var status = req.body.status;
  if (categoryId != undefined) {
    Categories.findOneAndUpdate(
      {
        _id: categoryId,
      },
      {
        $set: {
          categoryName: categoryName,
          code: code,
          status: status,
        },
      },
      function (err, series) {
        if (err) {
          res.json({
            success: false,
            msg: 'Failed to edit category',
          });
        } else {
          Categories.findOne(
            {
              _id: categoryId,
            },
            function (err, updatedData) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to edit category',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'category edited successfully',
                  data: updatedData,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// get all category api

apiRoutes.post('/getAllCategory', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    Categories.find({}, function (err, categories) {
      if (err) {
        res.json({
          success: false,
          msg: 'No category found',
        });
      } else {
        res.json({
          success: true,
          msg: 'Request sent successfully',
          data: categories,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// delete category api

apiRoutes.post('/deleteCategory', function (req, res) {
  var categoryId = req.body.categoryId;
  if (categoryId != undefined) {
    Categories.findByIdAndRemove(
      {
        _id: categoryId,
      },
      function (err, numberAffected, rawResponse) {
        if (err) {
          res.json({
            success: false,
            msg: 'No Category Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Category deleted Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// get Order Details api

apiRoutes.post('/getOrderDetails', function (req, res) {
  var orderId = req.body.orderId;
  if (orderId != undefined) {
    Order.findOne(
      {
        _id: orderId,
      },
      function (err, order) {
        if (err) {
          res.json({
            success: false,
            msg: 'No Order Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Request Sent Successfully',
            data: order,
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// add partner api

apiRoutes.post('/addPartner', function (req, res) {
  var name = req.body.name;
  var contactNum = req.body.contactNum;
  var location = req.body.location;
  var status = req.body.status;
  var address = req.body.address;
  // var createdDate=req.body.createdDate;
  var partnerData = {
    name: name,
    contactNum: contactNum,
    location: location,
    status: status,
    address: address,
    // createdDate:createdDate,
  };
  Partner.addPartner(partnerData, function (err, partner) {
    // NewUser.addNewUser(partnerData,function(err,partner){
    if (err) {
      res.json({
        success: false,
        msg: 'Failed to add partner.',
      });
    } else {
      res.json({
        success: true,
        msg: 'Partner added successfully.',
        data: partner,
      });
    }
  });
});

// edit partner api

apiRoutes.post('/editPartner', function (req, res) {
  var partnerId = req.body.partnerId;
  var name = req.body.name;
  var contactNum = req.body.contactNum;
  var location = req.body.location;
  var status = req.body.status;
  var address = req.body.address;
  // var createdDate=req.body.createdDate;
  if (partnerId != undefined) {
    Partner.findOneAndUpdate(
      {
        _id: partnerId,
      },
      {
        $set: {
          name: name,
          contactNum: contactNum,
          location: location,
          status: status,
          address: address,
        },
      },
      function (err, partner) {
        if (err) {
          res.json({
            success: false,
            msg: 'Failed to edit partner.',
          });
        } else {
          Partner.findOne(
            {
              _id: partnerId,
            },
            function (err, updatedData) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to edit partner.',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'Partner edited successfully.',
                  data: updatedData,
                });
              }
            }
          );
        }
      }
    );
  }
});

// get all partner

apiRoutes.post('/getAllPartner', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    Partner.find({}, function (err, partners) {
      if (err) {
        res.json({
          success: false,
          msg: 'No partners found.',
        });
      } else {
        res.json({
          success: true,
          msg: 'Requse sent successfully.',
          data: partners,
        });
      }
    });
  }
});

// delete partner api

apiRoutes.post('/deletePartner', function (req, res) {
  var partnerId = req.body.partnerId;
  // var userId=req.body.userId;
  if (partnerId != undefined) {
    Partner.findByIdAndRemove(
      {
        _id: partnerId,
      },
      function (err, numberAffected, rawResponse) {
        if (err) {
          res.json({
            success: false,
            msg: 'No Partner Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Partner deleted Successfully',
          });
        }
      }
    );
  }
});

// get all load api

apiRoutes.post('/getAllLoad', function (req, res) {
  var orderId = req.body.orderId;
  // if(orderId!=undefined){
  OrderInventory.find({
    orderId: orderId,
  })
    .populate('productPartId')
    .populate('warehouse')
    .populate('godown')
    .populate('location')
    .exec((err, order) => {
      if (err) {
        res.json({
          success: false,
          msg: 'No Order Found.',
        });
      } else {
        // var allLoad=order.allLoad;
        res.json({
          success: true,
          msg: 'Request Sent Successfully',
          data: order,
        });
      }
    });
  // }else {
  //     res.json({success: false,msg:'Authentication failed'});
  // }
});

// product loaded confirmation api

apiRoutes.post('/confirmProductLoaded', function (req, res) {
  var productPartId = req.body.productPartId;
  var userId = req.body.userId;
  if (userId != undefined) {
    ProductPart.findOne(
      {
        _id: productPartId,
      },
      function (err, productPart) {
        if (err) {
          res.json({
            success: false,
            msg: 'No Order Found.',
          });
        } else {
          ProductPart.findOneAndUpdate(
            {
              _id: productPartId,
            },
            {
              $set: {
                partName: productPart.partName,
                code: productPart.code,
                status: productPart.status,
                length: productPart.length,
                width: productPart.width,
                height: productPart.height,
                weight: productPart.weight,
                isLoaded: true,
              },
            },
            function (err, part) {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Failed to update.',
                });
              } else {
                ProductPart.findOne(
                  {
                    _id: productPartId,
                  },
                  function (err, updatedData) {
                    if (err) {
                      res.json({
                        success: false,
                        msg: 'No Order Found.',
                      });
                    } else {
                      res.json({
                        success: true,
                        msg: 'Part loaded successfully',
                        data: updatedData,
                      });
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// remaining load api

apiRoutes.post('/getRemainingLoad', function (req, res) {
  var orderId = req.body.orderId;
  var remainingLoad = [];
  OrderInventory.find({
    orderId: orderId,
  })
    .populate('productPartId')
    .populate('warehouse')
    .populate('godown')
    .populate('location')
    .exec((err, order) => {
      if (err) {
        res.json({
          success: false,
          msg: 'No Order Found.',
        });
      } else {
        order.filter((element, index) => {
          if (!element.isLoaded) {
            remainingLoad.push(element);
          }
          // var productParts=[];
          // await order.filter(elem =>{
          //     return !elem.productPartId.isLoaded
          // })
          // await productParts.push(element.productPartId);
          // productParts.forEach(async elem => {
          //     if(elem.isLoaded==false){
          //         remainingLoad.push(elem);
          //     }
          // });
          if (index + 1 == order.length) {
            res.json({
              success: true,
              msg: 'Request Sent Successfully',
              data: remainingLoad,
            });
          }
        });
      }
    });
});

// get current order api

apiRoutes.post('/getCurrentOrder', function (req, res) {
  var userId = req.body.userId;

  var currentOrder = [];
  if (userId != undefined) {
    // Order.find({}).populate('warehouse').populate('partner').populate('tax')
    // .populate({path:'allLoad.id',populate:{path:'productParts',model:'Parts'}}).exec((err,orders) => {
    Order.find({}, function (err, orders) {
      if (err) {
        res.json({
          success: false,
          msg: 'No Order Found.',
        });
      } else {
        // console.log(order);
        var allOrder = [];
        allOrder.push(orders);
        // console.log(allOrder);
        orders.forEach((element) => {
          if (element.isActive == true) {
            currentOrder.push(element);
          }
          // console.log(currentOrder);
        });
        res.json({
          success: true,
          msg: 'Request Sent Successfully',
          data: currentOrder,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// manage order inventory

apiRoutes.post('/manageOrderInventory', function (req, res) {
  var orderId = req.body.orderId;
  var warehouseId = req.body.warehouseId;
  var godownId = req.body.godownId;
  var locationId = req.body.locationId;
  var partUnitCode = req.body.partUnitCode;
  Order.findOne(
    {
      _id: orderId,
    },
    function (err, order) {
      if (err) {
        res.json({
          success: false,
          msg: 'No order found',
        });
      } else {
        if (order.orderType == 'purchase') {
          partUnitCode.forEach((element, index) => {
            OrderInventory.findOneAndUpdate(
              {
                partUnitCode: element,
              },
              {
                $set: {
                  status: 'Managed',
                  warehouse: warehouseId,
                  godown: godownId,
                  location: locationId,
                  isLoaded: true,
                },
              },
              function (err, inventory) {
                if (err) {
                  console.log(err);
                  res.json({
                    success: false,
                    msg: 'Failed to update',
                  });
                } else {
                  if (index + 1 == partUnitCode.length) {
                    res.json({
                      success: true,
                      msg: 'Updated Successfully',
                    });
                  }
                }
              }
            );
          });
        } else if (order.orderType == 'sales') {
          partUnitCode.forEach((element, index) => {
            OrderInventory.findOneAndUpdate(
              {
                partUnitCode: element,
              },
              {
                $set: {
                  status: 'Managed',
                  warehouse: warehouseId,
                  godown: godownId,
                  location: null,
                  isLoaded: true,
                },
              },
              function (err, inventory) {
                if (err) {
                  console.log(err);
                  res.json({
                    success: false,
                    msg: 'Failed to update',
                  });
                } else {
                  if (index + 1 == partUnitCode.length) {
                    res.json({
                      success: true,
                      msg: 'Updated Successfully',
                    });
                  }
                }
              }
            );
          });
        }
      }
    }
  );
});

// scan location api

apiRoutes.post('/scanLocation', function (req, res) {
  var locationBarcode = req.body.locationBarcode;
  if (locationBarcode != undefined || locationBarcode != null) {
    Location.findOne({
      location_barcode: locationBarcode,
    })
      .populate('warehouse')
      .populate('godown')
      .exec((err, godowns) => {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No location Found',
          });
        } else {
          res.json({
            success: true,
            msg: 'Location Details Sent Successfully',
            data: godowns,
          });
        }
      });
  } else {
    res.json({
      success: false,
      msg: 'No location Found',
    });
  }
});

// scan product parts

// apiRoutes.post('/scanProductPart',function(req,res){
//     var partBarcode=req.body.partBarcode;
//     ProductPart.findOne({"parts_barcode":partBarcode},function(err,part){
//         if(err){
//             console.log(err);
//             res.json({success:false,msg:'No Part Found.'});
//         }else {
//             res.json({success:true,msg:'Request Sent Successfully.',data:part});
//         }
//     });
// });

// add godown api

apiRoutes.post('/addGodown', function (req, res) {
  var name = req.body.name;
  var uniqueCode = req.body.uniqueCode;
  // var warehouseId=req.body.warehouseId;
  var userId = req.body.userId;
  var godownData = {
    name: name,
    uniqueCode: uniqueCode,
    // warehouse:warehouseId
  };
  if (userId != undefined) {
    Godown.addGodown(godownData, function (err, godown) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'Failed to add godown',
        });
      } else {
        res.json({
          success: true,
          msg: 'Godown added successfully',
          data: godown,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

//edit godown api

apiRoutes.post('/editGodown', function (req, res) {
  var godownId = req.body.godownId;
  var name = req.body.name;
  var uniqueCode = req.body.uniqueCode;
  // var warehouseId=req.body.warehouseId;
  var userId = req.body.userId;
  if (userId != undefined) {
    Godown.findOneAndUpdate(
      {
        _id: godownId,
      },
      {
        $set: {
          name: name,
          uniqueCode: uniqueCode,
        },
      },
      function (err, godown) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to update godown',
          });
        } else {
          Godown.findOne(
            {
              _id: godownId,
            },
            function (err, updatedGodown) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Failed to update godown',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'Godown updated successfully',
                  data: updatedGodown,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// get all godown api

apiRoutes.post('/getAllGodown', function (req, res) {
  var userId = req.body.userId;
  // var warehouseId=req.body.warehouseId;
  if (userId != undefined) {
    Godown.find({})
      .populate('warehouse')
      .exec((err, godowns) => {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to find godown',
          });
        } else {
          res.json({
            success: true,
            msg: 'Godown details sent successfully',
            data: godowns,
          });
        }
      });
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// delete godown api

apiRoutes.post('/deleteGodown', function (req, res) {
  var godownId = req.body.godownId;
  var userId = req.body.userId;
  if (userId != undefined) {
    Godown.findByIdAndRemove(
      {
        _id: godownId,
      },
      function (err, numberAffected, rawResponse) {
        if (err) {
          res.json({
            success: false,
            msg: 'No Godown Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Godown deleted Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// add location api

apiRoutes.post('/addLocation', function (req, res) {
  var name = req.body.name;
  var uniqueCode = req.body.uniqueCode;
  // var warehouseId=req.body.warehouseId;
  var godownId = req.body.godownId;
  var userId = req.body.userId;
  var locationData = {
    name: name,
    uniqueCode: uniqueCode,
    // warehouse:warehouseId,
    godown: godownId,
  };
  if (userId != undefined) {
    Location.addLocation(locationData, function (err, location) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'Failed to add godown',
        });
      } else {
        res.json({
          success: true,
          msg: 'Godown added successfully',
          data: location,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

//edit location api

apiRoutes.post('/editLocation', function (req, res) {
  var locationId = req.body.locationId;
  var name = req.body.name;
  var uniqueCode = req.body.uniqueCode;
  // var warehouseId=req.body.warehouseId;
  var godownId = req.body.godownId;
  var userId = req.body.userId;
  if (userId != undefined) {
    Location.findOneAndUpdate(
      {
        _id: locationId,
      },
      {
        $set: {
          name: name,
          uniqueCode: uniqueCode,
          godown: godownId,
        },
      },
      function (err, location) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to update location',
          });
        } else {
          Location.findOne(
            {
              _id: locationId,
            },
            function (err, updatedLocation) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Failed to update location.',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'Location updated successfully',
                  data: updatedLocation,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// get all location api

apiRoutes.post('/getAllLocation', function (req, res) {
  var userId = req.body.userId;
  // var warehouseId=req.body.warehouseId;
  if (userId != undefined) {
    Location.find({})
      .populate('warehouse')
      .populate('godown')
      .exec((err, location) => {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to find location',
          });
        } else {
          res.json({
            success: true,
            msg: 'Location details sent successfully',
            data: location,
          });
        }
      });
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// delete location api

apiRoutes.post('/deleteLocation', function (req, res) {
  var locationId = req.body.locationId;
  var userId = req.body.userId;
  if (userId != undefined) {
    Location.findByIdAndRemove(
      {
        _id: locationId,
      },
      function (err, numberAffected, rawResponse) {
        if (err) {
          res.json({
            success: false,
            msg: 'No Location Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Location deleted Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// submit product api

apiRoutes.post('/submitOrder', function (req, res) {
  // var userId=req.body.userId;
  // var productPartArray=req.body.productPartArray;
  // var godownId=req.body.godownId;
  // var locationId=req.body.locationId;
  // var warehouseId=req.body.warehouseId;
  var orderId = req.body.orderId;
  Order.findOneAndUpdate(
    {
      _id: orderId,
    },
    {
      $set: {
        orderStatus: 'Ready For Dispatch',
      },
    },
    function (err, order) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'Failed to update.',
        });
      } else {
        if (order.orderType == 'purchase') {
          Order.findOne(
            {
              _id: orderId,
            },
            function (err, updatedOrder) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Failed to update.',
                });
              } else {
                var warehouseId = updatedOrder.warehouse;
                console.log(warehouseId);
                Warehouse.findOne(
                  {
                    _id: warehouseId,
                  },
                  function (err, warehouse) {
                    if (err) {
                      console.log(err);
                    } else {
                      // console.log(warehouse);
                      var orderCode = updatedOrder.order_barcode;
                      console.log(orderCode);
                      var contactNum = warehouse.contactNum;
                      var options = {
                        authorization:
                          'xwKGnLJ9D8bmqkQ9oL5OEj3oXXBlR5hBUSbZxVyeiEN0Vi0Bw1PRlexCcNL9',
                        message:
                          'Order ' + orderCode + ' is ready for dispatch',
                        numbers: [contactNum],
                      };
                      fast2sms.sendMessage(options).then((response) => {
                        console.log(response);
                        console.log(response.return);
                        // res.json({success: true,msg:"Otp Sent",data:user,otp:OTP});
                        res.json({
                          success: true,
                          msg: 'Order Updated Successfully.',
                          data: updatedOrder,
                        });
                      });
                    }
                  }
                );
              }
            }
          );
        } else if (order.orderType == 'sales') {
          Order.findOne(
            {
              _id: orderId,
            },
            function (err, updatedOrder) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Failed to update.',
                });
              } else {
                // var warehouseId=updatedOrder.warehouse;
                // console.log(warehouseId);
                // Warehouse.findOne({"_id":warehouseId},function(err,warehouse){
                //     if(err){
                //         console.log(err);
                //     }else {
                // console.log(warehouse);
                var orderCode = updatedOrder.order_barcode;
                console.log(orderCode);
                var contactNum = 9711723615;
                var options = {
                  authorization:
                    'xwKGnLJ9D8bmqkQ9oL5OEj3oXXBlR5hBUSbZxVyeiEN0Vi0Bw1PRlexCcNL9',
                  message: 'Order ' + orderCode + ' is ready for dispatch',
                  numbers: [contactNum],
                };
                fast2sms.sendMessage(options).then((response) => {
                  console.log(response);
                  console.log(response.return);
                  // res.json({success: true,msg:"Otp Sent",data:user,otp:OTP});
                  res.json({
                    success: true,
                    msg: 'Order Updated Successfully.',
                    data: updatedOrder,
                  });
                });
                //     }
                // });
              }
            }
          );
        }
      }
    }
  );
});

// get not approved product parts api

apiRoutes.post('/getNotApprovedProductParts', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    // NotApprovedPart.find({},function(err,notApprovedPart){
    NotApprovedPart.find({})
      .populate('warehouse')
      .populate('godown')
      .populate('location')
      .populate('order')
      .populate('notApprovedProductParts')
      .exec((err, notApprovedParts) => {
        if (err) {
          res.json({
            success: false,
            msg: 'No product parts found to approve.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Request sent successfully',
            data: notApprovedParts,
          });
        }
      });
  }
});

// get approved product part list api

apiRoutes.post('/getApprovedProductParts', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    // NotApprovedPart.find({},function(err,notApprovedPart){
    ApprovedPart.find({})
      .populate('warehouse')
      .populate('godown')
      .populate('location')
      .populate('approvedProductParts')
      .exec((err, approvedParts) => {
        if (err) {
          res.json({
            success: false,
            msg: 'No product parts found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Request sent successfully',
            data: approvedParts,
          });
        }
      });
  }
});

// get products is warehouse

apiRoutes.post('/getproductsInWarehouse', function (req, res) {
  var userId = req.body.userId;
  var warehouseId = req.body.warehouseId;
  if (userId != undefined) {
    WarehouseInventory.find({
      warehouse: warehouseId,
    })
      .populate({
        path: 'warehouse',
        populate: {
          path: 'godownDetails',
        },
      })
      .populate({
        path: 'warehouse',
        populate: {
          path: 'location',
        },
      })
      .populate('orderId')
      .populate('productPartId')
      .exec((err, data) => {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No warehouse found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Request sent Successfully.',
            data,
          });
        }
      });
  }
});

// approve api

apiRoutes.post('/approve', function (req, res) {
  var orderId = req.body.orderId;
  Order.findOneAndUpdate(
    {
      _id: orderId,
    },
    {
      $set: {
        orderStatus: 'Dispatched',
      },
    },
    function (err, order) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'Failed to update',
        });
      } else {
        console.log('section1');
        if (order.orderType == 'purchase') {
          OrderInventory.find(
            {
              orderId: orderId,
            },
            function (err, orders) {
              console.log('section2');
              console.log(orders);
              orders.forEach((element, index) => {
                console.log('section3');
                console.log(element);
                WarehouseInventory.find(
                  {
                    partUnitCode: element.partUnitCode,
                  },
                  function (err, data) {
                    if (err) {
                      console.log(err);
                      res.json({
                        success: false,
                        msg: 'Failed to update',
                      });
                    } else {
                      console.log('section4');
                      if (data.length > 0) {
                        res.json({
                          success: false,
                          msg: 'Product part is already scanned and updated',
                        });
                      } else {
                        console.log(element.warehouse);
                        console.log(
                          'This is warehouse Idddddddddddddd.................'
                        );
                        var warehouseInventoryData = {
                          orderId: orderId,
                          productPartId: element.productPartId,
                          warehouse: element.warehouse,
                          godown: element.godown,
                          location: element.location,
                          partUnitCode: element.partUnitCode,
                        };
                        WarehouseInventory.addWarehouseInventory(
                          warehouseInventoryData,
                          function (err, warehouseInventory) {
                            if (err) {
                              console.log(err);
                              res.json({
                                success: false,
                                msg: 'Failed to add warehouse Inventory',
                              });
                            } else {
                              if (index + 1 == orders.length) {
                                res.json({
                                  success: false,
                                  msg: 'Added successfully',
                                  data: warehouseInventory,
                                });
                              }
                            }
                          }
                        );
                      }
                    }
                  }
                );
              });
            }
          );
        } else if (order.orderType == 'sales') {
          WarehouseInventory.remove(
            {
              orderId: orderId,
            },
            function (err, data) {
              if (err) {
                console.log(err);
                // res.json({success: false, msg: 'Failed to add warehouse Inventory'});
              } else {
                res.json({
                  success: true,
                  msg: 'Product part removed',
                });
              }
            }
          );
        }
      }
    }
  );
});

// reject api

apiRoutes.post('/reject', function (req, res) {
  var orderId = req.body.orderId;
  Order.findOneAndUpdate(
    {
      _id: orderId,
    },
    {
      $set: {
        orderStatus: 'Confirmed',
      },
    },
    function (err, order) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'Failed to update',
        });
      } else {
        OrderInventory.update(
          {
            orderId: orderId,
          },
          {
            $set: {
              status: 'New',
            },
          },
          function (err, orders) {
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Failed to update',
              });
            } else {
              res.json({
                success: true,
                msg: 'Updated Successfully',
              });
            }
          }
        );
      }
    }
  );
});

// get approved order list

apiRoutes.post('/getAllApproveOrderList', function (req, res) {
  var approveOrderList = [];
  Order.find({}, function (err, orders) {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        msg: 'No Orders Found.',
      });
    } else {
      console.log('section1');
      console.log(orders);
      orders.forEach((element, index) => {
        if (
          element.orderStatus == 'Dispatched' ||
          element.orderStatus == 'Delivered'
        ) {
          approveOrderList.push(element);
          console.log('section2');
          console.log(approveOrderList);
          if (index + 1 == orders.length) {
            console.log('section3');
            console.log(approveOrderList);
            res.json({
              success: true,
              msg: 'Request sent successfully.',
              data: approveOrderList,
            });
          } else {
            if (approveOrderList.length == 0)
              res.json({
                success: false,
                msg: 'No orders found.',
              });
          }
        }
      });
    }
  });
});

// submit order details api

// apiRoutes.post('/submitOrderDetails',function(req,res){
//     var orderId=req.body.orderId;
//     var orderStatus=req.body.orderStatus;
//     var deviceId=req.body.deviceId;
//         Order.findOne({"_id":orderId},function(err,order){
//             if(err){
//                 console.log(err);
//                 res.json({success: false, msg: 'No order found.'});
//             }else {
//                 Order.findOneAndUpdate({"_id":orderId},{$set:{"orderNumber":order.orderNumber,"truckNo":order.truckNo,
//                 "totalAmount":order.totalAmount,"partner":order.partnerId,"allLoad":order.allLoad,"orderType":order.orderType,
//                 "deliveryAddress":order.deliveryAddress,"orderDate":order.orderDate,"manufacturingPlant":order.plantId,
//                 "orderLocation":order.orderPlace,"tax":order.taxId,"orderStatus":orderStatus}},function(err,order1){
//                     if(err){
//                         console.log(err);
//                         res.json({success: false, msg: 'Failed to update order'});
//                     }else {
//                         axios.post('https://fcm.googleapis.com/fcm/send', {
//                             "notification":{
//                             "title":'New Message',
//                             "body":"Order" + orderStatus+"successfully",
//                             "sound":"default",
//                             "click_action":"FCM_PLUGIN_ACTIVITY",
//                             "icon":"assets/images/login/logo.png"
//                             },
//                             "to":deviceId,
//                             "priority":"high",
//                             "restricted_package_name":""
//                             },
//                             {
//                             headers: {
//                             'Content-Type':'application/json',
//                             'Authorization': 'key=AAAAFoaF5bI:APA91bGoSPmuCUNNbLdxCuodg_V2y7ApuN06LksZ0Ue5Y-on5J7okzYqo9nyzPS8OI5oCfbxzE6HJSysIjIu2aOCJT0NySpBkMGiO9Mdzs4uYk9H0drUXhOctFVGrwglJoRMDUHemp4y'
//                             }
//                             })
//                             .then(response => {
//                             console.log(`statusCode: ${response.statusCode}`)
//                             console.log("goeshere");
//                             res.json({success: true,msg:'Notification sent Successfully to warehouse manager.'});

//                             })
//                             .catch(error => {
//                             console.error(error);
//                             res.json({success: false,msg:'Unable to send Notification.'});
//                             })
//                     }
//                 });
//             }
//         });
// });

apiRoutes.post('/submitOrderDetails', function (req, res) {
  var contactNum = req.body.contactNum;
  var orderId = req.body.orderId;
  var orderStatus = req.body.orderStatus;
  // var OTP=makeid(6);
  // if(contactNum!=undefined){
  Order.findOne(
    {
      _id: orderId,
    },
    function (err, order) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'No order found.',
        });
      } else {
        Order.findOneAndUpdate(
          {
            _id: orderId,
          },
          {
            $set: {
              orderNumber: order.orderNumber,
              truckNo: order.truckNo,
              partner: order.partnerId,
              allLoad: order.allLoad,
              orderType: order.orderType,
              deliveryAddress: order.deliveryAddress,
              orderDate: order.orderDate,
              manufacturingPlant: order.plantId,
              orderLocation: order.orderPlace,
              tax: order.taxId,
              orderStatus: orderStatus,
            },
          },
          function (err, order1) {
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Failed to update order',
              });
            } else {
              NewUser.findOne(
                {
                  contactNum: contactNum,
                },
                function (err, user) {
                  if (err) {
                    res.json({
                      success: false,
                      msg: 'No User Found.',
                    });
                  } else {
                    var options = {
                      authorization:
                        'xwKGnLJ9D8bmqkQ9oL5OEj3oXXBlR5hBUSbZxVyeiEN0Vi0Bw1PRlexCcNL9',
                      message: 'Order' + orderStatus + ' successfully',
                      numbers: [contactNum],
                    };
                    fast2sms.sendMessage(options).then((response) => {
                      console.log(response);
                      console.log(response.return);
                      res.json({
                        success: true,
                        msg: 'Order Status Sent',
                      });
                    });
                  }
                }
              );
            }
          }
        );
      }
    }
  );
  // }else {
  //     res.json({success: false,msg:'Authentication failed'});
  // }
});

// get past order api

apiRoutes.post('/getPastOrder', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    Order.find(
      {
        orderStatus: 'Delivered',
      },
      function (err, orders) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No order found',
          });
        } else {
          if (orders.length != 0) {
            res.json({
              success: true,
              msg: 'Past orders sent successfully',
              data: orders,
            });
          } else {
            res.json({
              success: true,
              msg: 'No Past orders',
            });
          }
        }
      }
    );
  }
});

// scan order api

apiRoutes.post('/scanOrder', function (req, res) {
  var userId = req.body.userId;
  var orderBarcode = req.body.orderBarcode;
  if (userId != undefined) {
    Order.findOne({
      order_barcode: orderBarcode,
    })
      .populate('warehouse')
      .populate('partner')
      .populate('tax')
      .exec((err, order) => {
        // Order.findOne({"order_barcode":orderBarcode},function(err,order){
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No order found',
          });
        } else {
          res.json({
            success: true,
            msg: 'Order details sent successfully',
            data: order,
          });
        }
      });
  }
});

// get all pending order api

apiRoutes.post('/getPendingOrder', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    Order.find(
      {
        orderStatus: 'Ready For Dispatch',
      },
      function (err, order) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No order found',
          });
        } else {
          res.json({
            success: true,
            msg: 'Order details sent successfully',
            data: order,
          });
        }
      }
    );
  }
});

// add order api

apiRoutes.post('/addOrder', function (req, res) {
  var userId = req.body.userId;
  var orderNumber = req.body.orderNumber;
  var orderPlace = req.body.orderLocation;
  var partnerId = req.body.partnerId;
  var warehouseId = req.body.warehouseId;
  var taxId = req.body.taxId;
  var allLoad = req.body.allLoad;
  var orderType = req.body.orderType;
  var deliveryAddress = req.body.deliveryAddress;
  var truckNo = req.body.truckNo;
  var orderStatus = req.body.orderStatus;
  var orderDate = req.body.orderDate;
  var driverNumber = req.body.driverNumber;
  var orderTo = req.body.orderTo;
  var randomstring2 = require('randomstring');
  if (orderType == 'sales') {
    var orderData = {
      orderId: randomstring2.generate(8),
      orderNumber: orderNumber,
      allLoad: allLoad,
      tax: taxId,
      partner: partnerId,
      orderLocation: orderPlace,
      orderType: orderType,
      deliveryAddress: deliveryAddress,
      truckNo: truckNo,
      orderStatus: orderStatus,
      orderDate: orderDate,
      driverNumber: driverNumber,
      orderTo: orderTo,
    };
    if (userId != undefined) {
      Order.addOrder(orderData, function (err, order) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No Order Found.',
          });
        } else {
          var allParts = [];
          var orderId = order._id;
          UnitCounter.findOne(
            {
              id: 'part_unit_code',
            },
            function (err, unitCode) {
              if (err) {
                console.log(err);
              } else {
                let counter = unitCode.counter;
                allLoad.forEach((element) => {
                  var quantity = element.quantity;
                  if (element.orderItemType == 'Parts') {
                    var partId = element.id;
                    for (var i = 0; i < quantity; i++) {
                      var oData = {
                        orderId: orderId,
                        orderBarcode: order.order_barcode,
                        productPartId: partId,
                        status: 'New',
                        partUnitCode:
                          element.barcode + ('00000' + counter++).slice(-5),
                      };
                      OrderInventory.addOrderInventory(
                        oData,
                        function (err, inventory) {
                          if (err) {
                            console.log(err);
                          } else {
                            UnitCounter.findOneAndUpdate(
                              {
                                id: 'part_unit_code',
                              },
                              {
                                $set: {
                                  counter: counter,
                                },
                              },
                              function (err, counter) {
                                if (err) {
                                  console.log(err);
                                } else {
                                  console.log(counter);
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  } else if (element.orderItemType == 'Product') {
                    Product.findOne({
                      _id: element.id,
                    })
                      .populate('productParts')
                      .exec((err, product) => {
                        for (var i = 0; i < quantity; i++) {
                          product.productParts.forEach((element1) => {
                            var oData = {
                              orderId: orderId,
                              orderBarcode: order.order_barcode,
                              productPartId: element1._id,
                              status: 'New',
                              partUnitCode:
                                element1.parts_barcode +
                                ('00000' + counter++).slice(-5),
                            };
                            OrderInventory.addOrderInventory(
                              oData,
                              function (err, inventory) {
                                if (err) {
                                  console.log(err);
                                } else {
                                  UnitCounter.findOneAndUpdate(
                                    {
                                      id: 'part_unit_code',
                                    },
                                    {
                                      $set: {
                                        counter: counter,
                                      },
                                    },
                                    function (err, counter) {
                                      if (err) {
                                        console.log(err);
                                      } else {
                                        console.log(counter);
                                      }
                                    }
                                  );
                                }
                              }
                            );
                          });
                        }
                      });
                  }
                });
              }
            }
          );
          res.json({
            success: true,
            msg: 'order added successfully',
            data: order,
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: 'Authentication failed',
      });
    }
  } else if (orderType == 'purchase') {
    var orderData = {
      orderId: randomstring2.generate(8),
      orderNumber: orderNumber,
      allLoad: allLoad,
      tax: taxId,
      warehouse: warehouseId,
      orderLocation: orderPlace,
      orderType: orderType,
      deliveryAddress: deliveryAddress,
      truckNo: truckNo,
      orderStatus: orderStatus,
      orderDate: orderDate,
      driverNumber: driverNumber,
      orderTo: orderTo,
    };
    if (userId != undefined) {
      Order.addOrder(orderData, function (err, order) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No Order Found.',
          });
        } else {
          var orderId = order._id;
          UnitCounter.findOne(
            {
              id: 'part_unit_code',
            },
            function (err, unitCode) {
              if (err) {
                console.log(err);
              } else {
                let counter = unitCode.counter;
                allLoad.forEach((element) => {
                  var quantity = element.quantity;
                  if (element.orderItemType == 'Parts') {
                    var partId = element.id;
                    for (var i = 0; i < quantity; i++) {
                      var oData = {
                        orderId: orderId,
                        warehouse: order.warehouse,
                        orderBarcode: order.order_barcode,
                        productPartId: partId,
                        status: 'New',
                        partUnitCode:
                          element.barcode + ('00000' + counter++).slice(-5),
                      };
                      OrderInventory.addOrderInventory(
                        oData,
                        function (err, inventory) {
                          if (err) {
                            console.log(err);
                          } else {
                            UnitCounter.findOneAndUpdate(
                              {
                                id: 'part_unit_code',
                              },
                              {
                                $set: {
                                  counter: counter,
                                },
                              },
                              function (err, counter) {
                                if (err) {
                                  console.log(err);
                                } else {
                                  console.log(counter);
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  } else if (element.orderItemType == 'Product') {
                    Product.findOne({
                      _id: element.id,
                    })
                      .populate('productParts')
                      .exec((err, product) => {
                        for (var i = 0; i < quantity; i++) {
                          product.productParts.forEach((element1) => {
                            var oData = {
                              orderId: orderId,
                              warehouse: order.warehouse,
                              orderBarcode: order.order_barcode,
                              productPartId: element1._id,
                              status: 'New',
                              partUnitCode:
                                element1.parts_barcode +
                                ('00000' + counter++).slice(-5),
                            };
                            OrderInventory.addOrderInventory(
                              oData,
                              function (err, inventory) {
                                if (err) {
                                  console.log(err);
                                } else {
                                  UnitCounter.findOneAndUpdate(
                                    {
                                      id: 'part_unit_code',
                                    },
                                    {
                                      $set: {
                                        counter: counter,
                                      },
                                    },
                                    function (err, counter) {
                                      if (err) {
                                        console.log(err);
                                      } else {
                                        console.log(counter);
                                      }
                                    }
                                  );
                                }
                              }
                            );
                          });
                        }
                      });
                  }
                });
              }
            }
          );
          res.json({
            success: true,
            msg: 'order added successfully',
            data: order,
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: 'Authentication failed',
      });
    }
  }
});

//edit order api

apiRoutes.post('/editOrder', function (req, res) {
  var userId = req.body.userId;
  var orderId = req.body.orderId;
  var orderNumber = req.body.orderNumber;
  var orderPlace = req.body.orderLocation;
  var partnerId = req.body.partnerId;
  var warehouseId = req.body.warehouseId;
  var taxId = req.body.taxId;
  var allLoad = req.body.allLoad;
  var orderType = req.body.orderType;
  var deliveryAddress = req.body.deliveryAddress;
  var truckNo = req.body.truckNo;
  var orderStatus = req.body.orderStatus;
  var orderDate = req.body.orderDate;
  var driverNumber = req.body.driverNumber;
  var orderTo = req.body.orderTo;
  if (userId != undefined) {
    if (orderType == 'sales') {
      Order.findOneAndUpdate(
        {
          _id: orderId,
        },
        {
          $set: {
            orderNumber: orderNumber,
            orderLocation: orderPlace,
            truckNo: truckNo,
            orderStatus: orderStatus,
            tax: taxId,
            partner: partnerId,
            allLoad: allLoad,
            orderType: orderType,
            driverNumber: driverNumber,
            deliveryAddress: deliveryAddress,
            orderDate: orderDate,
            orderTo: orderTo,
          },
        },
        function (err, order) {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Failed to Update Order.',
            });
          } else {
            Order.findOne(
              {
                _id: orderId,
              },
              function (err, updatedOrder) {
                if (err) {
                  console.log(err);
                  res.json({
                    success: false,
                    msg: 'No Order Found.',
                  });
                } else {
                  res.json({
                    success: true,
                    msg: 'order Updated successfully',
                    data: updatedOrder,
                  });
                }
              }
            );
          }
        }
      );
    } else if (orderType == 'purchase') {
      Order.findOneAndUpdate(
        {
          _id: orderId,
        },
        {
          $set: {
            orderNumber: orderNumber,
            orderLocation: orderPlace,
            truckNo: truckNo,
            orderStatus: orderStatus,
            tax: taxId,
            allLoad: allLoad,
            orderType: orderType,
            driverNumber: driverNumber,
            deliveryAddress: deliveryAddress,
            orderDate: orderDate,
            warehouse: warehouseId,
            orderTo: orderTo,
          },
        },
        function (err, order) {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Failed to Update Order.',
            });
          } else {
            Order.findOne(
              {
                _id: orderId,
              },
              function (err, updatedOrder) {
                if (err) {
                  console.log(err);
                  res.json({
                    success: false,
                    msg: 'No Order Found.',
                  });
                } else {
                  res.json({
                    success: true,
                    msg: 'order Updated successfully',
                    data: updatedOrder,
                  });
                }
              }
            );
          }
        }
      );
    }
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// delete order api

apiRoutes.post('/deleteOrder', function (req, res) {
  var orderId = req.body.orderId;
  var userId = req.body.userId;
  if (userId != undefined) {
    Order.findByIdAndRemove(
      {
        _id: orderId,
      },
      function (err, numberAffected, rawResponse) {
        if (err) {
          res.json({
            success: false,
            msg: 'No Order Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Order deleted Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// get all order api

apiRoutes.post('/getAllOrder', function (req, res) {
  var userId = req.body.userId;
  var partnerId = req.body.partnerId;
  var plantId = req.body.plantId;
  if (userId != undefined) {
    Order.find({})
      .populate('warehouse')
      .populate('partner')
      .populate('tax')
      .populate('allLoad.id')
      .exec((err, orders) => {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No Order Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Request Sent Successfully',
            data: orders,
          });
        }
      });
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// add tax api

apiRoutes.post('/addTax', function (req, res) {
  var name = req.body.name;
  var gst = req.body.gst;
  var userId = req.body.userId;
  var taxData = {
    name: name,
    gst: gst,
  };
  if (userId != undefined) {
    Tax.addTax(taxData, function (err, tax) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'Failed to add tax.',
        });
      } else {
        res.json({
          success: true,
          msg: 'Tax added successfully',
          data: tax,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// edit tax api

apiRoutes.post('/editTax', function (req, res) {
  var taxId = req.body.taxId;
  var name = req.body.name;
  var gst = req.body.gst;
  var userId = req.body.userId;
  if (userId != undefined) {
    Tax.findOneAndUpdate(
      {
        _id: taxId,
      },
      {
        $set: {
          name: name,
          gst: gst,
        },
      },
      function (err, tax) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Failed to edit tax.',
          });
        } else {
          Tax.findOne(
            {
              _id: taxId,
            },
            function (err, updatedData) {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Db error',
                });
              } else {
                res.json({
                  success: true,
                  msg: 'Tax Updated successfully',
                  data: updatedData,
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// delete tax api

apiRoutes.post('/deleteTax', function (req, res) {
  var taxId = req.body.taxId;
  var userId = req.body.userId;
  if (userId != undefined) {
    Tax.findByIdAndRemove(
      {
        _id: taxId,
      },
      function (err, numberAffected, rawResponse) {
        if (err) {
          res.json({
            success: false,
            msg: 'No Tax Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Tax deleted Successfully',
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

//get all tax api

apiRoutes.post('/getAllTax', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    Tax.find({}, function (err, tax) {
      if (err) {
        res.json({
          success: false,
          msg: 'No Tax Found.',
        });
      } else {
        res.json({
          success: true,
          msg: 'Request Sent Successfully',
          data: tax,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// add product api

apiRoutes.post('/addProductApi', function (req, res) {
  // var userId=req.body.userId;
  var productName = req.body.productName;
  var productCost = req.body.productCost;
  var productCode = req.body.productCode;
  // var productQuantity=req.body.productQuantity;
  var categories = req.body.categories;
  var status = req.body.status;
  var weight = req.body.weight;
  var series = req.body.series;
  var description = req.body.description;
  var productParts = req.body.productParts;
  var productImage = req.body.productImage;
  var productData = {
    productName: productName,
    productCost: productCost,
    productCode: productCode,
    // productQuantity:productQuantity,
    categories: categories,
    status: status,
    weight: weight,
    series: series,
    description: description,
    productParts: productParts,
    productImage: productImage,
  };
  // if(userId!=undefined){
  Product.addProduct(productData, function (err, product) {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        msg: 'Failed to add product.',
      });
    } else {
      // var productId=product._id;
      // var productCode="8900001"+productId.substring(0,6);
      // console.log(productCode);
      Product.findOne({});
      res.json({
        success: true,
        msg: 'product added successfully',
        data: product,
      });
    }
  });
  // }else {
  //     res.json({success: false,msg:'Authentication failed'});
  // }
});

// edit product api

apiRoutes.post('/editProductApi', function (req, res) {
  var productName = req.body.productName;
  var productCost = req.body.productCost;
  var productCode = req.body.productCode;
  var productQuantity = req.body.productQuantity;
  var categories = req.body.categories;
  var status = req.body.status;
  var weight = req.body.weight;
  var series = req.body.series;
  var description = req.body.description;
  var productParts = req.body.productParts;
  var productId = req.body.productId;
  var productImage = req.body.productImage;
  // var productSubCategory=req.body.productSubCategory
  // if(userId!=undefined){
  Product.findOneAndUpdate(
    {
      _id: productId,
    },
    {
      $set: {
        productName: productName,
        productCost: productCost,
        productCode: productCode,
        productQuantity: productQuantity,
        categories: categories,
        status: status,
        weight: weight,
        series: series,
        description: description,
        productParts: productParts,
        productImage: productImage,
      },
    },
    function (err, product) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'Failed to update Product.',
        });
      } else {
        Product.findOne(
          {
            _id: productId,
          },
          function (err, updatedData) {
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'No Product Found.',
              });
            } else {
              res.json({
                success: true,
                msg: 'Product Updated successfully.',
                data: updatedData,
              });
            }
          }
        );
      }
    }
  );
  // }else {
  //     res.json({success: false,msg:'Authentication failed'});
  // }
});

// get all products api

apiRoutes.post('/getAllProductsApi', function (req, res) {
  var userId = req.body.userId;
  if (userId != undefined) {
    Product.find({})
      .populate('categories')
      .populate('series')
      .populate('productParts')
      .exec((err, product) => {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No Product found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Product found.',
            data: product,
          });
        }
      });
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// get products by warehouse id api

apiRoutes.post('/getAllProductsInWarehouse', function (req, res) {
  var userId = req.body.userId;
  var warehouseId = req.body.warehouseId;
  if (userId != undefined) {
    Product.find(
      {
        warehouseId: warehouseId,
      },
      function (err, product) {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'No Product found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'Product found.',
            data: product,
          });
        }
      }
    );
  } else {
    res.json({
      success: false,
      msg: 'Authentication Failed.',
    });
  }
});

// delete product api

apiRoutes.post('/deleteProductApi', function (req, res) {
  var productId = req.body.productId;
  var userId = req.body.userId;
  // if(userId!=undefined){
  Product.findByIdAndRemove(
    {
      _id: productId,
    },
    function (err, numberAffected, rawResponse) {
      if (err) {
        res.json({
          success: false,
          msg: 'No Product Found.',
        });
      } else {
        res.json({
          success: true,
          msg: 'Product deleted Successfully',
        });
      }
    }
  );
  // }
});

// get dashboard details api

apiRoutes.post('/getDashboardDetails', function (req, res) {
  var completedOrder = [];
  var lastWeekOrder = [];
  var salesArray = [];
  var purchaseArray = [];
  var x = 0;
  Order.find(
    {
      createdDate: {
        $lt: new Date(),
        $gte: new Date(new Date().setDate(new Date().getDate() - 7)),
      },
    },
    function (err, orders) {
      if (err) {
        console.log(err);
      } else {
        lastWeekOrder = orders;
        var totalOrder = orders.length;
        lastWeekOrder.forEach((element) => {
          if (element.isActive == false) {
            completedOrder.push(element);
          }
          if (element.orderType == 'sales') {
            salesArray.push(element);
          }
          if (element.orderType == 'purchase') {
            purchaseArray.push(element);
          }
          completedOrder.forEach((element) => {
            var y = x + element.totalAmount;
            x = y;
          });
        });
        NewUser.find({}, function (err, users) {
          if (err) {
            console.log(err);
          } else {
            var userCount = users.length;
          }
          res.json({
            success: true,
            msg: 'success',
            oneWeekOrder: lastWeekOrder,
            completedOrder: completedOrder,
            userCount: userCount,
            totalOrder: totalOrder,
            oneWeekRevenue: x,
            oneWeekSales: salesArray,
            oneWeekPurchase: purchaseArray,
          });
        });
      }
    }
  );
});

// add manufacturing plant api

apiRoutes.post('/addManufacturingPlant', function (req, res) {
  // var userId=req.body.userId;
  var name = req.body.name;
  var contactNum = req.body.contactNum;
  var location = req.body.location;
  var address = req.body.address;
  var status = req.body.status;
  var plantData = {
    name: name,
    contactNum: contactNum,
    location: location,
    address: address,
    status: status,
  };
  if (contactNum != undefined) {
    ManufacturingPlant.addManufacturingPlant(plantData, function (err, plant) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'Failed to add manufacturing plant.',
        });
      } else {
        res.json({
          success: true,
          msg: 'Manufacturing Plant added successfully',
          data: plant,
        });
      }
    });
  } else {
    res.json({
      success: false,
      msg: 'Authentication failed',
    });
  }
});

// edit manufacturing parts api

apiRoutes.post('/editManufacturingPlant', function (req, res) {
  var plantId = req.body.plantId;
  var name = req.body.name;
  var contactNum = req.body.contactNum;
  var location = req.body.location;
  var address = req.body.address;
  var status = req.body.status;
  ManufacturingPlant.findOneAndUpdate(
    {
      _id: plantId,
    },
    {
      $set: {
        name: name,
        contactNum: contactNum,
        location: location,
        address: address,
        status: status,
      },
    },
    function (err, plant) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'Failed to update Manufacturing Plant.',
        });
      } else {
        ManufacturingPlant.findOne(
          {
            _id: plantId,
          },
          function (err, updatedData) {
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'No Manufacturing Plant Found.',
              });
            } else {
              res.json({
                success: true,
                msg: 'Manufacturing Plant Updated successfully.',
                data: updatedData,
              });
            }
          }
        );
      }
    }
  );
});

// get manufacturing plant api

apiRoutes.post('/getManufacturingPlant', function (req, res) {
  var plantId = req.body.plantId;
  // var userId=req.body.userId;
  // if(userId!=undefined){
  ManufacturingPlant.findOne(
    {
      _id: plantId,
    },
    function (err, plant) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'No Manufacturing Plant Found.',
        });
      } else {
        res.json({
          success: true,
          msg: 'Manufacturing Plant sent successfully',
          data: plant,
        });
      }
    }
  );
  // }else {
  //     res.json({success: false,msg:'Authentication failed'});
  // }
});

//get all manufacturing plant api

apiRoutes.post('/getAllManufacturingPlant', function (req, res) {
  // var userId=req.body.userId;
  // if(userId!=undefined){
  ManufacturingPlant.find({}, function (err, plants) {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        msg: 'No Manufacturing plant found.',
      });
    } else {
      res.json({
        success: true,
        msg: 'Manufacturing Plant sent successfully',
        data: plants,
      });
    }
  });
  // }else {
  //     res.json({success: false,msg:'Authentication failed'});
  // }
});

// delete maufacturing plant api

apiRoutes.post('/deleteManufacturingPlant', function (req, res) {
  var plantId = req.body.plantId;
  // var userId=req.body.userId;
  // if(userId!=undefined){
  ManufacturingPlant.findByIdAndRemove(
    {
      _id: plantId,
    },
    function (err, numberAffected, rawResponse) {
      if (err) {
        res.json({
          success: false,
          msg: 'No Manufacturing Plant Found.',
        });
      } else {
        res.json({
          success: true,
          msg: 'Manufacturing Plant deleted Successfully.',
        });
      }
    }
  );
  // }
});

// add warehouse api

apiRoutes.post('/addWarehouse', function (req, res) {
  // var userId=req.body.userId;
  var name = req.body.name;
  var contactNum = req.body.contactNum;
  var warehouseLocation = req.body.warehouseLocation;
  var address = req.body.address;
  // var godownDetails=req.body.godownDetails;
  // var location=req.body.location;
  // var godownName=req.body.godownName;
  // var godownLocation=req.body.godownLocation;
  // var godownUniqueCode=req.body.godownUniqueCode;
  var warehouseData = {
    name: name,
    contactNum: contactNum,
    warehouseLocation: warehouseLocation,
    address: address,
    // godownDetails:godownDetails,
    // location:location
    // godownName:godownName,
    // godownLocation:godownLocation,
    // godownUniqueCode:godownUniqueCode
  };
  // if(userId!=undefined){
  Warehouse.addWarehouse(warehouseData, function (err, warehouse) {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        msg: 'Failed to add warehouse.',
      });
    } else {
      res.json({
        success: true,
        msg: 'Warehouse added successfully',
        data: warehouse,
      });
    }
  });
  // }else {
  //     res.json({success: false,msg:'Authentication failed'});
  // }
});

// edit warehouse api

apiRoutes.post('/editWarehouse', function (req, res) {
  var warehouseId = req.body.warehouseId;
  var name = req.body.name;
  var contactNum = req.body.contactNum;
  var warehouseLocation = req.body.warehouseLocation;
  var address = req.body.address;
  var godownDetails = req.body.godownDetails;
  var location = req.body.location;
  var godownId = req.body.godownId;
  // var locationId=req.body.locationId;
  var locationId = [];
  var godownIds = [];

  Warehouse.findOne(
    {
      _id: warehouseId,
    },
    function (err, warehouse) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'No Warehouse Found.',
        });
      } else {
        console.log('heyyyyyyyyyyyyyyyyyyyy');
        Godown.remove(
          {
            warehouse: warehouseId,
          },
          async function (err, numberAffected, rawResponse) {
            if (err) {
              console.log(err);
              res.json({
                success: false,
                msg: 'Failed to delete godown.',
              });
            } else {
              Location.remove(
                {
                  warehouse: warehouseId,
                },
                async function (err, numberAffected, rawResponse) {
                  if (err) {
                    console.log(err);
                    res.json({
                      success: false,
                      msg: 'Failed to delete godown.',
                    });
                  } else {
                    console.log('heyyyyyyyyyyyyyyyyyyyy');
                    await godownDetails.forEach(
                      async (element, godownIndex) => {
                        var gDetails = {
                          name: element.name,
                          uniqueCode: element.uniqueCode,
                          warehouse: warehouseId,
                        };
                        await Godown.addGodown(
                          gDetails,
                          async function (err, godown) {
                            if (err) {
                              console.log(err);
                              res.json({
                                success: false,
                                msg: 'Failed to add godown.',
                              });
                            } else {
                              var godownId = godown._id;
                              godownIds.push(godownId);
                              await location.forEach(
                                async (elem, locationIndex) => {
                                  if (elem.godown == gDetails.name) {
                                    var locDetails = {
                                      name: elem.name,
                                      uniqueCode: elem.uniqueCode,
                                      warehouse: warehouseId,
                                      godown: godownId,
                                    };
                                    await Location.addLocation(
                                      locDetails,
                                      function (err, loc) {
                                        if (err) {
                                          console.log(err);
                                          res.json({
                                            success: false,
                                            msg: 'Failed to add location.',
                                          });
                                        } else {
                                          locationId.push(loc._id);
                                          if (
                                            locationIndex + 1 ==
                                              location.length &&
                                            godownIndex + 1 ==
                                              godownDetails.length
                                          ) {
                                            Warehouse.findOneAndUpdate(
                                              {
                                                _id: warehouseId,
                                              },
                                              {
                                                $set: {
                                                  name: name,
                                                  contactNum: contactNum,
                                                  warehouseLocation: warehouseLocation,
                                                  address: address,
                                                  godownDetails: godownIds,
                                                  location: locationId,
                                                },
                                              },
                                              function (err, wareh) {
                                                if (err) {
                                                  console.log(err);
                                                  res.json({
                                                    success: false,
                                                    msg:
                                                      'Failed to update warehouse.',
                                                  });
                                                } else {
                                                  Warehouse.findOne(
                                                    {
                                                      _id: warehouseId,
                                                    },
                                                    function (
                                                      err,
                                                      updatedWarehouse
                                                    ) {
                                                      if (err) {
                                                        console.log(err);
                                                        res.json({
                                                          success: false,
                                                          msg:
                                                            'No Warehouse Found.',
                                                        });
                                                      } else {
                                                        // if((locationIndex+1)==location.length && (godownIndex+1)==godownDetails.length){
                                                        res.json({
                                                          success: true,
                                                          msg:
                                                            'Request sent successfully.',
                                                          data: updatedWarehouse,
                                                        });
                                                        // }
                                                      }
                                                    }
                                                  );
                                                }
                                              }
                                            );
                                          }
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    );
                    console.log(godownIds);
                    console.log(locationId);
                  }
                }
              );
            }
          }
        );
      }
    }
  );
});

// get warehouse api

apiRoutes.post('/getWarehouse', function (req, res) {
  var warehouseId = req.body.warehouseId;
  // var userId=req.body.userId;
  // if(userId!=undefined){
  Warehouse.find({
    _id: warehouseId,
  })
    .populate('godownDetails')
    .populate('location')
    .exec((err, warehouse) => {
      // Warehouse.findOne({"_id":warehouseId},function(err, warehouse) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'No Warehouse Found.',
        });
      } else {
        res.json({
          success: true,
          msg: 'Warehouse sent successfully',
          data: warehouse,
        });
      }
    });
  // }else {
  //     res.json({success: false,msg:'Authentication failed'});
  // }
});

//get all warehouse api

apiRoutes.post('/getAllWarehouse', function (req, res) {
  // var userId=req.body.userId;
  // if(userId!=undefined){
  Warehouse.find({})
    .populate('godownDetails')
    .populate('location')
    .exec((err, warehouse) => {
      // Warehouse.find({},function(err, warehouse) {
      if (err) {
        console.log(err);
        res.json({
          success: false,
          msg: 'No Warehouse Found.',
        });
      } else {
        res.json({
          success: true,
          msg: 'Warehouse sent successfully',
          data: warehouse,
        });
      }
    });
  // }else {
  //     res.json({success: false,msg:'Authentication failed'});
  // }
});

// delete warehouse api

apiRoutes.post('/deleteWarehouse', function (req, res) {
  var warehouseId = req.body.warehouseId;
  // var userId=req.body.userId;
  // if(userId!=undefined){
  Warehouse.findByIdAndRemove(
    {
      _id: warehouseId,
    },
    function (err, numberAffected, rawResponse) {
      if (err) {
        res.json({
          success: false,
          msg: 'No Warehouse Found.',
        });
      } else {
        res.json({
          success: true,
          msg: 'Warehouse deleted Successfully.',
        });
      }
    }
  );
  // }
});

// get user api

apiRoutes.post('/getUser', function (req, res) {
  var contactNum = req.body.contactNum;
  if (contactNum != undefined) {
    NewUser.findOne(
      {
        contactNum: contactNum,
      },
      function (err, user) {
        if (err) {
          res.json({
            success: false,
            msg: 'No User Found.',
          });
        } else {
          res.json({
            success: true,
            msg: 'User sent Successfully.',
            data: user,
          });
        }
      }
    );
  }
});

// increase barcode number api

apiRoutes.post('/increaseBarcodeNumber', function (req, res) {
  var partId = req.body.partId;
  var barcodeNumber = req.body.barcodeNumber;
  ProductPart.findOne(
    {
      _id: partId,
    },
    function (err, part) {
      if (err) {
        res.json({
          success: false,
          msg: 'No Product Part Found.',
        });
      } else {
        ProductPart.findOneAndUpdate(
          {
            _id: partId,
          },
          {
            $set: {
              partName: part.partName,
              code: part.code,
              status: part.status,
              isLoaded: part.isLoaded,
              length: part.length,
              width: part.width,
              height: part.height,
              weight: part.weight,
              barcodeNumber: barcodeNumber,
            },
          },
          function (err, part1) {
            if (err) {
              res.json({
                success: false,
                msg: 'Failed to update.',
              });
            } else {
              ProductPart.findOne(
                {
                  _id: partId,
                },
                function (err, updatedPart) {
                  if (err) {
                    console.log(err);
                  } else {
                    res.json({
                      success: true,
                      msg: 'Barcode Number Updated successfully.',
                      data: updatedPart,
                    });
                  }
                }
              );
            }
          }
        );
      }
    }
  );
});

apiRoutes.get('/getAllMedia', (req, res) => {
  Media.find({}, (err, doc) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Error: Getting media files',
      });
    } else {
      res.json({
        success: true,
        data: doc,
      });
    }
  });
});
apiRoutes.post('/addMedia', (req, res) => {
  var imageUrl = req.body.imageUrl;

  Media.create(
    {
      imageUrl,
    },
    (err, doc) => {
      if (err) {
        res.json({
          success: false,
          msg: 'Error: Add Error',
        });
      } else {
        res.json({
          success: true,
          data: doc,
        });
      }
    }
  );
});

apiRoutes.delete('/deleteMedia', (req, res) => {
  var mediaId = req.body.mediaId;

  Media.remove(
    {
      _id: mediaId,
    },
    (err, doc) => {
      if (err) {
        res.json({
          success: false,
          msg: 'Error: Delete Error',
        });
      } else {
        res.json({
          success: true,
          msg: 'Removed Succesfully',
        });
      }
    }
  );
});

apiRoutes.get('/testnotification/:userId', (req, res) => {
  common.sendNotification(
    req.params.userId,
    'New message',
    'vaibhav fuke sent you a message'
  );
  res.send('asdf');
});

apiRoutes.post('/updateUserVerified', (req, res) => {
  var userId = req.body.userId;
  var isVerified = req.body.isVerified;

  User.update(
    {
      _id: userId,
    },
    {
      $set: {
        isVerified,
      },
    },
    function (err, user) {
      if (err) {
        res.status(400).json({
          success: false,
          msg: 'Something went wrong! Try Again',
        });
      } else {
        if (isVerified) {
          User.findOne(
            {
              _id: userId,
            },
            function (err, userdet) {
              // console.log(userdet)
              // console.log("User")
              //     var accountSid = 'ACa8048e8e69fe674e0fb2ed45878b1908';
              // var authToken = 'f5047c845be3ae443d0c4c8b8b2ded62';
              // var client = new twilio(accountSid, authToken);
              // console.log('+91'+userdet.contactNum.toString())
              // client.messages
              // .create({
              //     body: 'Congratulations! Your profile has been successfully approved. Please login and enjoy the world of HobbyIt',
              //     from: '+12184966046',
              //     to: '+91'+userdet.contactNum.toString()
              // }).then(message => console.log("message id"+ message));
              let transporter = nodemailer.createTransport({
                host: 'smtp.sendgrid.net',
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                  user: 'apikey',
                  pass:
                    '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',
                },
              });
              var mailOptions = {
                from: 'promoter@hobbyit.co',
                to: userdet.emailId,
                subject: 'Your account has been verified',
                text: ' Please login and enjoy the world of HobbyIt',
              };

              transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                  console.log(error);
                } else {
                  console.log('Email sent: ' + info.response);
                }
              });

              res.json({
                success: true,
                msg: 'Updated',
              });
            }
          );
        } else {
          res.json({
            success: true,
            msg: 'Updated',
          });
        }
      }
    }
  );
});

apiRoutes.post('/modifyCredit', (req, res) => {
  let {
    creditId,
    credit1,
    discount1,
    price1,
    credit2,
    discount2,
    price2,
  } = req.body;

  Credit.update(
    {
      _id: creditId,
    },
    {
      $set: {
        credit1,
        discount1,
        price1,
        credit2,
        discount2,
        price2,
      },
    },
    function (err, user) {
      if (err) {
        res.status(400).json({
          success: false,
          msg: 'Something went wrong! Try Again',
          err,
        });
      } else {
        res.json({
          success: true,
          msg: 'Updated',
        });
      }
    }
  );
});

// Credit.create(
//   {
//     credit1: 1,
//     discount1: 1,
//     price1: 1,
//     credit2: 1,
//     discount2: 1,
//     price2: 1,
//   },
//   (err, user) => {}
// );

apiRoutes.post('/getCredit', (req, res) => {
  Credit.findOne({}, function (err, credit) {
    if (err) {
      res.status(400).json({
        success: false,
        msg: 'Something went wrong! Try Again',
      });
    } else {
      res.json({
        success: true,
        credit,
      });
    }
  });
});

apiRoutes.post('/getProfUserDetail', (req, res) => {
  const userId = req.body.userId;
  try {
    User.findOne(
      {
        _id: userId,
      },
      function (err, user) {
        if (err) {
          res.status(400).json({
            success: false,
            msg: 'Something went wrong! Try Again',
          });
        } else {
          res.json({
            success: true,
            user,
          });
        }
      }
    );
  } catch (err) {
    res.status(400).json({
      success: false,
      msg: 'Something went wrong! Try Again',
    });
  }
});

function makeid(length) {
  var result = '';
  var characters = '0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

// ALDER ERP API's end

apiRoutes.post('/login', route.login);
apiRoutes.post('/signup', route.signup);
apiRoutes.post('/loginUser', route.loginUser);
apiRoutes.post('/loginUserEmail', route.loginUserEmail);
apiRoutes.post('/signupUser', route.signupUser);
apiRoutes.post('/loginVendor', route.loginVendor);
apiRoutes.post('/forgot', route.forgot);

// import new routes
let userRoute = require('./routes/user.routes');
let notificationRoute = require('./routes/notification.routes');

apiRoutes.use('/v2/user', userRoute);
apiRoutes.use('/v2/notification', notificationRoute);
// apiRoutes.get('/v2/user', (req,res)=>{
//     res.send('asdf')
// });

// Start the server
app.listen(port);

console.log('Server is running at  http://localhost:' + port);

cron.schedule('* */1 * * *', async () => {
  const post = await PostType.findOneAndRemove(
    {
      createdDate: {
        $lt: new Date().getTime() - 24 * 60 * 60 * 1000,
      },
    },
    {
      postType: '2',
    },
    function (err, removestory) {
      if (err) {
        console.log('error');
      } else {
        //    console.log(removestory)
        console.log('below mentioned story has been removed');
      }
    }
  );
  console.log(post);
});
//    this will find and remove every story which was created before 24 hours from present time

// User.addUser(
//   {
//     contactNum: '1234567890',
//     emailId: 'admin@gmail.com',
//     password: 'Hobbyit@12345',
//     accessLevelName: 'superadmin',
//     isSuperAdmin: true,
//   },
//   function (err, userData) {
//     console.log(err);
//     console.log(userData);
//   }
// );
