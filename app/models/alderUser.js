var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcryptjs');

var AlderUserSchema = new Schema({

   createdDate : 
    { 
        type : Date, 
        default: Date.now 
    },

   userName: {
        type: String,
        
    },
    userType: {
        type: String,
        
    },
    mobileNumber:{
        type: Number,
        unique: true,   
    },
    email:{
        type: String
    },
    password: {
        type: String,
        
    },

    deviceId: {
        type: String
    },
    userLocation: {
        type: String
    },
    userAddress: {
        type: String
    },
    userLatitude: {
        type: String,
    },

    userLongitude: {
        type: String,
    },

    accessLevelId: {
         type: String
        
    },

     accessLevelName : {
        type: String
    },   


    isSuperAdmin:
    {
        type:Boolean
    },

    emailIdRegistered:{
        type:Boolean,
        default:false
    },    
    
});
 

var AlderUser= module.exports = mongoose.model('AlderUser', AlderUserSchema);

module.exports.addUser = function(alderUser, callback){   
    AlderUser.create(alderUser, callback);
};
