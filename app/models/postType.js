var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostTypeSchema = new Schema({
   
    // userType: {
    //     type: String,
    // },

    userId: {
        type: String,
    },

    createdDate : 
    { 
        type : Date,
        default: Date.now 
    },
    // image: {
    //     type: String,
        
    // },
   
    // video :{
    //    type: String,  
   
    // },

    postUrl: {
        type: String,
    },

    posttype :{
        type: String,  
    
     },

    story :{
        type: String,  
    
     },

     postTitle :{
        type: String,  
    
     },

     postDescription :{
        type: String,  
    
     },
     postCategory: {
        type: String, 
     },

    views: [],
     viewCount: {
        type: Number,
        default: 0,
    },
    likes: [],
    likeCount: {
        type: Number,
        default: 0,
    },
    userImg: {
        type: String, 
    },
    userName: {
        type: String,
    }
    
    // isBookmarked: {
    //     type: Boolean,
    //     default: false
    // },

});
 

var PostType= module.exports = mongoose.model('PostType', PostTypeSchema);

module.exports.addPostType = function(postType, callback){   
    PostType.create(postType, callback);
};

