var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var seriesSchema = new Schema({
   
    seriesName: {
        type: String,
        required: true
    },
    code: {
        type: String
    },
    status: {
        type: String
    },
});
 

var Series= module.exports = mongoose.model('Series', seriesSchema);
module.exports.addSeries = function(series, callback){
     // console.log("logging in nowwwwww"+client);
    
     Series.create(series, callback);
};