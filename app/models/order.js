var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderSchema = new Schema({
   
 
    orderId :
    {
        type: String,
        required:true,
        unique:true
    },
    orderNumber: {
        type: Number
    },
    orderLocation: {
        type: String
    },
    orderType: {
        type: String
    },
    totalAmount:{
        type:Number
    },
    userId:{
        type:String
    },
    // customerUserId: {
    //     type:String
    // },
    partner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Partner'
    },
    // warehouseId: {
    //     type: String,
    //     ref: 'Warehouse'
    // },
    manufacturingPlant: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ManufacturingPlant'
    },
    tax: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tax'
    },
    customerName:{
        type:String
    },
    orderStatus:{
        type:String
    },
    rate:{
        type:Number,
        
    },
    feedback: 
    {
        type:String
    },
    allLoad:[],
    add:{
        type:String
    },
    deliveryAddress: {
        type:String
    },
    truckNo: {
        type: String
    },
    orderDate: {
        type: String
    },
    // isReviewed:{
    //     type:Boolean,
    //     default:false
    // },
    // isCancelled:{
    //     type:Boolean,
    //     default:false
    // },
    // isRate:{
    //     type:Boolean,
    //     default:false
    // },
    // isFeedback:{
    //     type:Boolean,
    //     default:false
    // },
    isActive:{
        type:Boolean,
        default:true
    },
    
    createdDate:
    {
        type:Date,
        default:Date.now
        
    },
    godownName : {
        type: String
    },
    godownLocation : {
        type: String
    },
    godownUniqueCode: {
        type: String
    }
});
 

var Order= module.exports = mongoose.model('Order', OrderSchema);

module.exports.addOrder = function(order, callback){
      console.log("logging in nowwwwww"+order);
    
	Order.create(order, callback);
};