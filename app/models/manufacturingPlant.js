var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var manufacturingPlantSchema = new Schema({
   
    name: {
        type: String,
        required: true
    },
    contactNum: {
        type: Number,
        required: true
    },
    location: {
        type: String
    },
    address: {
        type: String
    },
    status: {
        type: String
    }
});
 

var ManufacturingPlant= module.exports = mongoose.model('ManufacturingPlant', manufacturingPlantSchema);
module.exports.addManufacturingPlant = function(manufacturingPlant, callback){
     // console.log("logging in nowwwwww"+client);
    
     ManufacturingPlant.create(manufacturingPlant, callback);
};