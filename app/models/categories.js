var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var categoriesSchema = new Schema({
   
    categoryName: {
        type: String,
        required: true
    },
    code: {
        type: String
    },
    status: {
        type: String
    },
});
 

var Categories= module.exports = mongoose.model('Categories', categoriesSchema);
module.exports.addCategories = function(categories, callback){
     // console.log("logging in nowwwwww"+client);
    
     Categories.create(categories, callback);
};