var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var warehouseSchema = new Schema({
   
    name: {
        type: String,
        required: true
    },
    contactNum: {
        type: Number,
        required: true
    },
    warehouseLocation: {
        type: String
    },
    address: {
        type: String
    },
    godownDetails: [],
    location: []
    // godownName: {
    //     type: String
    // },
    // godownLocation: {
    //     type: String
    // },
    // godownUniqueCode: {
    //     type: String
    // }
});
 

var Warehouse= module.exports = mongoose.model('Warehouse', warehouseSchema);
module.exports.addWarehouse = function(warehouse, callback){
     // console.log("logging in nowwwwww"+client);
    
     Warehouse.create(warehouse, callback);
};