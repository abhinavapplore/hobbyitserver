var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BlockedUserSchema = new Schema({
   
    // userType: {
    //     type: String,
    // },

    userId: {
        type: String,
    },

    createdDate : 
    { 
        type : Date,
        default: Date.now 
    },
    // image: {
    //     type: String,
        
    // },
   
    // video :{
    //    type: String,  
   
    // },

    hostId:{
        type: Schema.Types.ObjectId,ref:'User'
    }
    // hostId:{
    //     type: String,
    // }
    
    // isBookmarked: {
    //     type: Boolean,
    //     default: false
    // },

});
 

var BlockedUser= module.exports = mongoose.model('BlockedUser', BlockedUserSchema);

module.exports.addBlockedUser = function(blockedUser, callback){   
    BlockedUser.create(blockedUser, callback);
};