const mongoose = require('mongoose'); 

const creditSchema = new mongoose.Schema({
    credit1: {
        type: Number,
        required: true
    },
    price1: {
        type: Number,
        required: true,
    },
    discount1: {
        type: Number,
        required: true
    },
    credit2: {
        type: Number,
        required: true
    },
    price2: {
        type: Number,
        required: true,
    },
    discount2: {
        type: Number,
        required: true
    }
});

const Credit = mongoose.model('Credit', creditSchema);

module.exports = Credit;