var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var newUserSchema = new Schema({
   
    createdDate : 
    { 
        type : Date, 
        default: Date.now 
    },
    userName: {
        type: String,    
    },
    userType: {
        type: String,    
    },
    contactNum:{
        type: Number,
        unique: true,   
        required: true
    },
    email:{
        type: String
    },
    password: {
        type: String,
        required: true    
    },
    userAddress: {
        type: String
    },
});
 

var NewUser= module.exports = mongoose.model('NewUser', newUserSchema);
module.exports.addNewUser = function(newUser, callback){
     // console.log("logging in nowwwwww"+client);
    
     NewUser.create(newUser, callback);
};