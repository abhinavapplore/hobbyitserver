var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
   
    productName: 
    {
        type: String,
        required: true
    },
    productCost: {
        type: Number,
    },
    productCode : {
        type: String,
    },
    productQuantity : 
    {
        type: String,
    },
    warehouseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Warehouse'
    },
    categories : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Categories'
    },
    status: {
        type: String
    },
    weight: {
        type: String
    },
    series: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Series'
    },
    description: {
        type: String
    },
    productParts: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Parts'
        }
    ],
    productImage: [],
    
    productSubCategory :
    {
       type: String  
    
    },
    // category: [{
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'category'
    //   }],
    //   seriesId: [{
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'series'
    //   }],
    //   parts: [{
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'parts'
    //   }]
    // isLoaded: {
    //     type:Boolean,
    //     default:false
    // }
    
});
 

var Product= module.exports = mongoose.model('Product', productSchema);
module.exports.addProduct = function(product, callback){
     // console.log("logging in nowwwwww"+client);
    
	Product.create(product, callback);
};