var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NotificationSchema = new Schema({
    userId:{
        type:String,
        
    }, title:{
        type:String,
        
    },body:{
        type:String,
        
    },

    createdDate : 
    { 
        type : Date,
        default: Date.now 
    },

    amount :{
       type: Number,  
    },
    notificationType: {
        type:String,
    },
    userName:{
        type:String,
        
    },
  
    notificationImage:{
        type:String,
        
    },
   
    notificationTime: {
        type: String,
    },
    

});
 

var Notification= module.exports = mongoose.model('Notification', NotificationSchema);

module.exports.addNotification = function(notification, callback){   
    Notification.create(notification, callback);
};