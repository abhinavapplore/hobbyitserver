var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var unlockUserSchema = new Schema({

    userId: {
        type: String,
    },
    hostUserId: {
        type: String,
    },
    createdDate: {
        type: Date,
        default:Date.now()
    },
    isActive: {
        type: Boolean,
        default: true
    }
});
 

var unlockUser= module.exports = mongoose.model('unlockUser', unlockUserSchema);

module.exports.addunlockUser = function(unlockuser, callback){   
    unlockUser.create(unlockuser, callback);
};