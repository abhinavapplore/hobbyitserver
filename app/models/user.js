var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcryptjs');

 
 
// set up a mongoose model
var UserSchema = new Schema({

   createdDate : 
    { 
        type : Date, 
        default: Date.now 
    },

   fullName: {
        type: String,
        
    },
    firstName: {
        type: String,
        
    },
    lastName: {
        type: String,
        
    },
    dob: {
        type: String,
    },
    emailId:{
        type: String,   
    },

    password: {
        type: String,
        
    },
    address: {
        type: String,
        
    },
    contactNum: {
        type: Number,
        unique: true,
        required: true
    },
    gender: {
        type: String,
    },
    wallet: {
        type: Number,
        default:0
    },

    deviceId: {
        type: String
    },

    user_img: {
        type: String
    },

    about: {
        type: String
    },

    userLatitude: {
        type: String,
    },

    userLongitude: {
        type: String,
    },

    skillLevel: {
        type: Number,
        default:0
    },

    accessLevelId: {
         type: String
        
    },

     accessLevelName : {
        type: String
    },   

    userAdd:{
        type:String                 
    },

    access_token:{
        type:String
    },  

    rating: {
        type:String
    },

    experience: {
        type:String
    },

    userType: {
        type:String
    },

    isSuperAdmin:
    {
        type:Boolean
    },

    // isProfessional: {
    //     type:Boolean,
    // },

    doc3: {
        type:String,
    },
    doc4: {
        type:String,
    },
    isVerified: {
        type:Boolean,
    },
    language: [],
    userHobby: [],
    userSkills: [],

    userInterest: [],

    unlockedProfessionalUser: [],

    bookmarkedPost: [],

    documents: [],

    emailIdRegistered:{
        type:Boolean,
        default:false
    },
   
  
    
    
     
     
                            
    
    
   
    
});
 
UserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});
 
UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};
UserSchema.statics.findOrCreate = function findOrCreate(profile, cb){
    var userObj = new this();
    this.findOne({_id : profile.id},function(err,result){ 
        if(!result){
            userObj.username = profile.displayName;
            //....
            userObj.save(cb);
        }else{
            cb(err,result);
        }
    });
};

var User= module.exports = mongoose.model('User', UserSchema);

module.exports.addUser = function(user, callback){   
    User.create(user, callback);
};

 
// module.exports = mongoose.model('User', UserSchema);