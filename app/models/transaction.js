var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TransactionSchema = new Schema({
      createdDate : 
    { 
        type : Date,
        default: Date.now 
    },
    transactionType: {
        type:String,
    },
    userName:{
        type:String,
        
    },
    userId:{
        type:String,
        
    },
    transactionImage:{
        type:String,
        
    },
    title:{
        type:String,
        
    },

    transactionAmount: {
        type: Number,
    },
    transactionDate: {
        type: String,
    },
    transactionTime: {
        type: String,
    },
    
});
 

var Transaction= module.exports = mongoose.model('Transaction',TransactionSchema);
module.exports.addTransaction = function(transaction, callback){
    // console.log("logging in nowwwwww"+client);
   
    Transaction.create(transaction, callback);
};


