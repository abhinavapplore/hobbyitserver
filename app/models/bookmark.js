var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BookmarkSchema = new Schema({

    userId: {
        type: String,
    },
    postId: {
        type: String,
    },
    postUserId: {
        type: String,
    },
    createdDate : 
    { 
        type : Date,
        default: Date.now 
    },
    postUrl: {
        type: String,
    },
    posttype :{
        type: String,  
    
     },
     postTitle :{
        type: String,  
    
     },
     postDescription :{
        type: String,  
    
     },
     postCategory: {
        type: String, 
     },
     isBookmarked: {
         type: Boolean,
         default: false
     }

});
 

var Bookmark= module.exports = mongoose.model('Bookmark', BookmarkSchema);

module.exports.addBookmark = function(bookmark, callback){   
    Bookmark.create(bookmark, callback);
};