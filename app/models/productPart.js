var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var partsSchema = new Schema({
   
    partName: {
        type: String,
        required: true
    },
    code: {
        type: String,
    },
    status: {
        type: String
    },
    isLoaded: {
        type: Boolean,
        default: false
    },
    isApproved: {
        type: Boolean,
        default: false
    },
    isRejected: {
        type: Boolean,
        default: false
    },
    length: {
        type: String
    },
    width: {
        type: String
    },
    height: {
        type: String
    },
    weight: {
        type: String
    }
});
 

var Parts= module.exports = mongoose.model('Parts', partsSchema);
module.exports.addParts = function(parts, callback){
     // console.log("logging in nowwwwww"+client);
    
     Parts.create(parts, callback);
};