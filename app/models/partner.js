var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var partnerSchema = new Schema({
   
    name: {
        type: String,
        required: true
    },
    contactNum: {
        type: Number,
        required: true
    },
    location: {
        type: String
    },
    address: {
        type: String
    },
    status: {
        type: String
    },
    // createdDate: {
    //     type: String
    // }
    createdDate : 
    { 
        type : Date, 
        default: Date.now 
    },
});
 

var Partner= module.exports = mongoose.model('Partner', partnerSchema);
module.exports.addPartner = function(partner, callback){
     // console.log("logging in nowwwwww"+client);
    
     Partner.create(partner, callback);
};