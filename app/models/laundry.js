var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LaundrySchema = new Schema({
    userId: {
        type: String,
    },
    hostUserId: {
        type: String,
    },
    createdDate: {
        type: Date,
        default:Date.now()
    },
    isActive: {
        type: Boolean,
        default: true
    }
});
 
var Laundry= module.exports = mongoose.model('Laundry', LaundrySchema);

module.exports.addLaundry = function(laundry, callback){
      
	Laundry.create(laundry, callback);
};
