var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MediaSchema = new Schema({
   
    // userType: {
    //     type: String,
    // },

    userId: {
        type: String,
    },

    createdDate : 
    { 
        type : Date,
        default: Date.now 
    },
    // image: {
    //     type: String,
        
    // },
   
    // video :{
    //    type: String,  
   
    // },

    hostId:{
        type: String,
    },

    imageUrl:{
        type: String,
    }
    
    // isBookmarked: {
    //     type: Boolean,
    //     default: false
    // },

});
 

var Media= module.exports = mongoose.model('Media', MediaSchema);

module.exports.addMedia = function(Mediatype, callback){   
    Media.create(Mediatype, callback);
};