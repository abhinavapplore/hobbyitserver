app.run(['$rootScope', 'AUTH_EVENTS', 'Auth', 'AuthAttempted', function ($rootScope, AUTH_EVENTS, Auth, AuthAttempted) {
    $rootScope.$on('$stateChangeStart', function (event, next) {
        //        console.log(next);
        var authorizedRoles = next.data.authorizedRoles;
        if (next.name != 'app.login' && next.name != 'app.dashboard') {
            AuthAttempted.set(next.name);
        }
        if (!Auth.isAuthorized(authorizedRoles)) {
            event.preventDefault();

            // Attempted to access the login page whilst already logged in
            if (next.name == 'app.login' && !event.defaultPrevented && Auth.isLoggedIn()) {
                console.log("You are already logged in!");
                $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
            }

            // Usual not logged in
            if (Auth.isAuthenticated()) {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized); // user is not allowed
            } else {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated); // user is not logged in
            }
        }
    });
}]);
