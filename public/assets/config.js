app.config(['$locationProvider', '$httpProvider', function ($locationProvider, $httpProvider) {

    // Set to normal URLs (i.e. not #)
    $locationProvider
        .html5Mode(true);

    $httpProvider.interceptors.push(['$injector', function ($injector) {
        return $injector.get('AuthInterceptor');
        }]);
    }])

.config(function (NotificationProvider) {
    NotificationProvider.setOptions({
        delay: 5000,
        startTop: 20,
        startRight: 10,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        maxCount: 4
    });
});
