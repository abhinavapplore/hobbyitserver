app.constant('PLATFORM_FEATURES', {
    video_assessment: 1,
    behavioural_report: 2,
    account_reporting: 3,
    job_approval_process: 4,
    hosted_application_forms: 5,
    media_library: 6,
    email_templates: 7,
    agency_invite: 8,
    career_website_job_feed: 9,
    cv_database_searching: 10,
    business_case_generator: 11,
    candidate_verification_referencing: 12,
    interviews_events_scheduling: 13,
    account_level_branding: 14
});
