app.controller('ApplicationCtrl', function ($scope, $rootScope, USER_ROLES, Auth, AUTH_EVENTS, $state, Session, UI_EVENTS, $timeout, Notification) {

    $scope.currentUser = null;
    $scope.userRoles = USER_ROLES;
    $scope.isAuthorized = Auth.isAuthorized;
    $scope.isLoggedIn = false;
    $scope.isLoginPage = false;
    $scope.pageHasBackButton = false;
    $scope.backButtonLocation = 'app.dashboard';
    $scope.backButtonLocation = 'Dashboard';
    $scope.$state = $state;

    // Abstract function
    $scope.goBack = function () {
        $state.go($scope.backButtonLocation);
        return false;
    };

    $scope.setCurrentUser = function (user) {
        $scope.currentUser = user;
    };

    $scope.logout = function () {
        Notification({
            message: "Logged out!"
        }, 'success');
        $scope.isLoggedIn = false;
        Session.destroy();
        $scope.setCurrentUser(Session.user.name);
        $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
    };

    // Set these to false and let the controllers determine if the should be true
    $scope.$on('$stateChangeStart', function () {
        $scope.pageHasBackButton = false;
        if (Auth.isLoggedIn())
            $scope.isLoggedIn = true;
    });


    // Set back button
    $scope.$on(UI_EVENTS.showBackButton, function (event, location, text) {
        if (!location) {
            location = 'app.dashboard';
        }
        $scope.pageHasBackButton = true;
        $scope.backButtonLocation = location;
        $scope.backButtonText = text;
    });

    // Successful logins
    $scope.$on(AUTH_EVENTS.loginSuccess, function () {
        $scope.isLoggedIn = true;
        $scope.setCurrentUser(Session.user.name);
        $state.go('app.dashboard', {
            'reload': true
        });
    });

    // Successful logout
    $scope.$on(AUTH_EVENTS.logoutSuccess, function () {
        $state.go('app.login');
    });

    // Invalid session
    $scope.$on(AUTH_EVENTS.notAuthenticated, function () {
        Session.destroy();
        $scope.setCurrentUser(Session.user.name);
        $state.go('app.login');
    });
    $scope.$on(AUTH_EVENTS.sessionTimeout, function () {
        $state.go('app.login');
    });

    // Forbidden
    $scope.$on(AUTH_EVENTS.notAuthorized, function () {
        $state.go('app.dashboard');
    });
});
