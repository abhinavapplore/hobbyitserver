app.service('GetVendorDashboard', function ($http, $q, API, Session) {
    return {
        'get': function (request) {
            var token = Session.vendor.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "getVendorDashboard/" + Session.vendor.vendor,
                    'cache': false,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'GET'
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err) {
                defer.reject(err);
            });
            return defer.promise;
        }
        
        
    };
});