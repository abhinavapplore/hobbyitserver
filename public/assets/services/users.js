app.service('Users', function ($http, $q, API, Session) {
    return {
        'getAll': function (request) {
            var token = Session.user.token;
            var userId = Session.user.user;
            var clientId = Session.user.clientId;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "users/" + userId,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'GET'
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'getAllHods': function (request) {
            var token = Session.user.token;
            var userId = Session.user.user;
            var clientId = Session.user.clientId;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "users/" + request.clientId + "/" + userId,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'GET'
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'update': function (request) {
            var token = Session.user.token;
            var userId = Session.user.user;
            var clientId = Session.user.clientId;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "updateUserDetails/" + userId,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'updateAlert': function (request) {
            var token = Session.user.token;
            var userId = Session.user.user;
            var clientId = Session.user.clientId;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "updateAlertTime/" + userId,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        }
    };
});
