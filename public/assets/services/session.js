app.service('Session', function (USER_ROLES, UI_EVENTS, $localStorage) {
    if (!angular.isDefined($localStorage.user)) {
        $localStorage.user = {};
        $localStorage.user.user = null;
        $localStorage.user.name = null;
        $localStorage.user.clientId = null;
        $localStorage.user.token = null;
        $localStorage.user.currentWorkingCity = null;
        $localStorage.user.userRole = USER_ROLES.all;
    }

    if (!angular.isDefined($localStorage.citiesArray)) {
        $localStorage.citiesArray = [];
    }

    if (!angular.isDefined($localStorage.expenseHeadsArray)) {
        $localStorage.expenseHeadsArray = [];
    }

    if (!angular.isDefined($localStorage.policiesArray)) {
        $localStorage.policiesArray = [];
    }

    this.user = $localStorage.user;
    this.citiesArray = $localStorage.citiesArray;
    this.expenseHeadsArray = $localStorage.expenseHeadsArray;
    this.policiesArray = $localStorage.policiesArray;
    this.vendorsArray = $localStorage.vendorsArray;

    this.isActive = function () {
        return ($localStorage.user.user !== null);
    };

    this.create = function (token, user, userRole) {
        $localStorage.user.token = token;
        $localStorage.user.user = user._id;
        $localStorage.user.clientId = user.clientId;
        $localStorage.user.name = user.fullName;
        $localStorage.user.userRole = userRole;
        $localStorage.user.currentWorkingCity = user.currentWorkingCityId;
        

        this.user = $localStorage.user;
    };

    this.saveCities = function (cities) {
        $localStorage.citiesArray = cities;

        this.citiesArray = $localStorage.citiesArray;
    };

    this.saveVendors = function (vendors) {
        $localStorage.vendorsArray = vendors;

        this.vendorsArray = $localStorage.vendorsArray;
    };

    this.saveExpenseHeads = function (heads) {
        $localStorage.expenseHeadsArray = heads;

        this.expenseHeadsArray = $localStorage.expenseHeadsArray;
    };
      this.saveBanks = function (heads) {
        $localStorage.bankArray = heads;

        this.bankArray = $localStorage.bankArray;
    };
    this.saveTrips = function (heads) {
        $localStorage.tripsArray = heads;

        this.tripsArray = $localStorage.tripsArray;
    };

    this.savePolicies = function (heads) {
        $localStorage.policiesArray = heads;

        this.policiesArray = $localStorage.policiesArray;
    };

    this.destroy = function () {
        $localStorage.user.token = null;
        $localStorage.user.user = null;
        $localStorage.user.name = null;
        $localStorage.user.clientId = null;
        $localStorage.user.currentWorkingCity = null;
        $localStorage.user.userRole = USER_ROLES.all;
        $localStorage.citiesArray= null;
        $localStorage.expenseHeadsArray = null;
        
        $localStorage.vendorsArray = null;
        $localStorage.policiesArray = null;
        $localStorage.tripsArray = null;                
        this.user = $localStorage.user;
    };

    this.getToken = function () {
        return this.isActive() ? $localStorage.user.token : null;
    };

});
