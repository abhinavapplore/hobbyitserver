app.service('Auth', function ($http, $q, Session, USER_ROLES, API, $rootScope, AUTH_EVENTS, AuthAttempted) {
    this.login = function (credentials) {
        var defer = $q.defer(),
            httpConfig = {},
            httpProperties = {
                'url': API.baseUrl + "login/",
                'config': httpConfig,
                'method': 'POST',
                'data': credentials
            };

        $http(httpProperties).success(function (resp) {
            if (resp.success) {
                var role = resp.user.accessLevelName;
                Session.create(resp.token, resp.user, role);
                AuthAttempted.setTimes(1);
                $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                defer.resolve(resp);
            } else {
                defer.reject(resp);
            }
        }).error(function (err, status) {
            defer.reject(err, status);
        });
        return defer.promise;
    };
    this.fb_login = function (credentials) {
        var defer = $q.defer(),
            httpConfig = {},
            httpProperties = {
                'url': "http://ec2-52-66-7-57.ap-south-1.compute.amazonaws.com/login/facebook",
                'config': httpConfig,
                'method': 'POST',
                'headers': {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                'data': credentials
            };

        $http(httpProperties).success(function (resp) {
            if (resp.success) {
                var role = resp.user.accessLevelName;
                Session.create(resp.token, resp.user, role);
                AuthAttempted.setTimes(1);
                $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                defer.resolve(resp);
            } else {
                defer.reject(resp);
            }
        }).error(function (err, status) {
            defer.reject(err, status);
        });
        return defer.promise;
    };
    this.isAuthenticated = function () {
        return Session.isActive();
    };
    this.isAuthorized = function (authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }
        return (authorizedRoles.indexOf(Session.user.userRole) !== -1);
    };
    this.isLoggedIn = function (authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }
        return (this.isAuthenticated() && authorizedRoles.indexOf(USER_ROLES.all) === -1);
    }
})
