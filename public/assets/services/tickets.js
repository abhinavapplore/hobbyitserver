app.service('Tickets', function ($http, $q, API, Session) {
    return {
        'getAll': function () {
            var token = Session.user.token;
            var userId = Session.user.user;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "tickets",
                    'cache': false,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'GET',
                    'params': {
                        'userId': userId
                    }

                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'update': function (request) {
            var token = Session.vendor.token;
            var vendorId = Session.vendor.vendorId;
            
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "acceptOrder/" + vendorId,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'create': function (request) {

            var token = Session.user.token;

            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'method': 'POST',
                    'url': API.baseUrl + 'payment',
                    'data': request,
                    'headers': {
                        'Content-Type': 'application/json',
                        'Authorization': token
                    }
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'cancel': function (request) {

            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'method': 'POST',
                    'url': API.baseUrl + 'cancelpayment/',
                    'cache': false,
                    'data': request,
                    'headers': {
                        'Content-Type': 'application/json',
                        'Authorization': token
                    }
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        }
    };
});
