app.service('GetUserDashboard', function ($http, $q, API, Session) {
    return {
        'get': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "getUserDashboard/" + Session.user.user,
                    'cache': false,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'GET'
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err) {
                defer.reject(err);
            });
            return defer.promise;
        }
        
        
    };
});


