app.service('Transactions', function ($http, $q, API, Session) {
    return {
        'getAll': function () {
            var token = Session.user.token;
            var userId = Session.user.user;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "transactions",
                    'cache': false,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'GET',
                    'params': {
                        'userId': userId
                    }

                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'create': function (request) {
              console.log("transactions"+request);

            var token = Session.user.token;

            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'method': 'POST',
                    'url': API.baseUrl + 'transaction',
                    'data': request,
                    'headers': {
                        'Content-Type': undefined,
                        'Authorization': token
                    },
                    'transformRequest': angular.identity
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'cancel': function (request) {

            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'method': 'POST',
                    'url': API.baseUrl + 'canceltransaction/',
                    'cache': false,
                    'data': request,
                    'headers': {
                        'Content-Type': 'application/json',
                        'Authorization': token
                    }
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'approve': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "approvetransaction/",
                    'cache': false,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'reject': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "rejecttransaction/",
                    'cache': false,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        }
    };
});
