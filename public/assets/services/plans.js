app.service('Plan', function ($http, $q, API, Session) {
    return {
        
        'update': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "updateProduct/",
                    'headers': {
                        'Content-Type': undefined,
                        'Authorization': token
                    },
                    'transformRequest': angular.identity,
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'createn': function (request) {
            console.log("transactions"+request);

          var token = Session.user.token;

          var defer = $q.defer(),
              httpConfig = {},
              httpProperties = {
                  'method': 'POST',
                  'url': API.baseUrl + 'addProduct/',
                  'data': request,
                  'headers': {
                      'Content-Type': undefined,
                      'Authorization': token
                  },
                  'transformRequest': angular.identity
              };
          $http(httpProperties).success(function (resp) {
              if (resp.success) {
                  defer.resolve(resp);
              } else {
                  defer.reject(resp);
              }

          }).error(function (err, status) {
              defer.reject(err);
          });
          return defer.promise;
      },
      'delete': function (request) {
        var token = Session.user.token;
        var defer = $q.defer(),
            httpConfig = {},
            httpProperties = {
                'url': API.baseUrl + "deleteProduct/",
                'headers': {
                    'content-type': 'application/json',
                    'Authorization': token
                },
                'method': 'POST',
                'data': request
            };
        $http(httpProperties).success(function (resp) {
            if (resp.success) {
                defer.resolve(resp);
            } else {
                defer.reject(resp);
            }

        }).error(function (err, status) {
            defer.reject(err);
        });
        return defer.promise;
    }
    };
});
