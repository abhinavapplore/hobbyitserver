app.service('AuthSignup', function ($http, $q, API) {
    return {
        'signup': function (credentials) {
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "signup/",
                    'config': httpConfig,
                    'method': 'POST',
                    'data': credentials
                };

            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });

            return defer.promise;
        }
    };
});
