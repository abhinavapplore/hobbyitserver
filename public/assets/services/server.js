var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');

var rp = require('request-promise');
var baseUrl ="https://groceryimage.s3.ap-south-1.amazonaws.com/" ;
var morgan      = require('morgan');
//var querystring=require('querystring');
var mongoose    = require('mongoose');
var methodOverride = require('method-override')
var cors = require('cors');
const request = require('request');
var http = require('http');
var config      = require('./config/database'); // get db config file
require('dotenv').config();
require( 'dotenv' ).load();
var path = require('path');
var port        = process.env.PORT || 80;
var ObjectId = require('mongodb');
var jwt         = require('jwt-simple');

var Product        = require('./app/models/product'); 
var Category        = require('./app/models/category');
// var Coin        = require('./app/models/coin'); 
// var Req        = require('./app/models/req'); 

var Trip        = require('./app/models/trip');
var User        = require('./app/models/user');
var Order        = require('./app/models/order');
var randomstring = require("randomstring");

//multer

var multer = require( 'multer' );
var s3 = require( 'multer-storage-s3' );
var storage = s3({
    destination : function( req, file, cb ) {
        
        cb( null, '' );
        
    },
    filename    : function( req, file, cb ) {
        
        cb( null, randomstring.generate() +path.extname(file.originalname) );

        
    },
    bucket      : 'groceryimage',
    region      : 'us-east-2'
});
var uploadMiddleware = multer({ storage: storage });



//instamojo config
var Insta = require('instamojo-nodejs');
Insta.setKeys("a3a6c4782596013f7a03354f40660510", "c1236d3cc20985c70b94deda4b88b14c");


// const stripe = require('stripe')('sk_test_wCH2MwRMuM34Eooy906zGVTz00py2M67X0');

var moment = require('moment');
 
  //  Id     = require('./app/models/id');

  //instamojo setup
  


  


function getNextSequence(name,fn) {
    var retseq=0;
   
    Id.findOneAndUpdate({_id: name}, {$inc: { seq: 1 }}, {new: true}, function(err, doc){
    if(err){
        console.log("Something wrong when updating data!");
    }

  
         retseq=doc.seq;
       
        fn(retseq);
     
});
   
}




Date.prototype.addHours = function(h) {    
   this.setTime(this.getTime() + (h*60*60*1000)); 
   return this;   
}

Date.prototype.addDays = function(days)
{
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

var hasOwnProperty = Object.prototype.hasOwnProperty;
var isEmpty=function (obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
//app = express();
app.use(cors());     
// log to console
app.use(morgan('dev'));
 app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type,Authorization, Accept");
  next();
});

//app.get("*", function(req, res) {
//    res.render("/public/index.html");
//});
// Use the passport package in our application

mongoose.connect("mongodb://root:9sOGWLlpHceS@localhost/exp?authSource=admin", {useNewUrlParser: true});
//mongoose.connect(config.database);
// pass passport for configuration

route = require('./routes/routes');
var apiRoutes = express.Router();

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/assets/images'));
 app.use(express.static('uploads'));


app.get('/', function(req, res) {
     res.sendFile(path.join(__dirname, './public', 'index.html'));
   
    });
        
// connect the api routes under /api/*
app.use('/api', apiRoutes);

 
Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] == obj) {
            return true;
        }
    }
    return false;
}







apiRoutes.get('/users/', function(req, res){
	 
  
    User.find(function(err, user) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err){
                res.json(err);
            }
                
    else
        {
              res.json({success: true,users: user}); // return all todos in JSON format
        }
          
        });
    

});


apiRoutes.get('/trips', function(req, res){
        var token = req.query.Authorization;
        var userId=req.query.userId;
        var isauth= route.memberinfo(token,userId);
        token= "JWT "+token;
        var userRole=req.query.userRole;
        if(isauth){
            
            if (userRole=='superadmin')
            {
                Trip.find({adminRemark:"xyz"},function(err, trip) {
console.log(trip);
                    // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                    if (err){
                        res.send(err);
                    }
                        
            else
                {
                      res.json({success: true,trip: trip}); // return all todos in JSON format
                }
                  
                });

            }
            else{
                    
                Trip.find({'userId':userId},function(err, trip) {

                    // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                    if (err){
                        res.send(err);
                    }
                        
            else
                {
                      res.json({success: true,trip: trip}); // 
                }
                  
                });

            }
               
                
                     
                         
                         
                     
                
                 
   
           
    }
    else{
        res.json({success: false,msg:'No Authentication Token Present'});
    }
});

//api multer
apiRoutes.post( '/addProduct', uploadMiddleware.single('attachment'), function( req, res, next ) {

 
    var token = getToken(req.headers);
    var userId=req.body.userId;
    var product = req.body;
    var categoryName = req.body.productcategoryId.categoryName;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
    console.log("This is body "+JSON.stringify(req.body));
    console.log("This is file name"+req.file.filename);
    var productData = {
        productName: product.productName,
        productCost:product.productCost,
        productsell:product.productSell,
        productQuantity:product.productQuantity,
        categoryId:product.productcategoryId,
        categoryName:product.productcategory,
        productUrl:baseUrl+req.file.filename

    }


    console.log("tester"+JSON.stringify(req.body));

 

 
  

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    Product.addProduct(productData,function(err, productData){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Product Added Successfully',data:productData});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Token Authentication failed'});
 }

});

app.post('/api/instaPay', (req, res) => {

    console.log("req"+JSON.stringify(req.body));
    var data = new Insta.PaymentData();
    // var longurl = "";
 
data.purpose = "Test";            // REQUIRED
data.amount = req.body.amt;                  // REQUIRED
data.setRedirectUrl("");
 
Insta.createPayment(data, function(error, response) {
  if (error) {
    // some error
  } else {
    // Payment redirection link at response.payment_request.longurl
    console.log(response);
    response= JSON.parse(response);
    
    res.status(200).send({
        success: 'true',
        message: 'Payment retrieved successfully',
        data: response
      //   todos: db
      })
  }
});


    
  });
apiRoutes.post('/addProduct1', function(req, res){
    

    var token = getToken(req.headers);
    var userId=req.body.userId;
    var product = req.body;
    var categoryName = req.body.productcategoryId.categoryName;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
    
    var productData = {
        productName: product.productName,
        productCost:product.productCost,
        productsell:product.productSell,
        productQuantity:product.productQuantity,
        categoryId:product.productcategoryId._id,
        categoryName:categoryName

    }


    console.log("tester"+JSON.stringify(req.body));

 

 
  

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    Product.addProduct(productData,function(err, productData){
        if(err){
            
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Product Added Successfully',data:productData});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Token Authentication failed'});
 }
});

apiRoutes.post('/category', function(req, res){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    var category = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
    

    console.log("tester"+JSON.stringify(req.body));

 

 
  

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    Category.addCategory(category,function(err, categoryData){
        if(err){
            
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Category Added Successfully',data:categoryData});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});






apiRoutes.post('/updateCoin', function(req, res){
	   var coin = req.body;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var CoinId=Coin._id;
	 //  var coin = req.body;
    console.log("Coin"+JSON.stringify(coin));
    var isauth= route.memberinfo(token,userId);

    if(isauth){
        Coin.findOneAndUpdate(CoinId,{"coinBuyPrice":coin.coinBuyPrice,"coinSellPrice":coin.coinSellPrice},
                                           {},
                    function(err, model) {

   //handle it
            if(err){
            res.json({success: false,msg:'Failed to Update'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Coin Successfully Updated'});
            }
});
    }
    else{
        res.json({success: false,msg:'Failed to update Coin Token Authentication failed'});
    }
});
//Get all users
apiRoutes.get('/users/:clientId/:userId', function(req, res){
	 
    var token = getToken(req.headers);
       var userId=req.params.userId;
      var clientId=req.params.clientId;
   
       var isauth= route.memberinfo(token,userId);
       if(isauth){
   
   User.find({ 'clientId': clientId },function(err, user) {

           // if there is an error retrieving, send the error. nothing after res.send(err) will execute
           if (err){
               res.json(err);
           }
               
   else
       {
             res.json({success: true,users: user}); // return all todos in JSON format
       }
         
       });
   
       }
   else
       {
           
           res.json({success:false,msg:"Unauthorized"});
       }
});
apiRoutes.post('/order', function(req, res){


    console.log("body"+req.body);
    var txamt=req.body.txAmt;
  
    var token = req.body.token;
    var userId=req.body.userId;
    var userName=req.body.userName;
    var selectedItems= req.body.selectedItems;
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;

    var randomstring2 = require("randomstring");
       var cdata = {
        orderId :randomstring2.generate(8),
        orderDesc : selectedItems,
        totalAmount : txamt,
        userId : userId,
        userName : userName

       };
       
    
 
    
    if(isauth){
        User.findOne({ '_id':userId }, function(err, user) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }
        
            else
            {
                Order.addOrder(cdata, function(err, order){
                    if(err){
                        
                        res.json({success: false, msg: 'Failed to add Request'});
                    //	throw err;
                    }
                   
                    res.json({success: true,msg:'Request Sent Successfully',order:order});
                   
                });


            }

        });
	
    }
    else{
        res.json({success: false,msg:'Failed to add Request Token Authentication failed'});
    }
});

apiRoutes.get('/getOrders/:userId/:token' , function(req,res){

    var userId=req.params.userId;
    var token=req.params.token;
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;
    

    if(isauth){
        Order.find({userId:userId},function(err,orders)
        
        {
            res.json({success:true,orders:orders});
        });
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

        

});


apiRoutes.get('/getUserDashboard/:userId', function(req, res){
	  
    var token = getToken(req.headers);
    var userId=req.params.userId;
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;
    

    if(isauth){
        
        
         User.findOne({ '_id':userId }, function(err, user) {
             
             
             if(err){
                 
                res.json("failed to fetch data");
                 
             }
             if(!(isEmpty(user)))
                 {
             var accessLevelName =user.accessLevelName;
           
                     
                   
            if (user.isHod==true || accessLevelName=="hod")
            {
              
                
                 
                   
                    
                     
                     
                     Product.find(function(err, products) {
                        
            
            if (err){
                console.log("error");
                res.send(err);
            }
                       

                           
                         
                            Category.find(function(err, category) {
                                if(err)
                                    {
                                            console.log("Error");
                                    }
                              

                      
                           
                             
                           
                        
                         
                 res.json({success:true,product:products,category:category,user:user});
                        });
                    //   }
                            });
                         
     
                
            }
            //for admin            
                 
            
                     
             else if (user.accessLevelName=="superadmin"){
                 
                 User.find(function(err, user) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err){
                res.send(err);
            }
                    Product.find(function(err, products) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err){
                res.send(err);
            }
                      
                
            Order.find(function(err,orders)
            {

                         
                        
                              
                                        Category.find(function(err,categories)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }
                                           
                                            
                                               
                                                   
                                    res.json({success: true,users: user,products:products,orders:orders,categories:categories});
                                            });
                             
                                   
                                    
                
               
                                       
                                           
                                        
                                     }) ;   
                                    });
                        
                         
     
        
    
                
   
        
              // return all todos in JSON format
        
          
        });
    
                 
                 
             }
                    
             
            
             
                 // console.log(user);
            // res.json({success:true,msg:'User details fetch successfully',user:user});
             
             
                 }
             else
                 {
                     res.json({success: false,msg:'User does not exist'});
                 }
    });
         
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
});

apiRoutes.get('/getUserDashboard/:userId/:token', function(req, res){
	  
   // var token = getToken(req.headers);
    var userId=req.params.userId;
    var token=req.params.token;
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;
    

    if(isauth){
        
        
         User.findOne({ '_id':userId }, function(err, user) {
             
             
             if(err){
                 
                res.json("failed to fetch data");
                 
             }
             if(!(isEmpty(user)))
                 {
             var accessLevelName =user.accessLevelName;
           
                     
                   
            if (user.isHod==true || accessLevelName=="basic")
            {
              
                
                 
                   
                    
                     
                     
                     Product.find(function(err, products) {
                        
            
            if (err){
                console.log("error");
                res.send(err);
            }

            
                       

                           
                         
                            Category.find(function(err, category) {
                                if(err)
                                    {
                                            console.log("Error");
                                    }
                                    var fdata=[];
                                    for(var j=0; j<category.length;j++){
                                        var prod = [];
                                        var cat = {};
                                        for(var k =0; k<products.length; k++){
                                            if(category[j].categoryName==products[k].categoryName){
                                                prod.push(products[k]);


                                            }
                                        }
                                        cat.category=category[j].categoryName;
                                        cat.products=prod;
                                        fdata.push(cat);
                                        
                                        
                
                                    }
                        
                                    
                                    
                              

                      
                           
                             
                           
                        
                         
                 res.json({success:true,product:fdata,category:category,user:user});
                        });
                    //   }
                            });
                         
     
                
            }
            //for admin            
                 
            
                     
             else if (user.isSuperAdmin==true){
                 
                 User.find(function(err, user) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err){
                res.send(err);
            }
                    Product.find(function(err, products) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err){
                res.send(err);
            }
                      
                
                   
                         
                        
                              
                                        Category.find(function(err,categories)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }
                                              Order.find(function(err,orders)
                                              {


                                             
                                           
                                            
                                               
                                                   
                                    res.json({success: true,users: user,products:products,orders:orders,categories:categories});
                                            });
                             
                                   
                                    
                
               
                                       
                                           
                                        
                                     }) ;   
                                    });
                         
     
        
    
                
   
        
              // return all todos in JSON format
        
          
        });
    
                 
                 
             }
                    
             
            
             
                 // console.log(user);
            // res.json({success:true,msg:'User details fetch successfully',user:user});
             
             
                 }
             else
                 {
                     res.json({success: false,msg:'User does not exist'});
                 }
    });
         
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
});


apiRoutes.get('/getUserDetails/:userId', function(req, res){
	  
    var token = getToken(req.headers);
    var userId=req.params.userId;
    var isauth= route.memberinfo(token,userId);
    
    if(isauth){
         User.findOne({ '_id':userId }, function(err, user) {
             
             if(err){
                 
                res.json("failed to fetch data");
                 
             }
            // City.find()
             
       
             res.json({success:true,msg:'User details fetch successfully',user:user});
             
    });
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});



apiRoutes.post('/updateUserDetails/:userId', function(req, res){
	  
    var token = getToken(req.headers);
    var userIdupdate=req.body.userId;
       var userId=req.params.userId;
    var isauth= route.memberinfo(token,userId);
    var user =req.body;
    var isPasswordChange=req.body.pwdmode;
    
    if(isauth){

        if(isPasswordChange)
            {
                User.findOne({ '_id': userIdupdate }, function(err, user) {
         
         if(!user){
             res.json({success: false, msg: 'No user with this email id exist'});
         }
         
         else{
             
        
            
                    var pass = req.body.pwd;
             
                    user.password=pass;
         
             
                    user.save(function(err,user){
                 
             
                    User.update({ _id:user._id }, { $set: { password:user.password}}, function (err, user)
                                {
                        if (err) throw err;
                       else{
                                    res.json({success: true,msg:'Password Successfully Updated'});
                                }
                                });
                        

        }); 
                
                
            }
         });
            }
                      
             else
             
             {
                 
                      User.update({'_id': userIdupdate}, user, function(err, numberAffected, rawResponse) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    res.json({success: true,msg:'User Successfully Updated'});
                                }
                        });
                 
                 
             }
 
        
      
     
    
        
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});






apiRoutes.post('/sendCoin', function(req, res){
	  

    
 
  
    var txamt=req.body.txAmt;
    var mobile=req.body.mobile;
   //console.log("bacha"+txamt+mobile);
    var token = req.body.Authorization;
    var userId=req.body.userId;
   
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;
    //var userRole=req.query.userRole;

    console.log("body"+JSON.stringify(req.body));
    
    if(isauth){

                User.findOne({"_id":userId},function(err,userfrom)
            {
             if (err)
                {
                    console.log("error"+err);
                }
        User.findOne({"contactNum":mobile},function(err,userto)
        {
    if (err)
        {
            console.log("error"+err);
            res.json({success: false, msg: 'No such user registered'});

        }
        else if(userto==null)
        {
            res.json({success: false, msg: 'No such user registered'});
        }
        


        else{
//updating the sender balance

        var usdBalSender=userfrom.currentUSDbalance;
        var usdBalRec=userto.currentUSDbalance;
        var tx=parseInt(txamt);

        var updated_senderBal =usdBalSender-tx;
        var updated_recBal =usdBalRec+tx;
        console.log("Hello",usdBalSender,usdBalRec,tx,updated_senderBal,updated_recBal);

            User.findByIdAndUpdate( userId, { $set:{
              "currentUSDbalance":updated_senderBal
            }},{new:true}, function(err, us) {
               //handle it
                        if(err){
                        res.json({success: false,msg:'failed to send'});
                      //  throw err;
                    }
                        else{

                            User.findByIdAndUpdate( userto._id, { $set:{
                                "currentUSDbalance":updated_recBal
                            }}, function(err, rc) {
                               //handle it
                                        if(err){
                                        res.json({success: false,msg:'failed'});
                                      //  throw err;
                                    }
                                        else{
                                            var randomstring2 = require("randomstring");
                                            var trip={
                                                userId:userId,
                                                txnType:"Send",
                                                txnStatus:"Completed",
                                                usdAmount:txamt,
                                                txnId:randomstring2.generate(8) 
                    
                                                
                                            };
                                            var randomstring1 = require("randomstring");
                                            var trip1={
                                                userId:userto._id,
                                                txnType:"Recieve",
                                                txnStatus:"Completed",
                                                usdAmount:txamt,
                                                txnId:randomstring1.generate(8) 
                    
                                                
                                            };
                                            Trip.addTrip(trip, function(err, trip){
                                                if(err){
                                                    
                                                    res.json({success: false, msg: 'Failed to add trip'});
                                                    throw err;
                                                }
                                                else
                                                {
                                                   // res.json({success: true,msg:soldcoins+' Amount Successfully sent',data:{"currentXYZbalance": finalbal,"currentUSDbalance":usdAmt+usdbalance}});
                                                   Trip.addTrip(trip1, function(err, trip){
                                                    if(err){
                                                        
                                                        res.json({success: false, msg: 'Failed to add trip'});
                                                        throw err;
                                                    }
                                                    else
                                                    {
                                                        res.json({success: true,msg:' Amount Successfully sent',data:{"updated_senderBal":updated_senderBal,"user":us}});
                                                    }
                                        
                                                });
                                                
                                                }
                                    
                                            });
                                           
                                                         
                          
                                              
                                           
                                        }
                            });
                            
                              
          
                              
                           
                        }
            });
//updating the reciever balance
           
            
                

 
        }

        

        
       
       
    });
});
    }
    else {
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});






apiRoutes.post('/updateStatus', function(req, res){
	  

    var token = getToken(req.headers);
    var userId=req.body.userId;
    var tripId=req.body.tripId;
    var isauth= route.memberinfo(token,userId);
    
    if(isauth){
        Trip.update({_id: tripId}, { $set:{
            "txnStatus":"Completed"
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                    res.json({success: false,msg:'No such order'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Status Updated Successfully'});
                    }
        });

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});







apiRoutes.post('/login', route.login);
 apiRoutes.post('/signup', route.signup);
apiRoutes.post('/forgot', route.forgot);
// Start the server
app.listen(port);

console.log('Server is running at  http://localhost:' + port);        
        
        