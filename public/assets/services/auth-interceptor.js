app.factory('AuthInterceptor', ['$rootScope', '$q', 'AUTH_EVENTS', 'Session', function ($rootScope, $q, AUTH_EVENTS, Session) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            var token = Session.getToken();

            if (token)
                config.headers.Authorization = token;

            return config;
        },
        responseError: function (response) {
            $rootScope.$broadcast({
                401: AUTH_EVENTS.notAuthenticated,
                403: AUTH_EVENTS.notAuthorized,
                419: AUTH_EVENTS.sessionTimeout,
                440: AUTH_EVENTS.sessionTimeout
            }[response.status], response);
            return $q.reject(response);
        }
    };
}]);
