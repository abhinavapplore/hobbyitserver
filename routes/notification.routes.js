let app = require('express').Router();
var Notification = require('../app/models/notification');
var mongoose = require('mongoose');
app.get('/', async (req, res) => {
  let notification = await Notification.find().limit(100);

  res.send({ status: true, notification: notification });
});
app.get('/:userId', async (req, res) => {
  let notification = await Notification.find({
    userId: req.params.userId,
  })
    .limit(20)
    .sort({ _id: -1 });

  res.send({ status: true, notification: notification });
});

// app.post('/', async (req, res) => {
//   let notification = await Notification.addNotification({
//     userId: req.body.forUserId,
//     title: req.body.title,
//   });

//   res.send({ status: true, notification: notification });
// });

module.exports = app;
